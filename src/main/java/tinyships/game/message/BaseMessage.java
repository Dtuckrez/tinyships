/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.game.message;

import tinyships.game.mission.Mission;

/**
 *
 * @author Dean
 */
public class BaseMessage {
    private String title;
    private String sender;
    private String content;
    
    private Mission mission;
    
    private boolean isStoryMessage;
    private boolean isRead;
    
    
    public BaseMessage(String title, String sender, String content, boolean isStory) {
        this.title = title;
        this.sender = sender;
        this.content = content;
        
        this.isStoryMessage = isStory;
        this.isRead = false;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Mission getMission() {
        return mission;
    }

    public void setMission(Mission mission) {
        this.mission = mission;
    }

    public boolean isIsStoryMessage() {
        return isStoryMessage;
    }

    public void setIsStoryMessage(boolean isStoryMessage) {
        this.isStoryMessage = isStoryMessage;
    }

    public boolean isIsRead() {
        return isRead;
    }

    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }
}
