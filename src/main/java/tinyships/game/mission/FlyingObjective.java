/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.game.mission;

import org.newdawn.slick.geom.Circle;
import tinyships.objects.player.Player;

/**
 *
 * @author Dean
 */
public class FlyingObjective extends BaseObjective {

    private final int sectorX, sectorY;
    private final int xPos, yPos;
    private int completeTime = 0;

    private Circle objectiveArea;

    public FlyingObjective(String name, String description,
            int sectorX, int sectorY,
            int xPos, int yPos) {
        super(name, description);

        this.sectorX = sectorX;
        this.sectorY = sectorY;
        this.xPos = xPos;
        this.yPos = yPos;
    }

    public Circle getObjectiveArea() {
        return objectiveArea;
    }

    public void setObjectiveArea(Circle objectiveArea) {
        this.objectiveArea = objectiveArea;
    }

    public int getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(int completeTime) {
        this.completeTime = completeTime;
    }
    
    

    @Override
    public void updateObjective(Player player, int delta) {
        objectiveArea = new Circle(sectorX, sectorY, 150);
        if (player.getSectorX() == sectorX && player.getSectorY() == sectorY) {
            if (objectiveArea.contains(player.getShip().getCollisionZone())) {
                completeTime += 1 * delta;
            } else {
                completeTime -= 1 * delta;
            }
        }
        if (completeTime <= 0) {
            completeTime = 0;
        } else if (completeTime >= 4000) {
            setIsComplete(true);
            completeTime = 0;
        } else {
            setIsComplete(false);
        }
    }
}
