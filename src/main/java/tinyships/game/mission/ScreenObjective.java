/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.game.mission;

import tinyships.objects.player.Player;
import tinyships.enums.gui.GameScreens;
import tinyships.manager.ScreenManager;

/**
 *
 * @author Dean
 */
public class ScreenObjective extends BaseObjective {

    private final GameScreens screenId;
    private ScreenManager ScreenManager;

    public ScreenObjective(String name, String description, GameScreens screenId) {
        super(name, description);

        this.screenId = screenId;
    }

    @Override
    public void updateObjective(Player player, int delta) {
//        if (screenId == ScreenManager.getActiveScreen().getGameScreen()) {
//            if (!getIsComplete()) {
//                setIsComplete(true);
//            }
//        }
    }
}
