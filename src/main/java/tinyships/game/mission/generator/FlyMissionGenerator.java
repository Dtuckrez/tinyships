/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.game.mission.generator;

import java.util.ArrayList;
import java.util.Random;
import tinyships.game.mission.BaseObjective;
import tinyships.game.mission.FlyingObjective;
import tinyships.game.mission.Mission;
import tinyships.manager.SectorManager;
import tinyships.objects.sector.BaseSector;

/**
 *
 * @author Dean
 */
public class FlyMissionGenerator extends MissionGenerator {

    public static Mission GenerateMission() {

        int sectorX = 0;
        int sectorY = 0;

        int xPos = 10;
        int yPos = 10;

        ArrayList<BaseObjective> flyingObjectives = new ArrayList<>();
        flyingObjectives.add(new FlyingObjective("Scan", 
                "Go and get a scan in " + SectorManager.getSectors()[sectorX][sectorY].getName(), 
                sectorX, sectorY, xPos, yPos));

        Mission m = new Mission("Scanning", generateMissionText(sectorX, sectorY));
        m.setObjectives(flyingObjectives);

        return m;
    }
    
    public static String generateMissionText(int sectorX, int sectorY) {
        Random random = new Random();
        BaseSector sector = SectorManager.getSectors()[sectorX][sectorY];
        switch(random.nextInt(25)) {
            case 0 :
                return "Hey Pilot, I've got a job for you I need you to head to " + 
                        sector.getName() + " and get an Ion Particle Scan, this will really help me with my worm hole research";
            case 1 : 
                return "Pilot I need some new data taken from " + sector.getName() + 
                        ". This data will be a real insight into the origins of the transport gates!";
            
            case 2 : return "I need you to fly to " + sector.getName() + 
                    " and scan for any recent hyper jumps, I've got a feeling that someone is using some new tech";
                
                
            case 3 : return "We need a spy out in " + sector.getName() + 
                    ". We've had infomation that a The Swarm are testing out a new super weapon. please go and invastigate. ";
                
            case 4 : return "Pilot I need someone to head towards " + sector.getName() + 
                    " and make sure that everything is in order. we've had reports of Hyper Jumps going into that sector.";
            
            case 5 : return "Pilot I need you to get a scan of " + sector.getName() + 
                    ", The Swarm seem to be making use of this Sector with thier Hyper Junps.";
                
            case 6 : return "We need scans of Ion Particles from " + sector.getName() + 
                    " seems thier has been allot more ships passing through that Sector";
                
            case 7 : return "Seems we have more Pirates coming in and out of " + sector.getName() + " please go and check this out";
                
            case 8 : return "I needs scans of " + sector.getName() + "Can you get them for me?";
                
            case 9 : return sector.getName() + " Is being used as a trade route for pirates, "
                    + "I need you to make sure this infomation is correct. are you able to do this? ";
                
            default : return "Please head to " + sector.getName() + " I'm after scans for my new research.";
        }
    }
}
