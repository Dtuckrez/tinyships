/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.game.mission;

import java.util.ArrayList;

/**
 *
 * @author Dean
 */
public class Mission {

    private final ArrayList<BaseObjective> objectives = new ArrayList<>();
    
    private String name;
    private String desciption;

    private boolean isActive;
    
    public Mission(String name, String description) {
        super();
        this.name = name;
        this.desciption = description;
        this.isActive = false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesciption() {
        return desciption;
    }

    public void setDesciption(String desciption) {
        this.desciption = desciption;
    }

    public ArrayList<BaseObjective> getObjectives() {
        return objectives;
    }
    
    public void setObjectives(ArrayList<BaseObjective> objectives) {
        this.objectives.clear();
        this.objectives.addAll(objectives);
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean completeMission() {
        boolean missionComplete = false;
        for (BaseObjective baseObjective : objectives) {
            missionComplete = baseObjective.getIsComplete();
        }
        return missionComplete;
    }
}
