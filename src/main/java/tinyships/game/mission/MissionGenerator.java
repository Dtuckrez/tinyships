/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.game.mission;

import org.newdawn.slick.geom.Vector2f;
import tinyships.enums.Race;
import tinyships.enums.ship.equipment.EngineSize;
import tinyships.enums.ship.equipment.EngineType;
import tinyships.enums.ship.equipment.EquipmentType;
import tinyships.enums.ship.equipment.ShieldSize;
import tinyships.enums.ship.equipment.ShieldType;
import tinyships.manager.GameManager;
import tinyships.objects.ships.NPCShip;
import tinyships.xml.ships.ShipData;
import tinyships.xml.ships.engines.EngineData;
import tinyships.xml.ships.shields.ShieldData;

/**
 *
 * @author Dean
 */
public class MissionGenerator {
    
    public static Mission generateBountyMission() {

        int sectorX = 0;
        int sectorY = 0;

        int dif = 0;

        String brifing = "Pilot, we've got a" + String.valueOf(Race.PIRATE) + "flying around with a nice bounty on his head.";

        ShipData baseShipData = new ShipData();
        baseShipData.setHull(25);
        baseShipData.setShield(new ShieldData("", "", EquipmentType.TEST, ShieldType.TEST, ShieldSize.TEST, 100, 1000, 200));
        baseShipData.setEngine(new EngineData("", 0, 12, 0.01f, 0.2f, EngineSize.TEST, EngineType.TEST));
        NPCShip baseShip = new NPCShip(baseShipData);
        baseShip.setPosition(new Vector2f(600,600));

        int objectiveId = Integer.parseInt(Integer.toString(sectorX) + Integer.toString(sectorY) + Integer.toString(dif));

        GameManager.sectorManager.getSectorAtPosition(sectorX,sectorY).getShips().add(baseShip);

        Mission mission = new Mission("Bounty", brifing);
        mission.getObjectives().add(new DestroyShipObjective("Take down Pirate", "Take out the Pirtate", objectiveId));

        return mission;
        
    }
}
