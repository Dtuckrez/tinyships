/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.game.mission;

import tinyships.objects.player.Player;

/**
 *
 * @author Dean
 */
public class DockingObjective extends BaseObjective {

    private int sectorId;
    private int stationId;

    public DockingObjective(String name, String description, int sectorId, int stationId) {
        super(name, description);

        this.sectorId = sectorId;
        this.stationId = stationId;
    }

    public void setSectorId(int sectorId) {
        this.sectorId = sectorId;
    }

    public int getSectorId() {
        return this.sectorId;
    }

    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    public int getStationId() {
        return this.stationId;
    }

    @Override
    public void updateObjective(Player player, int delta) {
        if (player.getShip().getSector().getId() == sectorId && player.getShip().getIsDocked() && 
                player.getShip().getDockedStation().getStationId() == stationId) {
            isComplete = true;
        }
    }
}
