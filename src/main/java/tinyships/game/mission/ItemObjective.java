/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.game.mission;

import tinyships.objects.player.Player;
import tinyships.objects.cargo.BaseCargo;
import tinyships.objects.ships.cargobay.HeldCargo;

/**
 *
 * @author Dean
 */
public class ItemObjective extends BaseObjective {

    private final BaseCargo cargo;
    private final int amountNeeded;

    public ItemObjective(String name, String description,
            BaseCargo cargo, int amountNeeded) {
        super(name, description);

        this.cargo = cargo;
        this.amountNeeded = amountNeeded;
    }

    @Override
    public void updateObjective(Player player, int delta) {
        for (HeldCargo heldCargo : player.getShip().getCargo()) {
            if (heldCargo.getAmount() >= amountNeeded) {
                setIsComplete(true);
            }
        }
    }
}
