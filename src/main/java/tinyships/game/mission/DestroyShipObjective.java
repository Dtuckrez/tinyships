/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.game.mission;

import tinyships.objects.ships.BaseShip;

/**
 *
 * @author Dean
 */
public class DestroyShipObjective extends BaseObjective {

    private int shipId = Integer.MAX_VALUE;

    public DestroyShipObjective(String name, String description, int shipId) {
        super(name, description);

        this.shipId = shipId;
    }
    
    public void checkIfShipWasDestroyed(BaseShip baseShip) {
//        if (baseShip.getObjectiveId() == shipId) {
//            this.isComplete = true;
//        }
    }
}
