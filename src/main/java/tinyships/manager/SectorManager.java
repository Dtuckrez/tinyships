/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.manager;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.objects.player.Player;
import tinyships.objects.sector.BaseSector;
import tinyships.xml.SectorLoader;

/**
 *
 * @author Dean
 */
public class SectorManager extends Thread implements Runnable {
    public final static BaseSector[][] sectors = new BaseSector[2][2];
    public static GameContainer gc;
    public static int delta;
    
    public SectorManager() {
        sectors[0][0] = (BaseSector)SectorLoader.loadSector("assets/sectors/sectors.xml", 0);
    }
    
    public static BaseSector[][] getSectors() {
        return sectors;
    }
    
    public BaseSector getSectorAtPosition(int x, int y) {
        return sectors[x][y];
    }
    
    public void drawSector(GameContainer gc, Graphics g, Player player) {
        sectors[player.getSectorX()][player.getSectorY()].drawSector(gc, g);
    }
    
    public void drawSectorEntities(GameContainer gc, Graphics g, Player player) {
        
        sectors[player.getSectorX()][player.getSectorY()].drawObjects(gc, g);
        sectors[player.getSectorX()][player.getSectorY()].drawItem(gc, g);
        sectors[player.getSectorX()][player.getSectorY()].drawShips(gc, g);
    }
    
    @Override
    public void run() {
        for (int x = 0; x < 1; x++) {
            for (int y = 0; y < 1; y++) {
                if (sectors[x][y] != null) {
                    sectors[x][y].updateSector(gc, delta, 0, 0, GameManager.getPlayer());
                }
            }
        }
        delta = GameManager.curDelta;
    }
}
