/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.manager;

import java.util.ArrayList;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.objects.ships.weapons.projectile.BaseProjectile;

/**
 *
 * @author Dean
 */
public class ProjectileManager {

    public static ArrayList<BaseProjectile> projectiles = new ArrayList<>();

    public static void addNewProjectile(BaseProjectile baseProjectile) {
        // Check projectile Size
        if (projectiles.size() <= 0) {
            projectiles.add(baseProjectile);
        } else {
            // loop over and see if any projectiles are dead
            boolean addNew = false;
            for (int i = 0; i < projectiles.size(); i++) {
                if (!projectiles.get(i).isAlive() && projectiles.get(i).getClass().equals(baseProjectile.getClass())) {
                    projectiles.get(i).reset(baseProjectile);
                    addNew = false;
                    return;
                } else {
                    addNew = true;
                }
            }
            if (addNew) {
                projectiles.add(baseProjectile);
            }
        }
    }

    public static void updateProjectiles(int delta) {
        for (BaseProjectile baseProjectile : projectiles) {
            baseProjectile.move(delta);
        }
    }

    public static void drawProjectiles(GameContainer gc, Graphics g) {
        for (BaseProjectile baseProjectile : projectiles) {
            baseProjectile.draw(gc, g);
        }
    }
}
