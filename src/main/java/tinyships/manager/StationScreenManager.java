/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.manager;

import java.util.ArrayList;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import tinyships.enums.gui.GameScreens;
import tinyships.global.Colours;
import tinyships.objects.ships.PlayerShip;
import tinyships.objects.entity.station.BaseStation;
import tinyships.objects.screen.TargetScreen;
import tinyships.objects.screen.station.StationCrewScreen;
import tinyships.objects.screen.station.StationMessageBoardScreen;
import tinyships.objects.screen.station.StationOverScreen;
import tinyships.objects.screen.station.StationTradeScreen;
import tinyships.ui.screens.BaseScreen;

/**
 *
 * @author Dean
 */
public class StationScreenManager {

    public static final String TARGET_SCREEN = "targetscreen";
    public static final String LOG_SCREEN = "logscreen";
    public static final String TRADE_SCREEN = "tradescreen";
    public static final String CREW_SCREEN = "crewscreen";
    
    public BaseStation station;
    public PlayerShip playerShip;

    public static StationOverScreen stationOverScreen;
    public static ArrayList<BaseScreen> screens = new ArrayList<>();

    public StationScreenManager() {
//        screens.add(new TargetScreen(GameScreens.TARGETSCEEN, TARGET_SCREEN, Colours.BOXBLUE));
//        screens.add(new StationMessageBoardScreen(GameScreens.STATIONLOGSCREEN, LOG_SCREEN, Colours.BOXBLUE));
//        screens.add(new StationTradeScreen(GameScreens.STATIONTRADESCREEN, TRADE_SCREEN, Colours.BOXBLUE));
//        screens.add(new StationCrewScreen(GameScreens.STATIONCREWSCREEN, CREW_SCREEN, Colours.BOXBLUE));
//        stationOverScreen = new StationOverScreen(GameScreens.BASESCREEN, "", Colours.BOXBLUE);
    }
    
    public static StationCrewScreen getCrewScreen() {
        for (BaseScreen screen : screens) {
//            if (screen.getName().equals(CREW_SCREEN)) {
//                return (StationCrewScreen) screen;
//            }
        }
        return null;
    }

    public static void changeScreen(GameScreens screenId) {
        closeAllScreens();
        for (BaseScreen screen : screens) {
//            if (screen.getGameScreen().equals(screenId)) {
//                screen.isScreenActive = true;
//                return;
//            }
        }
    }

    public static void closeAllScreens() {
        for (BaseScreen screen : screens) {
            screen.isScreenActive = false;
        }
    }

    public void deactivateAllStation() {
    }

    public void setStation(BaseStation station) {
        this.station = station;
//        ((TargetScreen) screens.get(0)).setEntity(station);
//        screens.get(0).isScreenActive = true;
//        stationOverScreen.isScreenActive = false;
    }

    public void runScreen(boolean isLeftDown, GameContainer gc, int delta) {
        
        Input input = gc.getInput();
        
        if (input.isKeyDown(Input.KEY_ESCAPE)) {
            closeAllScreens();
            GameManager.getPlayer().getShip().setIsDocked(false);
            GameManager.screenManager.setShowingStationScreen(false);
        }
        
        for (BaseScreen screen : screens) {
            if (screen.isScreenActive) {
                screen.updateScreen(isLeftDown, gc, delta);
            }
        }
        stationOverScreen.updateScreen(isLeftDown, gc, delta);
    }

    public void drawScreens(GameContainer gc, Graphics g) {
        for (BaseScreen screen : screens) {
            if (screen.isScreenActive) {
                screen.drawScreen(gc, g);
                break;
            }
        }
        stationOverScreen.drawScreen(gc, g);
    }
}
