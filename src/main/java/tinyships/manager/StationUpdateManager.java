/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.manager;

/**
 *
 * @author Dean
 */
public class StationUpdateManager extends Thread implements Runnable {
    
    private int mainGameDelta = 0;
    
    
    @Override
    public void run() {
//        GameManager.
    }
    
    public void updateGameDelta(int delta) {
        mainGameDelta = delta;
    } 
    
}
