/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.manager;

import java.util.ArrayList;
import tinyships.enums.gui.GameScreens;
import tinyships.objects.player.Player;
import tinyships.game.mission.BaseObjective;
import tinyships.game.mission.Mission;
import tinyships.objects.screen.MissionScreen;

/**
 *
 * @author Dean
 */
public class MissionManager {

    ArrayList<Mission> missons = new ArrayList<>();

    public MissionManager() {

    }

    public void updateManager(ScreenManager screenManager, Player player, int delta) {
        for (int i = 0; i < player.getMissions().size(); i++) {
            boolean completeMission = true;
            for (BaseObjective objective : player.getMissions().get(i).getObjectives()) {
                objective.updateObjective(player, delta);
                if (!objective.getIsComplete()) {
                    completeMission = false;
                }
            }

            if (completeMission) {
                screenManager.showMissionBox(player.getMissions().get(i).getName());
                player.getMissions().get(i).completeMission();
                player.getMissions().get(i).getObjectives().clear();
                player.getMissions().remove(i);
//                ((MissionScreen)ScreenManager.getScreenById(GameScreens.MISSIONSCREEN)).setHaveMissionsUpdated(false);
            }
        }
    }
    
    public void addNewMission() {
        
    }
}
