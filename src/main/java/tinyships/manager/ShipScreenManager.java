/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tinyships.manager;

import java.util.ArrayList;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import tinyships.enums.gui.GameScreens;
import tinyships.global.Colours;
import static tinyships.manager.StationScreenManager.closeAllScreens;
import tinyships.objects.screen.TargetScreen;
import tinyships.objects.screen.ship.ShipCargoScreen;
import tinyships.objects.screen.ship.ShipEquipmentScreen;
import tinyships.objects.screen.ship.ShipOverScreen;
import tinyships.objects.screen.ship.ShipWeaponScreen;
import tinyships.objects.ships.BaseShip;
import tinyships.ui.screens.BaseScreen;

/**
 *
 * @author Dean
 */
public class ShipScreenManager {

    public static final String SHIPEQ_SCREEN = "equipmentScreen";
    public static final String SHIPWEP_SCREEN = "weaponScreen";
    public static final String SHIPCARGP_SCREEN = "cargoScreen";

    private BaseShip ship;

    public static ShipOverScreen shipOverScreen;
    public static ArrayList<BaseScreen> screens = new ArrayList<>();

    public ShipScreenManager() {
//        shipOverScreen = new ShipOverScreen(GameScreens.SHIPSCREEN, "", Colours.BOXBLUE);
//        screens.add(new TargetScreen(GameScreens.TARGETSCEEN, SHIPEQ_SCREEN, Colours.BOXBLUE));
//        screens.add(new ShipWeaponScreen(GameScreens.SHIPWEAPONSCREEN, SHIPWEP_SCREEN, Colours.BOXBLUE));
//        screens.add(new ShipEquipmentScreen(GameScreens.SHIPEQUIPMENTSCREEN, SHIPEQ_SCREEN, Colours.BOXBLUE));
//        screens.add(new ShipCargoScreen(GameScreens.SHIPCARGOSCREEN, SHIPCARGP_SCREEN, Colours.BOXBLUE));
    }

    public void setShip(BaseShip ship) {
        this.ship = ship;
    }

    public static void changeScreen(GameScreens screenId) {
        closeAllScreens();
        for (BaseScreen screen : screens) {
//            if (screen.getGameScreen().equals(screenId)) {
//                screen.isScreenActive = true;
//                return;
//            }
        }
    }

    public void runScreen(boolean isLeftDown, GameContainer gc, int delta) {

        Input input = gc.getInput();

        if (input.isKeyDown(Input.KEY_ESCAPE)) {
            closeAllScreens();
            GameManager.screenManager.setShowingShipScreen(false);
            for (BaseScreen screen : screens) {
                screen.isScreenActive = false;
            }
        }

        for (BaseScreen screen : screens) {
            if (screen.isScreenActive) {
                screen.updateScreen(isLeftDown, gc, delta);
            }
        }
        shipOverScreen.updateScreen(isLeftDown, gc, delta);
    }

    public void drawScreens(GameContainer gc, Graphics g) {
        for (BaseScreen screen : screens) {
            if (screen.isScreenActive) {
                screen.drawScreen(gc, g);
                break;
            }
        }
        shipOverScreen.drawScreen(gc, g);
    }
}
