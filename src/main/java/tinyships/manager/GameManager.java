/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.manager;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Vector2f;
import tinyships.enums.ship.ShipType;
import tinyships.game.message.BaseMessage;
import tinyships.game.mission.DockingObjective;
import tinyships.game.mission.FlyingObjective;
import tinyships.game.mission.Mission;
import tinyships.objects.entity.BaseEntity;
import tinyships.objects.player.Player;
import tinyships.objects.ships.PlayerShip;
import tinyships.xml.ShipLoader;

/**
 *
 * @author Dean
 */
public class GameManager {

    public static int curDelta;
    private static boolean isNewGame = true;
    public static int realX, realY;
    public final static Player player = new Player();
    public static SectorManager sectorManager;
    public static ScreenManager screenManager;
    public static MissionManager missionManager;
    public static DialogManager dialogManager;
    public static BackgroundManager backgroundManager;

    public static int lastIdNumber = 0;

    public static boolean isHolding = false;
    
    public GameManager() {

//        sectors[1] = TileSectorLoader.loadSector("assets/sectors/Sector.xml");
        player.setShip((PlayerShip) ShipLoader.loadShip(ShipType.PLAYER, "assets/ships/ships.xml",
                0));

        player.getShip().setPosition(new Vector2f(0, -120));

        sectorManager = new SectorManager();
        screenManager = new ScreenManager(this);
        missionManager = new MissionManager();
        dialogManager = new DialogManager();
        backgroundManager = new BackgroundManager();
        player.getShip().setSector(sectorManager.getSectorAtPosition(player.getSectorX(), player.getSectorY()));
    }

    public static Player getPlayer() {
        return player;
    }

    public static DialogManager getDialogManger() {
        return dialogManager;
    }

    public void updateMousePosition(GameContainer gc) {
        Input input = gc.getInput();
        realX = ((int) player.getShip().getPosition().x) + input.getMouseX() - gc.getWidth() / 2;
        realY = ((int) player.getShip().getPosition().y) + input.getMouseY() - gc.getHeight() / 2;
    }
    
    public static float getRealMouseX() {
        return realX;
    }
    
    public static float getRealMouseY(){
        return realY;
    }

    public boolean isLeftMousePressed(GameContainer gc) {
        return gc.getInput().isMousePressed(0);
    }

    public void updateGame(GameContainer gc, int delta) {
        curDelta = delta;
        Input input = gc.getInput();

        if (isNewGame) {
            
            dialogManager.setShowingDialog(true);
            dialogManager.getTextBox().updatePages("Hi there Pilot, I see you've got a new ship... Good for you but if you want to your head you might want to go and find away in making some money... You've still got that bounty on your head! ", false);
            dialogManager.addPagesToDialog("Pilot, one of my contacts has sent you a message, he's willing to meet you and may have some work for you... Check your messages and see what they want. One word of adive don't do anything to upset them... they have a short well let's just say, upsetting them might not be the best idea. /N Nova, Out");
            Mission mission = new Mission("First Steps", "Meet With R3X at the trading outpost in Blue Star");
            mission.getObjectives().add(new DockingObjective("Dock and Meet with Dave", "Dock with the Energy Station in Blue Light", 0, 0));
            
            BaseMessage message = new BaseMessage("First Contact", "Mr. Wright", "Pilot, you don't know me but I know enough about you. /N I understand you owe allot of money to a certain bounty hunter. You see I paid the bounty on your head so you're... well you're free to work with me anyway. As you now owe me the money you have two choices you can do as I ask or you can fly off and see what happens? /N if you are going to be a good little pup I've sent you the details of your first mission, check your log screen. /N Mr. Write", true);
            message.setMission(mission);
            player.getMessages().add(message);
            isNewGame = false;
            dialogManager.setShowingDialog(true);
        }

        player.getShip().updateShip(gc, delta);
        sectorManager.getSectorAtPosition(player.getSectorX(), player.getSectorY()).updateSector(gc, delta, 0, 0, player);
        sectorManager.getState();
        if (sectorManager.getState() == Thread.State.NEW) {
            sectorManager.start();
        }

        screenManager.runScreen(player, gc, delta, screenManager.getActiveScreen(), input);

        if (isLeftMousePressed(gc)) {
            if (sectorManager.getSectorAtPosition(player.getSectorX(), player.getSectorY()).checkIfObjectIsBeingSelected(realX, realY)) {
                for (BaseEntity entity : sectorManager.getSectorAtPosition(player.getSectorX(), player.getSectorY()).getEntitys()) {
                    if (entity.isSelected()) {
                        player.getShip().setTargetEntity(entity);
                        return;
                    }
                }
            }
        }

        updateMousePosition(gc);
        ProjectileManager.updateProjectiles(delta);
        missionManager.updateManager(GameManager.screenManager, player, delta);
        dialogManager.updateDialogManager(gc, delta, input);
        if (player != null) {
            backgroundManager.updateBackground(player, delta);
        }
        input.clearMousePressedRecord();
        input.clearKeyPressedRecord();
    }

    public void drawSector(GameContainer gc, Graphics g) {
        sectorManager.drawSector(gc, g, player);
    }

    public void drawGame(GameContainer gc, Graphics g) {
        sectorManager.drawSectorEntities(gc, g, player);
        player.draw(gc, g);
        ProjectileManager.drawProjectiles(gc, g);
    }

    public static int useIdNumber() {
        lastIdNumber++;
        return lastIdNumber;
    }
}
