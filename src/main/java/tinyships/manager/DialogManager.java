/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.manager;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import tinyships.enums.gui.GameScreenElements;
import tinyships.game.mission.Mission;
import tinyships.global.Colours;
import tinyships.global.Fonts;
import tinyships.objects.screen.element.MultiLineTextBox;

/**
 *
 * @author Dean
 */
public class DialogManager {

    
    private Mission missionToAdd;
    private boolean showingDialog = false;
    
    private MultiLineTextBox textBox = new MultiLineTextBox(GameScreenElements.LOG, 
            "", Fonts.dialogFont, 96 * 2, 364, 736, 125, Colours.WHITE, Colours.BOXBLUE);

    public DialogManager() {

    }

    public Mission getMissionToAdd() {
        return missionToAdd;
    }

    public void setMissionToAdd(Mission missionToAdd) {
        this.missionToAdd = missionToAdd;
    }
    
    public void updateDialogBox(String Dialog, boolean hasButtons) {
        textBox.updatePages(Dialog, hasButtons);
    }
    
    public void addPagesToDialog(String Dialog) {
        textBox.addNewPage(Dialog);
    }

    public boolean isShowingDialog() {
        return showingDialog;
    }

    public void setShowingDialog(boolean showing) {
        this.showingDialog = showing;
    }

    public MultiLineTextBox getTextBox() {
        return textBox;
    }

    public void updateDialogManager(GameContainer gc, int delta, Input input) {
        
        boolean isLeftDown = gc.getInput().isMouseButtonDown(0);
        if (showingDialog) {
            textBox.updateElement(isLeftDown, gc, delta);
            if (textBox.isHasButtons()) {
                if (textBox.getAcceptButton().isButtonPressed(isLeftDown)) {
                    GameManager.player.getMissions().add(missionToAdd);
                }
            }
        }
    }

    public void drawDialog(GameContainer gc, Graphics g) {
        if (showingDialog) {
            textBox.drawElement(gc, g);
        }
    }
    
}
