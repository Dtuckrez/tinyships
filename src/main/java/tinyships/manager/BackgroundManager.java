/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.manager;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import tinyships.objects.player.Player;

/**
 *
 * @author Dean
 */
public class BackgroundManager {

    private Image image;
   
    Vector2f position = new Vector2f(250, 300);
    private final BackgroundSection[][] backgroundSections = new BackgroundSection[3][3];

    public BackgroundManager() {
        
        try {
            image =  new Image("images/sector/objects/planets/0.png");
        } catch (SlickException ex) {
            Logger.getLogger(BackgroundManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                backgroundSections[i][j] = new BackgroundSection("images/player/test.png", -1024 + (j * 1024), -768 + (i * 768));
            }
        }
    }

    public void updateBackground(Player player, int delta) {
        
        
            position.x -= player.getShip().getEngine().getSpeed() * 0.1f * Math.sin(Math.toRadians(player.getShip().getRotation()));
            position.y += player.getShip().getEngine().getSpeed() * 0.1f * Math.cos(Math.toRadians(player.getShip().getRotation()));
        
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                backgroundSections[i][j].updateBackgroundSection(player, delta);
            }
        }
    }

    public void drawBackground(GameContainer gc, Graphics g) {
        g.drawImage(image, position.x, position.y);
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                backgroundSections[i][j].drawBackgroundSection(gc, g);
            }
        }
    }

    private static class BackgroundSection {

        Image image;
        Vector2f position = new Vector2f();

        public BackgroundSection(String file, int xPos, int yPos) {
            try {
                image = new Image(file);
            } catch (SlickException ex) {
                Logger.getLogger(BackgroundManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            position.x = xPos;
            position.y = yPos;
        }

        public void updateBackgroundSection(Player player, int delta) {
            position.x -= player.getShip().getEngine().getSpeed() * 0.3f * Math.sin(Math.toRadians(player.getShip().getRotation()));
            position.y += player.getShip().getEngine().getSpeed() * 0.3f * Math.cos(Math.toRadians(player.getShip().getRotation()));

            if (position.x > 1024) {
                position.x = -1024;
            }

            if (position.x + 1024 < 0) {
                position.x = 1024;
            }

            if (position.y > 768) {
                position.y = -768;
            }

            if (position.y + 768 < 0) {
                position.y = 768;
            }
        }

        public void drawBackgroundSection(GameContainer gc, Graphics g) {
            if (position.x <= 1024 && position.x + 1024 >= 0
                    && position.y <= 768 && position.y + 768 >= 0) {
                image.draw(position.x, position.y, 1024, 768);
            }
        }
    }
}
