    /*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.manager;

import java.util.ArrayList;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import tinyships.objects.player.Player;
import tinyships.enums.gui.GameScreenContainers;
import tinyships.enums.gui.GameScreenElements;
import tinyships.enums.gui.GameScreens;
import tinyships.objects.entity.BaseEntity;
import tinyships.objects.entity.station.BaseStation;
import tinyships.ui.screens.BaseScreen;
import tinyships.objects.screen.HUDScreen;
import tinyships.objects.screen.MessageScreen;
import tinyships.objects.screen.MissionScreen;
import tinyships.objects.screen.OverScreen;
import tinyships.objects.screen.ship.ShipScreen;
import tinyships.objects.screen.TargetScreen;
import tinyships.objects.screen.element.ButtonImageElement;
import tinyships.objects.ships.BaseShip;

/**
 *
 * @author Dean
 */
public class ScreenManager {

    public static boolean staticnotUpdated = false;
    public static boolean showingShipScreen = false;
    public static boolean showingStationScreen = false;

    public static StationScreenManager stationScreenManager;
    public static ShipScreenManager shipScreenManager;

    public static OverScreen overScreen;

    public static ArrayList<BaseScreen> screens = new ArrayList<>();


    public ScreenManager(GameManager gameManager) {
        stationScreenManager = new StationScreenManager();
        shipScreenManager = new ShipScreenManager();

        overScreen = new OverScreen(GameScreens.BASESCREEN, "", Color.yellow);
        setupScreens(screens);
    }

    public boolean isStaticnotUpdated() {
        return staticnotUpdated;
    }

    public void setStaticnotUpdated(boolean staticnotUpdated) {
        ScreenManager.staticnotUpdated = staticnotUpdated;
    }

    public boolean isShowingShipScreen() {
        return showingShipScreen;
    }

    public void setShowingShipScreen(boolean showingShipScreen) {
        ScreenManager.showingShipScreen = showingShipScreen;
    }

    public boolean isShowingStationScreen() {
        return showingStationScreen;
    }

    public void setShowingStationScreen(boolean showingStationScreen) {
        ScreenManager.showingStationScreen = showingStationScreen;
    }

    private static void setupScreens(ArrayList<BaseScreen> screens) {
//        screens.add(new HUDScreen(GameScreens.HUDSCREEN,
//                "gameScreen", new Color(0, 0.3f, 0.7f, 0.6f)));
//        screens.get(0).isScreenActive = true;
//
//        screens.add(new MissionScreen(GameScreens.MISSIONSCREEN,
//                "missionScreen", new Color(0, 0.3f, 0.7f, 0.6f)));
//
//        screens.add(new TargetScreen(GameScreens.TARGETSCEEN,
//                "targetscreen", new Color(0, 0.3f, 0.7f, 0.6f)));
//        
//        screens.add(new MessageScreen(GameScreens.MESSAGESCREEN,
//                "messagescreen", new Color(0, 0.3f, 0.7f, 0.6f)));
    }

    public void updateScreens(boolean leftDown, Player player, GameContainer gc, int delta, Input input) {
    }

    public void runScreen(Player player, GameContainer gc, int delta,
            BaseScreen screen, Input input) {
        boolean isLeftDown = gc.getInput().isMouseButtonDown(0);
        if (input.isKeyPressed(Input.KEY_ESCAPE)) {
            GameManager.dialogManager.setShowingDialog(false);
            closeAllScreens();
            getScreenViaId(GameScreens.HUDSCREEN).isScreenActive = true;
        }

        updateOverScreen(isLeftDown, player, gc, delta, input);

        if (!showingStationScreen && !showingShipScreen) {
            runScreens(screen, isLeftDown, gc, delta, player);
        } else if (showingStationScreen && !showingShipScreen) {
            updateStationManager(isLeftDown, gc, delta);
        } else if(showingShipScreen && !showingStationScreen) {
            updateShipManager(isLeftDown, gc, delta);
        }
    }

    private void runScreens(BaseScreen screen, boolean isLeftDown, GameContainer gc, int delta, Player player) {
//        if (!showingStationScreen && !showingShipScreen) {
//            if (screen instanceof BaseScreen) {
//                ((BaseScreen) screen).updateScreen(isLeftDown, gc, delta);
//            }
//            if (screen instanceof HUDScreen) {
//                ((HUDScreen) screen).updateScreen(player, gc, delta);
//                ((HUDScreen) screen).updateScreen(isLeftDown, gc, delta);
//            }
//            if (screen instanceof MissionScreen) {
//                ((MissionScreen) screen).updateScreen(isLeftDown, gc, delta);
//            }
//            if (screen instanceof OverScreen) {
//                ((OverScreen) screen).updateScreen(player, gc, delta);
//            }
//            if (screen instanceof TargetScreen) {
//                ((TargetScreen) screen).updateScreen(isLeftDown, gc, delta);
//            }
//            if (screen instanceof MessageScreen) {
//                ((MessageScreen) screen).updateScreen(isLeftDown, gc, delta);
//            }
//        }
    }

    public void drawScreens(GameContainer gc, Graphics g) {
        if (!showingStationScreen && !showingShipScreen) {
            for (BaseScreen screen : screens) {
                if (screen.isScreenActive) {
                    screen.drawScreen(gc, g);
                }
            }
            overScreen.drawScreen(gc, g);
        } else if (showingStationScreen && !showingShipScreen) {
            stationScreenManager.drawScreens(gc, g);
        } else if(showingShipScreen && !showingStationScreen) {
            shipScreenManager.drawScreens(gc, g);
        }
    }

    public void addScreen(BaseScreen screenToAdd) {
        if (doesScreenExsist(screenToAdd)) {
            return;
        }

        screens.add(screenToAdd);
    }

    public boolean doesScreenExsist(BaseScreen screenToAdd) {
        for (BaseScreen screen : screens) {
            if (screen instanceof BaseScreen) {
                return true;
            }
        }
        return false;
    }

    public static BaseScreen getScreenByName(String nameIn) {
        for (BaseScreen screen : screens) {
            if (screen.getName().equals(nameIn)) {
                return screen;
            }
        }
        return new BaseScreen(GameScreens.BASESCREEN, "", Color.yellow);
    }

    public static BaseScreen getScreenById(GameScreens gameScreenId) {
        for (BaseScreen screen : screens) {
            if (screen.getScreenId().equals(gameScreenId)) {
                return screen;
            }
        }
        return new BaseScreen(GameScreens.BASESCREEN, "", Color.yellow);
    }

    public BaseScreen getActiveScreen() {
        for (BaseScreen screen : screens) {
            if (screen.isScreenActive) {
                return screen;
            }
        }
        return new BaseScreen(GameScreens.BASESCREEN, "", Color.yellow);
    }

    public BaseScreen getScreenViaId(GameScreens screenIn) {
//        for (BaseScreen screen : screens) {
//            if (screen.getGameScreen() == screenIn) {
//                return screen;
//            }
//        }
        return new BaseScreen(GameScreens.BASESCREEN, "", Color.yellow);
    }

    
    public void updateOverScreen(boolean leftDown, Player player, GameContainer gc, int delta, Input input) {
//        overScreen.updateScreen(leftDown, gc, delta);
//        overScreen.updateScreen(player, gc, delta);
//        if (leftDown) {
//            if (((ButtonImageElement) overScreen.getContainerById(GameScreenContainers.NAVIGATEMENU).getElementViaId(GameScreenElements.SHIP)).isButtonPressed(leftDown)) {
//                closeAllScreens();
//                showingShipScreen = true;
//            }
//            if (((ButtonImageElement) overScreen.getContainerById(GameScreenContainers.NAVIGATEMENU).getElementViaId(GameScreenElements.PLAYER)).isButtonPressed(leftDown)) {
//                closeAllScreens();
//                getScreenViaId(GameScreens.PLAYERSCREEN).isScreenActive = true;
//            }
//            if (((ButtonImageElement) overScreen.getContainerById(GameScreenContainers.NAVIGATEMENU).getElementViaId(GameScreenElements.MISSION)).isButtonPressed(leftDown)) {
//                closeAllScreens();
//                getScreenViaId(GameScreens.MISSIONSCREEN).isScreenActive = true;
//                ((MissionScreen) getScreenViaId(GameScreens.MISSIONSCREEN)).setPlayer(player);
//            }
//            if (((ButtonImageElement) overScreen.getContainerById(GameScreenContainers.NAVIGATEMENU).getElementViaId(GameScreenElements.MESSAGE)).isButtonPressed(leftDown)) {
//                closeAllScreens();
//                getScreenViaId(GameScreens.MESSAGESCREEN).isScreenActive = true;
//            }
//        }
    }

    public static void closeAllScreens() {
        for (BaseScreen screen : screens) {
            screen.isScreenActive = false;
        }
    }

    public void showMissionBox(String message) {
        overScreen.setShowMission(true, message);
    }

    public static void setTargetScreenEntity(BaseEntity entity) {
        for (BaseScreen screen : screens) {
//            if (screen instanceof TargetScreen) {
//                ((TargetScreen) screen).setEntity(entity);
//            }
        }
    }
    
    public static void setShipManagerShip(BaseShip baseShip) {
        if (baseShip != null) {
            shipScreenManager.setShip(GameManager.getPlayer().getShip());
        }
    }

    public static void showStationScreen(BaseStation baseStation) {
        closeAllScreens();
        showingStationScreen = true;
        showingShipScreen = false;
        stationScreenManager.setStation(baseStation);
    }
    public static void showShipScreen(BaseStation baseStation) {
        closeAllScreens();
        showingStationScreen = false;
        showingShipScreen = true;
        stationScreenManager.setStation(baseStation);
    }

    public static void changeScreen(GameScreens screenId) {
        closeAllScreens();
        for (BaseScreen screen : screens) {
//            if (screen.getGameScreen().equals(screenId)) {
//                screen.isScreenActive = true;
//                return;
//            }
        }
    }
    
    public static void fineScreenViaId() {
        
    }

    public void updateStationManager(boolean isLeftDown, GameContainer gc, int delta) {
        stationScreenManager.runScreen(isLeftDown, gc, delta);
    }
    public void updateShipManager(boolean isLeftDown, GameContainer gc, int delta) {
        shipScreenManager.runScreen(isLeftDown, gc, delta);
    }

    public static StationScreenManager getStationScreenManager() {
        return stationScreenManager;
    }

    public static void setStationScreenManager(StationScreenManager stationScreenManager) {
        ScreenManager.stationScreenManager = stationScreenManager;
    }

    public static ShipScreenManager getShipScreenManager() {
        return shipScreenManager;
    }

    public static void setShipScreenManager(ShipScreenManager shipScreenManager) {
        ScreenManager.shipScreenManager = shipScreenManager;
    }
    
    
}
