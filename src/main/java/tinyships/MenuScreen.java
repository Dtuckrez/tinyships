/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

/**
 *
 * @author Dean
 */
public class MenuScreen {

    public static final Vector2f START_BUTTON_POS = new Vector2f(368, 284);

    private final Image backdrop;
    private final Image startButton;

    public MenuScreen() throws SlickException {
        super();
        
        backdrop = new Image("images/drop/MainMenu.png");
        startButton = new Image("images/button/start.png");
    }

    public final boolean update(final GameContainer gc, final int delta) {
        Input input = gc.getInput();

        return input.getMouseX() >= START_BUTTON_POS.x
                && input.getMouseX() < START_BUTTON_POS.x + startButton.getWidth()
                && input.getMouseY() >= START_BUTTON_POS.y
                && input.getMouseY() < START_BUTTON_POS.y + startButton.getHeight()
                && input.isMousePressed(0);
    }

    public final void draw(final GameContainer gc, final Graphics g) {
        g.drawImage(backdrop, 0, 0);
        g.drawImage(startButton, START_BUTTON_POS.x, START_BUTTON_POS.y);
    }
}
