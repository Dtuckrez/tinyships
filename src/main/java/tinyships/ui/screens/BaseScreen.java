/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.ui.screens;

import java.util.ArrayList;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.enums.gui.GameScreenContainers;
import tinyships.enums.gui.GameScreenElements;
import tinyships.enums.gui.GameScreens;
import tinyships.objects.screen.element.BaseScreenElement;
import tinyships.objects.screen.container.BaseContainer;

/**
 *
 * @author Dean
 */
public class BaseScreen implements IBaseScreen {

    protected GameScreens screenId;
    protected String name;
    protected ArrayList<BaseScreenElement> elements = new ArrayList<>();
    protected ArrayList<BaseContainer> containers = new ArrayList<>();

    protected Color overlayColor;

    public boolean isScreenActive;

    public BaseScreen(GameScreens screenId, String name, Color color) {
        this.screenId = screenId;
        this.name = name;
        this.overlayColor = color;
    }

    public ArrayList<BaseScreenElement> getBaseScreenElements() {
        return elements;
    }

    public ArrayList<BaseContainer> getBaseContainers() {
        return containers;
    }

    public BaseContainer getContainerById(GameScreenContainers containerId) {
        for (BaseContainer container : containers) {
            if (container.containerId.equals(containerId)) {
                return container;
            }
        }
        return new BaseContainer(GameScreenContainers.WEAPON, "", 0, 0, 0, 0);
    }

    public BaseScreenElement getElementById(GameScreenElements id) {
        for (BaseScreenElement element : elements) {
            if (element.elementId == id) {
                return element;
            }
        }
        return new BaseScreenElement(GameScreenElements.BASEELEMENT);
    }

    public BaseContainer getContainerViaName(String name) {
        for (BaseContainer container : containers) {
            if (container.name.equals(name)) {
                return container;
            }
        }
        return new BaseContainer(GameScreenContainers.WEAPON, "", 0, 0, 0, 0);
    }

    public String getName() {
        return this.name;
    }

    public GameScreens getScreenId() {
        return screenId;
    }

    public void setScreenId(GameScreens screenId) {
        this.screenId = screenId;
    }

    public ArrayList<BaseScreenElement> getElements() {
        return elements;
    }

    public void setElements(ArrayList<BaseScreenElement> elements) {
        this.elements = elements;
    }

    public ArrayList<BaseContainer> getContainers() {
        return containers;
    }

    public void setContainers(ArrayList<BaseContainer> containers) {
        this.containers = containers;
    }

    public Color getOverlayColor() {
        return overlayColor;
    }

    public void setOverlayColor(Color overlayColor) {
        this.overlayColor = overlayColor;
    }

    public boolean isIsScreenActive() {
        return isScreenActive;
    }

    public void setIsScreenActive(boolean isScreenActive) {
        this.isScreenActive = isScreenActive;
    }
   
    public void updateScreen(boolean leftDown, GameContainer gc, int delta) {
        for (BaseScreenElement element : elements) {
            element.updateElement(leftDown, gc, delta);
        }

        for (BaseContainer container : containers) {
            container.updateContainer(leftDown, gc, delta);
        }
    }
    
    @Override
    public void drawScreen(GameContainer gc, Graphics g) {
        if (isScreenActive) {
            g.setColor(overlayColor);

            g.fillRect(0, 0, 800, 600);
            for (BaseScreenElement element : elements) {
                element.drawElement(gc, g);
            }
        }

        g.setColor(Color.white);
    }
}
