/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tinyships.ui.screens;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

/**
 *
 * @author User
 */
public interface IBaseScreen {
    public void updateScreen(boolean leftDown, GameContainer gc, int delta);
    public void drawScreen(GameContainer gc, Graphics g);

}
