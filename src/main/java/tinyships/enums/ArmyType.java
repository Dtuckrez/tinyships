/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.enums;

/**
 *
 * @author Dean
 */
public enum ArmyType {

    TEST(0),
    PLAYER(1);

    private int armyType = 0;

    private ArmyType(int armyType) {
        this.armyType = armyType;
    }

    public int getArmyTypeValue() {
        return armyType;
    }
}
