/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.enums.gui;

/**
 *
 * @author Dean
 */
public enum GameScreenContainers {

    BASECONTAINER(0),
    WEAPON(1),
    NAVIGATEMENU(2),
    SHIPMENU(3),
    EQUIPMENT(4),
    CARGO(5),
    CREW(6),
    PLAYERINFO(7),
    PLAYERSTATS(8),
    PLAYERSHIPS(9),
    PLAYERSTATIONS(10),
    MISSIONS(11),
    MISSIONINFO(12),
    OBJECTIVE(13),
    ENTITY(14),
    INFO(15),
    STATS(16),
    LOG(17),
    STATIONEQUIPMENT(18),
    SHIPEQUIPMENT(19),
    BUYEQUIPMENT(20),
    DIALOG(21),
    MESSAGE(22),
    LISTA(23),
    LISTB(24),
    LISTD(26),
    LISTE(27),
    LISTF(28),
    SHIPWEAPON(29);

    private int gameScreenContainer = 0;

    private GameScreenContainers(int gameScreenContainer) {
        this.gameScreenContainer = gameScreenContainer;
    }

    public int getGameScreenContainerValue() {
        return gameScreenContainer;
    }
}