/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.enums.gui;

/**
 *
 * @author Dean
 */
public enum GameScreenElements {

    BASEELEMENT(0),
    SHIPNAME(1),
    HEATHBAR(2),
    SHIELDBAR(3),
    POWERBAR(4),
    MESSAGE(5),
    MISSION(6),
    PLAYER(7),
    SHIP(8),
    SECTOR(9),
    GALAXY(10),
    SHIPTYPE(11),
    POWER(12),
    SHIELD(13),
    HULL(14),
    DESCRIPTION(15),
    LINEONE(16),
    LINETWO(17),
    LINETHREE(18),
    LINEFOUR(19),
    LINEFIVE(20),
    LINESIX(21),
    WEAPON(22),
    EQUIPMENT(23),
    CARGO(24),
    CARGOSIZE(25),
    CREW(26),
    OBJECTIVE(27),
    WEAPONEQUIPPED(28),
    EQUIPMENTEQUIPPED(29),
    STATION(30),
    INFO(31),
    DOCK(32),
    LOG(33),
    CONTACT(34),
    TRADE(35),
    CARGOVALUE(36),
    CARGOPRICE(37),
    EXIT(38),
    SLIDER(39),
    STATIONCOUNT(40),
    SHIPCOUNT(41),
    CURRENTNUMBER(42),
    TOTAL(43),
    TARGETNAME(44),
    TARGETHEALTH(45),
    TARGETSHIELD(46),
    TARGETID(47),
    SPEED(48),
    RANGE(49);

    private int gameScreenElement = 0;

    private GameScreenElements(int gameScreenElement) {
        this.gameScreenElement = gameScreenElement;
    }

    public int getGameScreenElementValue() {
        return gameScreenElement;
    }
}
