/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.enums.gui;

/**
 *
 * @author Dean
 */
public enum GameScreens {

    BASESCREEN(0),
    HUDSCREEN(1),
    SHIPSCREEN(2),
    PLAYERSCREEN(3),
    MISSIONSCREEN(4),
    TARGETSCEEN(5),
    STATIONSCREEN(7),
    STATIONINFOSCREEN(8),
    STATIONLOGSCREEN(9),
    STATIONTRADESCREEN(10),
    STATIONCREWSCREEN(11),
    MESSAGESCREEN(12),
    SHIPEQUIPMENTSCREEN(13),
    SHIPCREWSCREEN(14),
    SHIPWEAPONSCREEN(15),
    SHIPCARGOSCREEN(16);

    private int gameScreen = 0;

    private GameScreens(int gameScreen) {
        this.gameScreen = gameScreen;
    }

    public int getGameScreenContainerValue() {
        return gameScreen;
    }
}
