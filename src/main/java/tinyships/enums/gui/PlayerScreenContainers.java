/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.enums.gui;

/**
 *
 * @author Dean
 */
public enum PlayerScreenContainers {

    BASECONTAINER(0);

    private int playerScreenContainers = 0;

    private PlayerScreenContainers(int playerScreenContainers) {
        this.playerScreenContainers = playerScreenContainers;
    }

    public int getPlayerScreenContainersContainerValue() {
        return playerScreenContainers;
    }
}
