/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.enums;

/**
 *
 * @author Dean
 */
public enum StationType {

    TRADESTATION(0),
    ENERGYSTATION(1);

    private int stationType = 0;

    private StationType(int stationType) {
        this.stationType = stationType;
    }

    public int getstationTypeTypeValue() {
        return stationType;
    }
}
