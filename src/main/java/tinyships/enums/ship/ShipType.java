/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.enums.ship;

/**
 *
 * @author Mike
 */
public enum ShipType {

    PLAYER(0),
    NPC(1);

    private int shipType = 0;

    private ShipType(int shipType) {
        this.shipType = shipType;
    }

    public int getShipTypeValue() {
        return shipType;
    }
}
