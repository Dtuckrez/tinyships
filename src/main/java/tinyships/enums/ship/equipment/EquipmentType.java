/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.enums.ship.equipment;

/**
 *
 * @author Dean
 */
public enum EquipmentType {

    TEST(0),
    MINING(1);

    private int equipmentType = 0;

    private EquipmentType(int equipmentType) {
        this.equipmentType = equipmentType;
    }

    public int getEquipmentTypeValue() {
        return equipmentType;
    }
}
