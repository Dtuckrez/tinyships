/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.enums.ship.equipment;

/**
 *
 * @author Dean
 */
public enum WeaponType {

    BASEWEAPON(0),
    BASELASER(1);

    private int weaponType = 0;

    private WeaponType(int weaponType) {
        this.weaponType = weaponType;
    }

    public int getWeaponTypeValue() {
        return weaponType;
    }
}
