/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.enums;

/**
 *
 * @author Dean
 */
public enum MissionType {
    
    FLY(0),
    TRADE(1),
    COMBAT(2);
    
    private int missionType = 0;

    private MissionType(int missionType) {
        this.missionType = missionType;
    }

    public int getMissionTypeValue() {
        return missionType;
    }
}
