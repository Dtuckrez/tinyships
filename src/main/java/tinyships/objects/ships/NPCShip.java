/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.ships;

import tinyships.xml.ships.ShipData;
import java.util.Random;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Ellipse;
import tinyships.ai.actions.LoopAction;
import tinyships.ai.actions.MoveAction;
import tinyships.ai.actions.MoveToEntityAction;
import tinyships.ai.actions.SweepAttackAction;
import tinyships.objects.ships.equipment.BaseEquipment;
import tinyships.objects.ships.equipment.IBaseEquipment;

/**
 *
 * @author Dean
 */
public class NPCShip extends BaseShip implements IBaseShip {

    public Ellipse shape;

    public NPCShip(ShipData shipData) {
        super(shipData);

        //this.weapon = new BaseWeapon("", 0, 3, 4, 2);
        setImage("images/ship/enemy.png");
    }

    @Override
    public void updateShip(GameContainer gc, int delta) {
        if (isAlive) {
            ExecuteAction(delta);
            damage();
            destroyShip();
            Random random = new Random();
            if (hull <= 0) {
//                this.getEngine().getParticleEngine().emitterLocation = this.getShipCenter();
//                this.getEngine().getParticleEngine().lifeTime = 60;
//                this.getEngine().getParticleEngine().update(true);
//                getEngine().getParticleEngine().particleColours = new Color(random.nextInt(200) + 10, 45, 16, 150);
            }
        }
    }

    public void ExecuteAction(int delta) {
        if (!actions.isEmpty() && actionIndex < actions.size()) {
            if ((actions.get(actionIndex) instanceof LoopAction)) {
                if (((LoopAction) actions.get(actionIndex)).performAction()) {
                    actionIndex = 0;
                }
            }
            if (actions.get(actionIndex) instanceof MoveAction) {
                if (((MoveAction) actions.get(actionIndex)).performAction(this, delta)) {
//                    actionIndex += 1;
                }
            }
            if ((actions.get(actionIndex) instanceof MoveToEntityAction)) {
                if (this.targetEntity != null) {
                    if (((MoveToEntityAction) actions.get(actionIndex)).performAction(this, targetEntity, delta)) {

                        if (actions.get(actionIndex) instanceof MoveAction) {
                            if (((MoveAction) actions.get(actionIndex)).performAction(this, delta)) {
//                                actionIndex += 1;
                            }
                        }
                        if ((actions.get(actionIndex) instanceof MoveToEntityAction)) {
                            if (this.targetEntity != null) {
                                if (((MoveToEntityAction) actions.get(actionIndex)).performAction(this, targetEntity, delta)) {
//                        actionIndex += 1;
                                }
                            }
                        }
//            if ((actions.get(actionIndex) instanceof GatherAction)) {
//                if (this.targetEntity != null)
//                    if (((GatherAction) actions.get(actionIndex)).performAction(this, targetEntity, delta)) {
//                        actionIndex += 1;
//                    }
//            }
                        if ((actions.get(actionIndex) instanceof SweepAttackAction)) {
                            if (this.targetEntity != null) {
                                if (((SweepAttackAction) actions.get(actionIndex)).peformAction(this, this.getEShip(), delta)) {
                                    actionIndex += 1;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void drawShip(GameContainer gc, Graphics g) {
        
        if (isAlive) {
//        drawWeapons(gc, g);
//        g.draw(shape);
            for (IBaseEquipment eqp : equipmentBays) {
                ((BaseEquipment) eqp).drawEquipment(gc, g);
            }
            
            drawEngineParticles(gc, g);
            if (image != null) {
                image.setRotation(rotation);
                image.setCenterOfRotation(image.getWidth() / 2, image.getHeight() / 2);
                image.draw(position.x, position.y);
                if (collisionZone != null) {
                    g.draw(getSpeedCircle());
                }
            }
        }
    }
}
