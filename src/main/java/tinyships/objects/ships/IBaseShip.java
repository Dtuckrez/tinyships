/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.ships;

import java.util.ArrayList;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;
import tinyships.objects.entity.BaseEntity;
import tinyships.objects.ships.engines.IBaseEngine;
import tinyships.objects.ships.equipment.BaseEquipment;

/**
 *
 * @author Dean
 * @author Mike
 */
public interface IBaseShip {

    String getId();
    void setId(String id);
    
    // Getter and Setter
    String getName();
    void setName(String name);

    int getHull();
    void setHull(int hull);

    int getMaxHull();
    void setMaxHull(int maxHull);

    IBaseEngine getEngine();
    void setEngine(IBaseEngine baseEngine);

    ArrayList<BaseEquipment> getEquipmentBays();
    void setEquipmentBays(ArrayList<BaseEquipment> equipmentBays);

    int getCargoSize();
    void setCargoSize(int cargoSize);

    Image getImage();
    void setImage(String image);

    BaseEntity getTargetEntity();
    void setTargetEntity(BaseEntity entity);

    // Movement
    Vector2f getPosition();
    void setPosition(Vector2f position);

    float getRotation();
    void setRotation(float rotation);

    // Attack
    void shoot(int delta);

    // Rendering
    void drawShip(GameContainer gc, Graphics g);
    void updateShip(GameContainer gc, int delta);
}
