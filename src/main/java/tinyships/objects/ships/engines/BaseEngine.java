/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.ships.engines;

import tinyships.xml.ships.engines.EngineData;
import org.newdawn.slick.Color;
import tinyships.enums.ship.equipment.EngineSize;
import tinyships.enums.ship.equipment.EngineType;

/**
 *
 * @author Dean
 * @author Mike
 */
public class BaseEngine implements IBaseEngine {

    private String name;

    private float speed;
    private float maxSpeed;
    private float acceleration;
    private float turn;

    private EngineSize engineSize;
    private EngineType engineType;

    private Color color;

    public BaseEngine(EngineData data) {
        this.name = data.getName();
        this.speed = data.getSpeed();
        this.maxSpeed = data.getMaxSpeed();
        this.acceleration = data.getAcceleration();
        this.turn = data.getTurn();
        this.engineSize = data.getSize();
        this.engineType = data.getType();
        this.color = new Color(0, 0.09f, 0.05f, 0.07f);
    }

    public BaseEngine(String file) {
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public float getSpeed() {
        return this.speed;
    }

    @Override
    public void setSpeed(float speed) {
        this.speed = speed;
    }

    @Override
    public float getMaxSpeed() {
        return this.maxSpeed;
    }

    @Override
    public void setMaxSpeed(float maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public float getAcceleration() {
        return this.acceleration;
    }

    @Override
    public void setAcceleration(float acceleration) {
        this.acceleration = acceleration;
    }

    @Override
    public float getTurn() {
        return this.turn;
    }

    @Override
    public void setTurn(float turn) {
        this.turn = turn;
    }

    @Override
    public EngineSize getEngineSize() {
        return this.engineSize;
    }

    @Override
    public void setEngineSize(EngineSize engineSize) {
        this.engineSize = engineSize;
    }

    @Override
    public EngineType getEngineType() {
        return this.engineType;
    }

    @Override
    public void setEngineType(EngineType engineType) {
        this.engineType = engineType;
    }

    @Override
    public Color getColor() {
        return this.color;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void accelerate() {
        if (speed < maxSpeed) {
            speed += acceleration;
        } else {
            speed = maxSpeed;
        }
    }
}
