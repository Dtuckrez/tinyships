/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.ships.engines;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.enums.ship.equipment.EngineSize;
import tinyships.enums.ship.equipment.EngineType;
import tinyships.objects.particleEngine.ParticleEngine;

/**
 *
 * @author Dean
 * @author Mike
 */
public interface IBaseEngine {

    String getName();

    void setName(String name);

    float getSpeed();

    void setSpeed(float speed);

    float getMaxSpeed();

    void setMaxSpeed(float maxSpeed);

    float getAcceleration();

    void setAcceleration(float acceleration);

    float getTurn();

    void setTurn(float turn);

    EngineSize getEngineSize();

    void setEngineSize(EngineSize engineSize);

    EngineType getEngineType();

    void setEngineType(EngineType engineType);

    Color getColor();

    void setColor(Color color);

    void accelerate();
}
