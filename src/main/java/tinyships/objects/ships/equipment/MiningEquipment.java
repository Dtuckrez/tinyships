/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.ships.equipment;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Transform;
import tinyships.enums.ship.equipment.EquipmentType;
import tinyships.manager.GameManager;
import tinyships.objects.entity.BaseEntity;
import tinyships.objects.entity.astroid.BaseAstroid;
import tinyships.objects.ships.BaseShip;

/**
 *
 * @author Dean
 */
public class MiningEquipment extends BaseEquipment {

    private int collectionAmount;
    private int miningRate;

    private int currentTime = 0;

    public MiningEquipment(String name, EquipmentType type, String description) {
        super(name, description, type);
        miningRate = 2000;
        collectionAmount = 10;
    }

    @Override
    public boolean updateEquipment(BaseShip ship, int delta) {
        if (isBeingUsed) {
            if (ship.getSector() != null && ship.getEngine().getSpeed() == 0) {
                for (BaseEntity entity : ship.getSector().getEntitys()) {
                    if (entity instanceof BaseAstroid) {
                        ship.getCollisionZone().transform(Transform.createRotateTransform(ship.getRotation()));
                        if (entity.getCollisionZone().contains(ship.getCollisionZone())) {
                            currentTime += 1 * delta;
                            if (currentTime >= miningRate) {
                                System.out.println("Added Added Added");
                                ship.addCargo(((BaseAstroid)entity).getCargo(), collectionAmount);
                                ship.getShield().setPower(ship.getShield().getPower() / 2);
                                currentTime = 0;
                            }
                        } else {
                            isBeingUsed = false;
                        }
                    }
                }
            } else {
                isBeingUsed = false;
                currentTime = 0;
            }
        }
        return true;
    }

    public int getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(int currentTime) {
        this.currentTime = currentTime;
    }

    public int getCollectionAmount() {
        return collectionAmount;
    }

    public void setCollectionAmount(int collectionAmount) {
        this.collectionAmount = collectionAmount;
    }

    public int getMiningRate() {
        return miningRate;
    }

    public void setMiningRate(int miningRate) {
        this.miningRate = miningRate;
    }

    @Override
    public void drawEquipment(GameContainer gc, Graphics g) {
    }
}

/*
 if (sector != null && this.getEngine().getSpeed() == 0) {
 if (input.isKeyDown(Input.KEY_SPACE)) {
 for (BaseEntity entity : sector.getEntitys()) {
 if (entity instanceof BaseAstroid) {
 collisionZone.transform(Transform.createRotateTransform(rotation));
 if (entity.getCollisionZone().contains(this.collisionZone)) {
 mine(gc, (BaseAstroid) entity, delta);
 }
 }
 }
 } else {
 isMining = false;
 stopMining();
 }
 }
 }
 }
 }
 */
