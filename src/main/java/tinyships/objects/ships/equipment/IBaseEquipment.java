/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.ships.equipment;

import tinyships.enums.ship.equipment.EquipmentType;
import tinyships.objects.ships.BaseShip;

/**
 *
 * @author Dean
 */
public interface IBaseEquipment {

    EquipmentType getEquipmentType();

    void setEquipmentType(EquipmentType equipmentType);

    String getName();

    void setName(String name);
    
    boolean updateEquipment(BaseShip ship, int delta);
}
