/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.ships.equipment;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.enums.ship.equipment.EquipmentType;
import tinyships.objects.ships.BaseShip;

/**
 *
 * @author Dean
 */
public class BaseEquipment implements IBaseEquipment {

    protected EquipmentType type;
    protected String name;
    protected String description;
    protected boolean isBeingUsed;

    public BaseEquipment(String name, String description, EquipmentType type) {
        this.name = name;
        this.description = description;
        this.type = type;
        this.isBeingUsed = false;
    }

    @Override
    public EquipmentType getEquipmentType() {
        return this.type;
    }

    @Override
    public void setEquipmentType(EquipmentType equipmentType) {
        this.type = equipmentType;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public EquipmentType getType() {
        return type;
    }

    public void setType(EquipmentType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isIsBeingUsed() {
        return isBeingUsed;
    }

    public void setIsBeingUsed(boolean isBeingUsed) {
        this.isBeingUsed = isBeingUsed;
    }

    @Override
    public boolean updateEquipment(BaseShip ship, int delta) {
        return true;
    }

    public void drawEquipment(GameContainer gc, Graphics g) {
    }
}
