/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.ships.cargobay;

import java.util.logging.Level;
import java.util.logging.Logger;
import tinyships.objects.cargo.BaseCargo;

/**
 *
 * @author Dean
 */
public class HeldCargo {
    protected BaseCargo cargo;
    int amount;
    
    public HeldCargo () {
        
    } 

    public BaseCargo getCargo() {
        return cargo;
    }

    public void setCargo(BaseCargo cargo) {
        this.cargo = cargo;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
    
    public void addAmount(int amount) {
        this.amount += amount;
    }
    
    public void subtractAmount(int amount) {
        this.amount -= amount;
    }
    
    public void setCargoBay(BaseCargo cargo, int amount) {
        try {
            this.cargo = cargo.clone();
            this.amount = amount;
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(HeldCargo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
