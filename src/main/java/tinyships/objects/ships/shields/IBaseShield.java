/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.ships.shields;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.enums.ship.equipment.ShieldSize;
import tinyships.enums.ship.equipment.ShieldType;

/**
 *
 * @author Dean
 */
public interface IBaseShield {

    String getName();

    void setName(String name);

    ShieldSize getShieldSize();

    void setShieldSize(ShieldSize shieldSize);

    ShieldType getShieldType();

    void setShieldType(ShieldType shieldType);

    int getPower();

    void setPower(int power);

    int getMaxPower();

    void setMaxPower(int maxPower);

    int getRechargeDelay();

    void setRechageDelay(int rechargeDelay);

    int getRechargeRate();

    void setRechargeRate(int rechargeRate);

    void rechargeShield(int delta);

    void drawShield(GameContainer gc, Graphics g);
}
