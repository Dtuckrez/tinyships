/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.ships.shields;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.enums.ship.equipment.ShieldSize;
import tinyships.enums.ship.equipment.ShieldType;
import tinyships.objects.ships.equipment.BaseEquipment;
import tinyships.xml.ships.shields.ShieldData;

/**
 *
 * @author Dean
 */
public class BaseShield extends BaseEquipment implements IBaseShield {

    private ShieldType shieldType;
    private ShieldSize shieldSize;

    private int power;
    private int maxPower;

    private int rechargeDelay;
    private int currentRechargeDelay;

    private int rechargeRate;
    private int currentRechargeRate;

    public BaseShield(ShieldData data) {
        super(data.getName(), data.getDescription(), data.getEquipmentType());

        this.shieldType = data.getType();
        this.shieldSize = data.getSize();
        this.maxPower = data.getMaxPower();
        this.rechargeDelay = data.getRechargeDelay();
        this.rechargeRate = data.getRechargeRate();
        this.power = data.getPower();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public ShieldSize getShieldSize() {
        return this.shieldSize;
    }

    @Override
    public void setShieldSize(ShieldSize shieldSize) {
        this.shieldSize = shieldSize;
    }

    @Override
    public ShieldType getShieldType() {
        return this.shieldType;
    }

    @Override
    public void setShieldType(ShieldType shieldType) {
        this.shieldType = shieldType;
    }

    @Override
    public int getPower() {
        return this.power;
    }

    @Override
    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public int getMaxPower() {
        return this.maxPower;
    }

    @Override
    public void setMaxPower(int maxPower) {
        this.maxPower = maxPower;
    }

    @Override
    public int getRechargeDelay() {
        return this.rechargeDelay;
    }

    @Override
    public void setRechageDelay(int rechargeDelay) {
        this.rechargeDelay = rechargeDelay;
    }

    @Override
    public int getRechargeRate() {
        return this.rechargeRate;
    }

    @Override
    public void setRechargeRate(int rechargeRate) {
        this.rechargeRate = rechargeRate;
    }

    @Override
    public void rechargeShield(int delta) {
        if (power < maxPower) {
            currentRechargeDelay += 1 * delta;
            if (currentRechargeDelay >= rechargeDelay) {
                currentRechargeRate += 1 * delta;
                if (currentRechargeRate >= rechargeRate) {
                    currentRechargeRate = 0;
                    power += 1;
                }
            }
        } else {
            currentRechargeDelay = 0;
        }
    }

    @Override
    public void drawShield(GameContainer gc, Graphics g) {
    }
}
