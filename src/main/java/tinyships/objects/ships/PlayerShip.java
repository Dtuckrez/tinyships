/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.ships;

import tinyships.xml.ships.ShipData;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Ellipse;
import tinyships.enums.ship.equipment.EquipmentType;
import tinyships.manager.GameManager;
import tinyships.manager.ScreenManager;
import tinyships.objects.cargo.EquipmentCargo;
import tinyships.objects.entity.station.BaseStation;
import tinyships.objects.sector.item.BaseItem;
import tinyships.objects.ships.cargobay.HeldCargo;
import tinyships.objects.ships.equipment.BaseEquipment;
import tinyships.objects.ships.equipment.MiningEquipment;
import tinyships.objects.ships.weapons.BaseLaser;

/**
 *
 * @author Dean
 */
public class PlayerShip extends BaseShip implements IBaseShip {

    private int dockingTime = 0;

    public PlayerShip(ShipData shipData) {
        super(shipData);
        equipmentBaySize = 155;
        equipmentBays.add(new MiningEquipment("", EquipmentType.TEST, ""));
        for (int i = 0; i < 2; i++) {
            HeldCargo held = new HeldCargo();
            BaseLaser bl = new BaseLaser();
            bl.setName(i + " _Name");
            EquipmentCargo cargo = new EquipmentCargo(bl);
            held.setCargo(cargo);
            heldCargo.add(held);
        }
    }

    @Override
    public void updateShip(GameContainer gc, int delta) {
        if (!isDocked) {
            if (!GameManager.getDialogManger().isShowingDialog()) {
                rotate(gc, delta);
                move(gc, delta);
                changeSpeed(gc, delta);
                updateEquipment(delta);
                shoot(gc, delta);
            }
        }
//        weapon.updateProjectiles(gc, delta);
        if (baseShield != null) {
            baseShield.rechargeShield(delta);
        }

    }

    @Override
    public boolean useEquipment(int equimentBayIndex) {
        BaseEquipment equipment = equipmentBays.get(equimentBayIndex);
        if (equipment != null) {
            equipment.setIsBeingUsed(true);
        }
        return true;
    }

    public void rotate(GameContainer gc, int delta) {
        Input input = gc.getInput();
        if (input.isKeyDown(Input.KEY_D)) {
            this.rotation += getEngine().getTurn() * delta;
        }

        if (input.isKeyDown(Input.KEY_A)) {
            this.rotation -= getEngine().getTurn() * delta;
        }

        if (rotation < 0) {
            rotation = 360;
        }
        if (rotation > 360) {
            rotation = 0;
        }
    }

    public void changeSpeed(GameContainer gc, int delta) {
        Input input = gc.getInput();

        if (input.isKeyDown(Input.KEY_W)) {
            if (this.getEngine().getSpeed() < getEngine().getMaxSpeed()) {
                this.getEngine().setSpeed(
                        this.getEngine().getSpeed() + this.getEngine().getAcceleration() * delta);
            }
            if (this.getEngine().getSpeed() >= getEngine().getMaxSpeed()) {
                this.getEngine().setSpeed(getEngine().getMaxSpeed());
            }
        } else if (input.isKeyDown(Input.KEY_S)) {
            if (this.getEngine().getSpeed() > 0) {
                this.getEngine().setSpeed(
                        this.getEngine().getSpeed() - this.getEngine().getAcceleration() * delta);
            }
            if (this.getEngine().getSpeed() < 0) {
                this.getEngine().setSpeed(0);
            }
        }

        if (input.isKeyDown(Input.KEY_J)) {
            baseShield.setPower(0);
        }

        if (input.isKeyPressed(Input.KEY_COMMA)) {
            if (!sector.getEntitys().isEmpty()) {
                if (targetEntityIndex - 1 < 0) {
                    targetEntityIndex = sector.getEntitys().size() - 1;
                } else {
                    targetEntityIndex--;
                }
                setTargetEntity(sector.getEntitys().get(targetEntityIndex));
            }
        }
    }

    public void move(GameContainer gc, int delta) {
        // float angle = this.speed * delta;
        this.collisionZone = new Ellipse(
                this.image.getWidth() / 2 + this.position.x,
                this.image.getHeight() / 2 + this.position.y,
                image.getWidth() / 2,
                image.getHeight() / 2);

        this.position.x += getEngine().getSpeed() * Math.sin(Math.toRadians(rotation));
        this.position.y -= getEngine().getSpeed() * Math.cos(Math.toRadians(rotation));

        updateEngineParticles();

        dockWithStation(delta);
    }

    private void dockWithStation(int delta) {
        if (getTargetEntity() instanceof BaseStation) {

            if (((BaseStation) getTargetEntity()).getDockingPoint().contains(collisionZone)
                    && ((BaseStation) getTargetEntity()).isIsSetForDocking()) {
                if (getEngine().getSpeed() <= 0) {
                    dockingTime += 1 * delta;
                    image.setRotation(rotation += 270 * delta);
                    if (dockingTime >= 2000 && !isDocked) {
                        isDocked = true;
                        dockedStation = (BaseStation) targetEntity;
                        ScreenManager.showStationScreen(dockedStation);
                        dockingTime = 0;
                    }
                } else {
                    dockingTime = 0;
                }
            }
        }
    }

    public void shoot(GameContainer gc, int delta) {
        Input input = gc.getInput();

//        leftWeapon.setPosition(getLeftTop());
//        rightWeapon.setPosition(getRightTop());

        if (input.isKeyDown(Input.KEY_SPACE)) {
//            rightWeapon.fireWeapons(this, delta);
//            leftWeapon.fireWeapons(this, delta);
//            forwardWeapon.fireWeapons(this, delta);
//            rearWeapon.fireWeapons(this, delta);
        } else {
//            rightWeapon.cancelFireWeapons(this);
//            leftWeapon.cancelFireWeapons(this);
//            forwardWeapon.cancelFireWeapons(this);
//            rearWeapon.cancelFireWeapons(this);
        }
    }

    public void pickUpItem(BaseItem item) {
        if (collisionZone.intersects(item.getCollisionZone())) {
            if (item.isIsAlive()) {
                addCargo(item.getCargo(), 1);
                item.destroy();
            }
        }
    }

    @Override
    public void drawShip(GameContainer gc, Graphics g) {
        if (!isDocked) {
            if (baseShield != null) {
                baseShield.drawShield(gc, g);
            }

            drawEngineParticles(gc, g);
            if (image != null) {
                image.setRotation(rotation);
                image.setCenterOfRotation(image.getWidth() / 2, image.getHeight() / 2);
                image.draw(position.x, position.y);
            }
        }
    }
}
