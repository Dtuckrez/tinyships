/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.ships.weapons.projectile;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Ellipse;
import org.newdawn.slick.geom.Vector2f;
import tinyships.enums.ArmyType;
import tinyships.objects.particleEngine.ParticleEngine;

/**
 *
 * @author Dean
 */
public class BaseProjectile implements IBaseProjectile {

    protected Vector2f position;
    protected float speed;
    protected float angle;
    protected float lifeTime;
    protected boolean isAlive;

    protected ParticleEngine particleEngine;
    protected ArmyType armyType;

    public BaseProjectile() {
        position = new Vector2f();
        speed = 3f;
        isAlive = false;
        lifeTime = 1000;
        angle = 0;
        particleEngine = new ParticleEngine(Color.yellow, 15, 15, 5, isAlive);
    }

    @Override
    public Vector2f getPosition() {
        return this.position;
    }

    @Override
    public void setPosiion(Vector2f position) {
        this.position = position;
    }

    @Override
    public float getSpeed() {
        return this.speed;
    }

    @Override
    public void setSpeed(float speed) {
        this.speed = speed;
    }

    @Override
    public float getAngle() {
        return this.angle;
    }

    @Override
    public void setAngle(float angle) {
        this.angle = angle;
    }

    @Override
    public float getLifeTime() {
        return this.lifeTime;
    }

    @Override
    public void setLifeTime(float lifeTime) {
        this.lifeTime = lifeTime;
    }

    @Override
    public boolean isAlive() {
        return this.isAlive;
    }

    @Override
    public void setIsAlive(boolean isAlive) {
        this.isAlive = isAlive;
    }

    @Override
    public ParticleEngine getParticleEngine() {
        return this.particleEngine;
    }

    @Override
    public void setParticleEngine(ParticleEngine particleEngine) {
        this.particleEngine = particleEngine;
    }

    public ArmyType getArmyType() {
        return this.armyType;
    }

    public void setArmyType(ArmyType armyType) {
        this.armyType = armyType;
    }

    public void move(int delta) {
        if (isAlive) {
            particleEngine.isActive = true;
            position.x += (speed * Math.sin(Math.toRadians(angle)));
            position.y -= (speed * Math.cos(Math.toRadians(angle)));

            particleEngine.setEmitterLocation(new Vector2f(position));

            lifeTime -= 1 * delta;        
            particleEngine.update();
        }

        if (lifeTime <= 0) {
            isAlive = false;
            particleEngine.isActive = false;
            particleEngine.particles.clear();

        }
    }

    public void reset(BaseProjectile baseProjectile) {
        setAngle(baseProjectile.getAngle());
        setPosiion(new Vector2f(baseProjectile.position));
        setSpeed(baseProjectile.speed);
        setArmyType(baseProjectile.getArmyType());
        isAlive = true;
        this.lifeTime = baseProjectile.lifeTime;
    }

    public Ellipse getCollisionZone() {
        return new Ellipse(position.x, position.y, 4, 4);
    }

    public void draw(GameContainer gc, Graphics g) {
        if (isAlive) {
            g.drawString(String.valueOf(lifeTime), position.x, position.y - 35);
            particleEngine.draw(gc, g);
        }
    }
}
