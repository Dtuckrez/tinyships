/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tinyships.objects.ships.weapons.projectile;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;

/**
 *
 * @author Dean
 */
public class LaserBeam extends BaseProjectile {

    Vector2f startPosition;
    Vector2f finishPosition;
    float currentTravel = 0;

    boolean isGrowing = false;

    public LaserBeam() {
        super();
    }

    @Override
    public Vector2f getPosition() {
        return this.position;
    }

    @Override
    public void setPosiion(Vector2f position) {
        this.position = new Vector2f(position);
    }
    
    public Vector2f getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(Vector2f startPosition) {
        this.startPosition = new Vector2f(startPosition);
    }

    public Vector2f getFinishPosition() {
        return finishPosition;
    }

    public void setFinishPosition(Vector2f finishPosition) {
        this.finishPosition = new Vector2f(finishPosition);
    }

    public void setStartAndCalcualteFinishPosition(Vector2f start, float speed, float lifeTime) {
        this.startPosition = new Vector2f(start);
        this.position = new Vector2f(start);
        this.speed = speed;
        this.lifeTime = lifeTime;

        finishPosition = new Vector2f(start);
        
        isGrowing = true;
    }

    @Override
    public void move(int delta) {
        if (isAlive) {
            if (isGrowing) {
                currentTravel += speed;
                finishPosition.x += (speed * Math.sin(Math.toRadians(angle)));
                finishPosition.y -= (speed * Math.cos(Math.toRadians(angle)));
                if (currentTravel >= 300) {
                    isGrowing = false;
                    currentTravel = 0;
                }
            } else {
                currentTravel += speed;
                position.x += (speed * Math.sin(Math.toRadians(angle)));
                position.y -= (speed * Math.cos(Math.toRadians(angle)));
                if (currentTravel >= 300) {
                    isAlive = false;
                }
            }
        }
    }
    
    @Override
    public void reset(BaseProjectile baseProjectile) {
//        super.reset(baseProjectile);
        setAngle(baseProjectile.getAngle());
        setFinishPosition(baseProjectile.position);
        setStartPosition(baseProjectile.position);
        setPosiion(baseProjectile.position);
        setSpeed(baseProjectile.speed);
        setArmyType(baseProjectile.getArmyType());
        isAlive = true;
        isGrowing = true;
        currentTravel = 0.0f;
        this.lifeTime = baseProjectile.lifeTime;
    }

    @Override
    public void draw(GameContainer gc, Graphics g) {
        if (isAlive) {
            g.drawString(String.valueOf(finishPosition.x), startPosition.x, startPosition.y);
            g.setColor(Color.blue);
            g.setLineWidth(4);
//            g.drawLine(startPosition.x, startPosition.y, startPosition.x + 50, finishPosition.y + 50);
            g.drawLine(position.x, position.y, finishPosition.x, finishPosition.y);
        }
    }
}
