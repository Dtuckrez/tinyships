/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.ships.weapons.projectile;

import org.newdawn.slick.geom.Vector2f;
import tinyships.objects.particleEngine.ParticleEngine;

/**
 *
 * @author Dean
 */
public interface IBaseProjectile {

    Vector2f getPosition();

    void setPosiion(Vector2f position);

    float getSpeed();

    void setSpeed(float speed);

    float getAngle();

    void setAngle(float angle);

    float getLifeTime();

    void setLifeTime(float lifeTime);

    boolean isAlive();

    void setIsAlive(boolean isAlive);

    ParticleEngine getParticleEngine();

    void setParticleEngine(ParticleEngine particleEngine);
}
