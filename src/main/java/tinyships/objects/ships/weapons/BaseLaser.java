/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tinyships.objects.ships.weapons;

import java.util.Random;
import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Vector2f;
import tinyships.manager.ProjectileManager;
import tinyships.objects.ships.BaseShip;
import tinyships.objects.ships.weapons.projectile.BaseProjectile;
import tinyships.objects.ships.weapons.projectile.LaserBeam;
import tinyships.xml.ships.weapons.WeaponData;

/**
 *
 * @author Dean
 */
public class BaseLaser extends BaseWeapon {
    
    public BaseLaser(WeaponData data) {
        super(data);
        this.color = Color.orange;
        setImage("images/weapons/" + "1.png");
        Random random = new Random();
        this.description = "Ohh Laser " + random.nextInt(1244) * 34 ;
    }
    
    public BaseLaser() {
        super();
        this.color = Color.orange;
        setImage("images/weapons/" + 0 + ".png");
        Random random = new Random();
        this.description = "Ohh Laser " + random.nextInt(1244) * 34 ;

    }
    
    @Override
    public void fireWeapon(Vector2f position, BaseShip ship, int delta) {
        if (currentFireTime < fireTime) {
            currentFireTime += 1 * delta;
        }

        if (currentFireTime >= fireTime) {
            currentFireTime = 0;

            //Create a new projectile if we have none
            ProjectileManager.addNewProjectile(createNewProjectile(position, ship));
            // Check if projectile is dead if so reset it
        }
    }
    
    @Override
    public BaseProjectile createNewProjectile(Vector2f position, BaseShip ship) {
        LaserBeam projectile = new LaserBeam();
        projectile.setIsAlive(true);
        projectile.setAngle(ship.getRotation());
        projectile.setStartAndCalcualteFinishPosition(position, 100, 100);
        projectile.setSpeed(8f + ship.getEngine().getSpeed());
        projectile.setArmyType(ship.getArmyType());

        return projectile;
    }
}
