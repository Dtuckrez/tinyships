/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.ships.weapons;

import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Vector2f;
import tinyships.enums.ship.equipment.WeaponType;
import tinyships.objects.ships.BaseShip;

/**
 *
 * @author Dean
 */
public interface IBaseWeapon {

    String getName();

    void setName(String name);

    WeaponType weaponType();

    void setWeaponType(WeaponType weaponType);

    int getHullDamage();

    void setHullDamage(int hullDamage);

    int getShieldDamage();

    void setShieldDamage(int hullDamage);

    float getSpeed();

    void setSpeed(float speed);

    int getLifeTime();

    void setLifeTime(int lifeTime);

    int getFireTime();

    void setFireTime(int fireTime);

    int getCurrentFireTime();

    void setCurrentFireTime(int currentFireTime);

    Color getColor();

    void setColor(Color color);

    void fireWeapon(Vector2f position, BaseShip ship, int delta);
}
