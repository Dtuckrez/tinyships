/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.ships.weapons;

import org.newdawn.slick.geom.Vector2f;
import tinyships.objects.ships.BaseShip;
import tinyships.xml.ships.weapons.WeaponBayData;

/**
 *
 * @author Dean
 */
public class BaseWeaponBay {
    
    private BaseWeapon baseWeapon;
    private Vector2f position;

    public BaseWeaponBay(WeaponBayData data) {
        super();
        baseWeapon = new BaseWeapon(data.getWeapon());
//        baseWeapon = new BaseLaser(data.getWeapon());
    }

    public BaseWeapon getBaseWeapon() {
        return baseWeapon;
    }

    public void setBaseWeapon(BaseWeapon baseWeapon) {
        this.baseWeapon = baseWeapon;
    }
    
    public Vector2f getPosition() {
        return position;
    }

    public void setPosition(Vector2f position) {
        this.position = position;
    }

    public void fireWeapons(BaseShip ship, int delta) {
        if (baseWeapon != null) {
            baseWeapon.fireWeapon(position, ship, delta);
        }
    }

    public void cancelFireWeapons(BaseShip ship) {
        if (baseWeapon != null) {
            baseWeapon.setCurrentFireTime(0);
        }
    }
}
