/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.ships.weapons;

import org.newdawn.slick.Color;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Ellipse;
import org.newdawn.slick.geom.Vector2f;
import tinyships.enums.ship.equipment.EquipmentType;
import tinyships.enums.ship.equipment.WeaponType;
import tinyships.manager.ProjectileManager;
import tinyships.objects.ships.BaseShip;
import tinyships.objects.ships.equipment.BaseEquipment;
import tinyships.objects.ships.weapons.projectile.BaseProjectile;
import tinyships.xml.ships.weapons.WeaponData;

/**
 *
 * @author Dean
 */
public class BaseWeapon extends BaseEquipment implements IBaseWeapon, Cloneable {

    protected Image image;

    protected WeaponType weaponType;

    protected int hullDamage;
    protected int shieldDamage;

    private float speed;

    protected int lifeTime;

    protected Color color;

    protected int fireTime;
    protected int currentFireTime;

    public BaseWeapon(WeaponData data) {
        super(data.getName(), data.getDescription(), data.getEquipmentType());

        this.weaponType = data.getType();
        this.hullDamage = data.getHullDamage();
        this.shieldDamage = data.getShieldDamage();
        this.speed = data.getSpeed();
        this.lifeTime = data.getLifeTime();
        this.fireTime = data.getFireTime();

        this.color = Color.orange;

        String imagePath = null;
        if (weaponType != null) {
            imagePath = "images/weapons/" + 0 + ".png";
        } else {
            imagePath = "images/weapons/" + 0 + ".png";

        }

        setImage(imagePath);
    }

    public BaseWeapon() {
        super("=-=", "=-=", EquipmentType.TEST);
        this.weaponType = WeaponType.BASEWEAPON;
        this.hullDamage = 5;
        this.shieldDamage = 5;
        this.speed = 12;
        this.lifeTime = 3;
        this.fireTime = 2;

        this.color = Color.orange;
        setImage("images/weapons/" + weaponType.getWeaponTypeValue() + ".png");
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public Image getImage() {
        return image;
    }

    public final void setImage(String imageFile) {
        try {
            image = new Image(imageFile);
        } catch (SlickException ex) {
//            Logger.getLogger(BaseWeapon.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public WeaponType weaponType() {
        return weaponType;
    }

    @Override
    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }

    @Override
    public int getHullDamage() {
        return 4;
    }

    @Override
    public void setHullDamage(int hullDamage) {
        this.hullDamage = hullDamage;
    }

    @Override
    public int getShieldDamage() {
        return shieldDamage;
    }

    @Override
    public void setShieldDamage(int hullDamage) {
        this.hullDamage = hullDamage;
    }

    @Override
    public float getSpeed() {
        return speed;
    }

    @Override
    public void setSpeed(float speed) {
        this.speed = speed;
    }

    @Override
    public int getLifeTime() {
        return lifeTime;
    }

    @Override
    public void setLifeTime(int lifeTime) {
        this.lifeTime = lifeTime;
    }

    @Override
    public int getFireTime() {
        return fireTime;
    }

    @Override
    public void setFireTime(int fireTime) {
        this.fireTime = fireTime;
    }

    @Override
    public int getCurrentFireTime() {
        return currentFireTime;
    }

    @Override
    public void setCurrentFireTime(int currentFireTime) {
        this.currentFireTime = currentFireTime;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void fireWeapon(Vector2f position, BaseShip ship, int delta) {
        if (currentFireTime < fireTime) {
            currentFireTime += 1 * delta;
        }

        if (currentFireTime >= fireTime) {
            currentFireTime = 0;

            //Create a new projectile if we have none
            ProjectileManager.addNewProjectile(createNewProjectile(position, ship));
            // Check if projectile is dead if so reset it
        }
    }

    public BaseProjectile createNewProjectile(Vector2f position, BaseShip ship) {
        BaseProjectile projectile = new BaseProjectile();
        projectile.setIsAlive(true);
        projectile.setAngle(ship.getRotation());
        projectile.setPosiion(position);
        projectile.setSpeed(8f);
        projectile.setArmyType(ship.getArmyType());

        return projectile;
    }

    public Ellipse getWeaponRangeCircle(BaseShip ship, int delta) {
        Ellipse weaponRange = new Ellipse(
                ship.getShipCenter().x,
                ship.getShipCenter().y,
                this.lifeTime * 2, this.lifeTime * 2);

        return weaponRange;
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
