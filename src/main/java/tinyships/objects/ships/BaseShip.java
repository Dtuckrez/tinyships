/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.ships;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Ellipse;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;
import tinyships.enums.ArmyType;
import tinyships.enums.ship.equipment.EquipmentType;
import tinyships.ai.actions.BaseAction;
import tinyships.manager.GameManager;
import tinyships.manager.ProjectileManager;
import tinyships.objects.cargo.BaseCargo;
import tinyships.objects.cargo.EquipmentCargo;
import tinyships.objects.entity.BaseEntity;
import tinyships.objects.entity.station.BaseStation;
import tinyships.objects.particleEngine.ParticleEngine;
import tinyships.objects.sector.BaseSector;
import tinyships.objects.ships.cargobay.HeldCargo;
import tinyships.objects.ships.engines.BaseEngine;
import tinyships.objects.ships.engines.IBaseEngine;
import tinyships.objects.ships.equipment.BaseEquipment;
import tinyships.objects.ships.equipment.IBaseEquipment;
import tinyships.objects.ships.shields.BaseShield;
import tinyships.objects.ships.shields.IBaseShield;
import tinyships.objects.ships.weapons.BaseWeapon;
import tinyships.objects.ships.weapons.BaseWeaponBay;
import tinyships.objects.ships.weapons.projectile.BaseProjectile;
import tinyships.xml.ships.ShipData;

/**
 *
 * @author Dean
 * @author Mike
 */
public class BaseShip implements IBaseShip {

    protected String id;
    protected String name;
    protected String description;
    protected ArmyType armyType;
    protected int hull, maxHull;

    protected IBaseShield baseShield;
    protected IBaseEngine baseEngine;

    protected int equipmentBaySize;
    protected ArrayList<BaseEquipment> equipmentBays = new ArrayList<>();
    protected ArrayList<BaseWeaponBay> weaponBays = new ArrayList<>();

    protected boolean isEquipmentUpdated;

    protected int cargoSize = 25;
    protected ArrayList<HeldCargo> heldCargo = new ArrayList<>();

    protected Image image;

    protected ArrayList<BaseAction> actions = new ArrayList<>();
    protected int actionIndex;

    protected BaseEntity targetEntity;
    protected int targetEntityIndex;
    protected BaseShip eShip;

    protected boolean isMining;
    
    protected boolean isDocked;
    protected BaseStation dockedStation;
    protected BaseSector sector;

    protected Vector2f position;
    protected float rotation;

    protected Shape collisionZone;

    protected boolean isAlive = true;

    protected int engineAmount = 0;
    protected ParticleEngine[] particleEngines;

    public BaseShip(ShipData data) {
        this.name = data.getName();
        this.description = data.getDescription();
        this.armyType = data.getArmyType();
        this.maxHull = data.getHull();
        this.hull = maxHull - 20;
      
        
        this.baseShield = new BaseShield(data.getShield());
        this.baseEngine = new BaseEngine(data.getEngine());
//        this.weaponBays = data.getWeaponBays();

        this.equipmentBaySize = 1;
        //equipmentBays.add(new MiningEquipment(EquipmentType.MINING, ""));
        this.isEquipmentUpdated = true;
        
        this.position = new Vector2f();
        setImage("images/ship/" + data.getId() + ".png");

        engineAmount = data.getEngineAmount();
        particleEngines = new ParticleEngine[engineAmount];
        setupEngine();

    }

    public final void setupEngine() {
        for (int i = 0; i < engineAmount; i++) {
            particleEngines[i] = new ParticleEngine(baseEngine.getColor(), 25, 7, 6, true);
        }
    }

    public void genorateId(int x, int y) {
        this.id = ""
                + getName().toLowerCase().substring(0, 3)
                + GameManager.useIdNumber()
                + this.getArmyType().getArmyTypeValue() + x + y;
    }

// <editor-fold defaultstate="collapsed" desc="Getter and Setter">
    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArmyType getArmyType() {
        return armyType;
    }

    public void getArmyType(ArmyType armyType) {
        this.armyType = armyType;
    }

    @Override
    public int getHull() {
        return hull;
    }

    @Override
    public void setHull(int hull) {
        this.hull = hull;
    }

    @Override
    public int getMaxHull() {
        return maxHull;
    }

    @Override
    public void setMaxHull(int maxHull) {
        this.maxHull = maxHull;
    }

    public IBaseShield getShield() {
        return baseShield;
    }

    public void setShield(BaseShield shield) {
        this.baseShield = shield;
    }

    @Override
    public IBaseEngine getEngine() {
        return baseEngine;
    }

    @Override
    public void setEngine(IBaseEngine baseEngine) {
        this.baseEngine = baseEngine;
    }

    public int getEquipmentBaySize() {
        return equipmentBaySize;
    }

    public void setEquipmentBaySize(int equipmentBaySize) {
        this.equipmentBaySize = equipmentBaySize;
    }

    @Override
    public ArrayList<BaseEquipment> getEquipmentBays() {
        return equipmentBays;
    }

    @Override
    public void setEquipmentBays(ArrayList<BaseEquipment> equipmentBays) {
        this.equipmentBays = equipmentBays;
    }

    public boolean getIsEquipmentUpdated() {
        return isEquipmentUpdated;
    }

    public void setIsEquipmentUpdated(boolean isEquipmentUpdated) {
        this.isEquipmentUpdated = isEquipmentUpdated;
    }
    
    @Override
    public int getCargoSize() {
        int size = 0;
        for (HeldCargo cargo : heldCargo) {
           size = size += cargo.getAmount();
        }
        return size;
    }

    @Override
    public void setCargoSize(int cargoSize) {
        this.cargoSize = cargoSize;
    }

    public ArrayList<HeldCargo> getCargo() {
        return heldCargo;
    }

    public void setCargo(ArrayList<HeldCargo> heldCargo) {
        this.heldCargo = heldCargo;
    }

    @Override
    public Image getImage() {
        return image;
    }

    @Override
    public final void setImage(String imageFile) {
        try {
            image = new Image(imageFile);
        } catch (SlickException ex) {
            Logger.getLogger(BaseShip.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<BaseAction> getActions() {
        return actions;
    }

    public void setActions(ArrayList<BaseAction> actions) {
        this.actions = actions;
    }

    public int getActionIndex() {
        return actionIndex;
    }

    public void setActionIndex(int actionIndex) {
        this.actionIndex = actionIndex;
    }

    @Override
    public BaseEntity getTargetEntity() {
        return targetEntity;
    }

    @Override
    public void setTargetEntity(BaseEntity entity) {
        this.targetEntity = entity;
    }

    public int getTargetEntityIndex() {
        return targetEntityIndex;
    }

    public void setTargetEntityIndex(int targetEntityIndex) {
        this.targetEntityIndex = targetEntityIndex;
    }

    public BaseShip getEShip() {
        return eShip;
    }

    public void setEShip(BaseShip ship) {
        this.eShip = ship;
    }

    public boolean getIsMining() {
        return isMining;
    }

    public void setIsMining(boolean isMining) {
        this.isMining = isMining;
    }

    public boolean getIsDocked() {
        return isDocked;
    }

    public void setIsDocked(boolean isDocked) {
        this.isDocked = isDocked;
    }

    public BaseStation getDockedStation() {
        return dockedStation;
    }

    public void setDockedStation(BaseStation dockedStation) {
        this.dockedStation = dockedStation;
    }

    public BaseSector getSector() {
        return sector;
    }

    public void setSector(BaseSector sector) {
        this.sector = sector;
    }

    public boolean isIsAlive() {
        return isAlive;
    }

    public void setIsAlive(boolean isAlive) {
        this.isAlive = isAlive;
    }

    public Vector2f getLeftTop() {
        double h = Math.sqrt((image.getWidth() / 2 * image.getWidth() / 2) + (image.getHeight() / 2 * image.getHeight() / 2));

        double centerx = position.x + image.getWidth() / 2;
        double centery = position.y + image.getHeight() / 2;

        float angle = (float) (rotation + 180 + (180 / Math.PI * Math.atan(image.getHeight() / image.getWidth())));

        float x = (float) (centerx + h * Math.cos(Math.toRadians(angle)));
        float y = (float) (centery + h * Math.sin(Math.toRadians(angle)));

        return new Vector2f(x, y);
    }

    public Vector2f getRightTop() {
        double h = Math.sqrt((image.getWidth() / 2 * image.getWidth() / 2) + (image.getHeight() / 2 * image.getHeight() / 2));

        double centerx = position.x + image.getWidth() / 2;
        double centery = position.y + image.getHeight() / 2;

        float angle = (float) (rotation + (180 / Math.PI * Math.atan(image.getHeight() / image.getWidth())));

        float x = (float) (centerx + h * Math.cos(Math.toRadians(angle)));
        float y = (float) (centery + h * Math.sin(Math.toRadians(angle)));

        return new Vector2f(x, y);
    }

// </editor-fold>
    
// <editor-fold desc="Attack">
    @Override
    public void shoot(int delta) {
        // weapon.fire(this, delta);
    }
// </editor-fold>

// <editor-fold desc="Equipment">
    public boolean checkIfHasEquipment(EquipmentType type) {
        for (IBaseEquipment equipmentCheck : equipmentBays) {
            if (equipmentCheck.getEquipmentType().equals(type)) {
                return true;
            }
        }
        return false;
    }

    public void updateEquipment(int delta) {
        for (IBaseEquipment equipmentBay : equipmentBays) {
            if (equipmentBay != null) {
                equipmentBay.updateEquipment(this, delta);
            }
        }
    }

    public boolean useEquipment(int equimentBayIndex) {
        return false;
    }
// </editor-fold>

// <editor-fold desc="Cargo">
    public HeldCargo compareCargo(BaseCargo cargoToMatch) {
        for (HeldCargo cargo : heldCargo) {
            if (cargo.getCargo().getId() == cargoToMatch.getId()) {
                return cargo;
            }
        }
        return new HeldCargo();
    }

    public void addCargo(BaseCargo cargoToAdd, int amount) {
        boolean duplicateItem = false;
        if (!(cargoToAdd instanceof EquipmentCargo)) {
            for (HeldCargo cargo : heldCargo) {
                if (cargo.getCargo().getId() == cargoToAdd.getId()) {
                    duplicateItem = true;
                    int currentCargoSize = getCargoSize();
                    if (amount + currentCargoSize > cargoSize) {
                        amount = (amount + currentCargoSize) - cargoSize;
                        cargo.addAmount(amount);
                        return;
                    }
                }
            }
        }
        if (!duplicateItem) {
            HeldCargo cargo = new HeldCargo();
            if (cargoToAdd instanceof EquipmentCargo) {
                if (getCargoSize() < cargoSize) {
                    cargo.setCargo(cargoToAdd);
                    cargo.setAmount(1);
                }
            } else {
                cargo.setCargo(cargoToAdd);
                if (amount + getCargoSize() > cargoSize) {
                    amount = (amount + getCargoSize()) - cargoSize;
                }
                cargo.setAmount(amount);
            }
            heldCargo.add(cargo);
        }
    }
// </editor-fold>

// <editor-fold desc="Movement">
    @Override
    public Vector2f getPosition() {
        return position;
    }

    @Override
    public void setPosition(Vector2f position
    ) {
        this.position = position;
    }

    @Override
    public float getRotation() {
        return rotation;
    }

    @Override
    public void setRotation(float rotation
    ) {
        this.rotation = rotation;
    }

    public Vector2f getShipCenter() {
        float x = position.x + image.getWidth() / 2;
        float y = position.y + image.getHeight() / 2;
        return new Vector2f(x, y);
    }

    public Ellipse getSpeedCircle() {
        int width = image.getWidth();
        int height = image.getHeight();
        float x = position.x + width / 2;
        float y = position.y + height / 2;
        return new Ellipse(x, y, width, height);
    }

    public Shape getCollisionZone() {
        int width = image.getWidth();
        int height = image.getHeight();
        float x = position.x + width / 2;
        float y = position.y + height / 2;
        return new Ellipse(x, y, width / 2, height / 2);
    }

    public void updateEngineParticles() {

        if (engineAmount == 1) {
            particleEngines[0].setEmitterLocation(getShipCenter());
        } else if (engineAmount == 2) {
            particleEngines[0].setEmitterLocation(getLeftTop());
            particleEngines[1].setEmitterLocation(getRightTop());
        } else if (engineAmount == 3) {
            particleEngines[0].setEmitterLocation(getLeftTop());
            particleEngines[1].setEmitterLocation(getRightTop());
            particleEngines[2].setEmitterLocation(getShipCenter());
        }

        for (ParticleEngine particleEngine : particleEngines) {
            particleEngine.update(true);
        }
    }
// </editor-fold>

// <editor-fold desc="Damage">
    public void damage() {
        for (BaseProjectile projectile : ProjectileManager.projectiles) {
            if (projectile.isAlive() && projectile.getArmyType() != getArmyType()) {
                if (projectile.getCollisionZone().intersects(getCollisionZone())) {
                    projectile.setIsAlive(false);
                    hull -= 10;
                }
            }
        }
    }

    public void destroyShip() {
        if (hull <= 0) {
            isAlive = false;
        }
    }
// </editor-fold>

// <editor-fold desc="Rendering">
    @Override
    public void drawShip(GameContainer gc, Graphics g) {
        if (isAlive) {
            if (image != null) {
                image.setRotation(rotation);
                image.setCenterOfRotation(image.getWidth() / 2, image.getHeight() / 2);
            }
        }
    }

    public void drawEngineParticles(GameContainer gc, Graphics g) {
        for (ParticleEngine particleEngine : particleEngines) {
            particleEngine.draw(gc, g);
        }
    }

    @Override
    public void updateShip(GameContainer gc, int delta) {
        float speed = baseEngine.getSpeed();
        if (speed > 0) {
            double radians = Math.toRadians(rotation);
            position.x += speed * Math.sin(radians);
            position.y -= speed * Math.cos(radians);
        }
    }
    
    public void swapWeapons(BaseWeapon weaponA, BaseWeapon weaponB) {
        System.out.println("tinyships.objects.ships.BaseShip.swapWeapons()");
        if (weaponA != null && weaponB != null) {
            try {
                BaseWeapon weaponC = (BaseWeapon) weaponA.clone();
                weaponA = (BaseWeapon) weaponB.clone();
                weaponB = (BaseWeapon) weaponC.clone();
            } catch (CloneNotSupportedException ex) {
                Logger.getLogger(BaseShip.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
// </editor-fold>
}
