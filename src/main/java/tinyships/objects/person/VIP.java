/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.person;

import tinyships.enums.Race;

/**
 *
 * @author Dean
 */
public class VIP extends BasePerson{

    public VIP(String name, Race race) {
        super(name, race);
        this.isOnStation = true;
    }
    
    @Override
    public void updateBasePerson(int delta) {
        
    }
}
