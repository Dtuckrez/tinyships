/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.person;

import java.util.Random;
import tinyships.enums.Race;
import tinyships.game.mission.Mission;
import tinyships.game.mission.generator.FlyMissionGenerator;
import tinyships.manager.StationScreenManager;

/**
 *
 * @author Dean
 */
public class BasePerson {
    
    private Random random;

    protected String name;
    protected Race race;

    protected boolean hasMission = false;
    protected Mission mission;

    protected boolean isOnStation;

    protected int newMissionTimer;

    public BasePerson(String name, Race race) {
        this.name = name;
        this.race = race;
        this.isOnStation = true;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Race getRace() {
        return this.race;
    }

    public void setRace(Race race) {
        this.race = race;
    }

    public boolean isHasMission() {
        return hasMission;
    }

    public void setHasMission(boolean hasMission) {
        this.hasMission = hasMission;
    }

    public Mission getMission() {
        return mission;
    }

    public void setMission(Mission mission) {
        this.mission = mission;
    }

    public void giveMission() {
        hasMission = true;
    }

    public void updateBasePerson(int delta) {
        random = new Random();
        if (newMissionTimer == 0) {
            int num =  (random.nextInt(10) + 1) * 60000;
            newMissionTimer = num;
        }
        
        
        if (isOnStation) {
            newMissionTimer -= delta * 1;
            if (newMissionTimer <= 0) {
                mission = null;
                if (random.nextInt(100) < 33) {
                    System.out.println("MISSION");
                    hasMission = true;
                    genMission();
                }
                newMissionTimer = random.nextInt(10) + 1 * 60000;
                StationScreenManager.getCrewScreen().setUpdateCrew(true);
            }
        }
    }

    public void genMission() {

        if (hasMission && mission == null) {
            // Genorate a random mission
            Random random = new Random(3);
            /* FLY(0), TRADE(1),COMBAT(2);
             */

            int missionType = random.nextInt(3);
            switch (missionType) {
                case 0:
                    mission = FlyMissionGenerator.GenerateMission();
                    break;
                case 1:
                    mission = FlyMissionGenerator.GenerateMission();
                    break;
                case 2:
                    mission = FlyMissionGenerator.GenerateMission();
                    break;
                default:
                    mission = FlyMissionGenerator.GenerateMission();
                    break;
            }

        }
    }
}
