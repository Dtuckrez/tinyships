/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.sector;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Ellipse;
import org.newdawn.slick.geom.Shape;
import tinyships.ai.actions.MoveAction;
import tinyships.enums.StationType;
import tinyships.objects.entity.BaseEntity;
import tinyships.objects.entity.astroid.OreAstroid;
import tinyships.objects.entity.station.BaseStation;
import tinyships.objects.entity.station.EnergyStation;
import tinyships.objects.player.Player;
import tinyships.objects.sector.item.BaseItem;
import tinyships.objects.ships.BaseShip;
import tinyships.objects.ships.NPCShip;
import tinyships.xml.ShipLoader;
import tinyships.xml.astroid.AstroidData;
import tinyships.xml.astroid.AstroidsData;
import tinyships.xml.astroid.StationsData;
import tinyships.xml.sector.SectorData;
import tinyships.xml.station.StationData;

/**
 *
 * @author Dean
 */
public class BaseSector {

    protected int id;
    protected Image image;
    protected String name;
    protected String description;

    protected Shape bounds;

    protected int size;

    protected ArrayList<BaseEntity> entitys = new ArrayList<>();
    protected ArrayList<BaseItem> items = new ArrayList<>();
    protected ArrayList<BaseShip> ships = new ArrayList<>();

    public BaseSector(SectorData data) {
        try {
            this.id = data.getId();
            this.name = data.getName();
            this.description = data.getDescription();
            this.image = new Image("images/sector/" + data.getImageNumber() + ".png");
            this.size = data.getSize();
            this.bounds = new Ellipse(0, 0, size, size);
            loadStations(data.getStations());
            loadAstroids(data.getAstroidData());
            
        } catch (SlickException ex) {
            Logger.getLogger(BaseSector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadStations(StationsData data) {
        for (StationData d : data.getStations()) {
            if (d.getStationType().equals(StationType.TRADESTATION)) {
                entitys.add(new BaseStation(d.getName(), d.getxpos(), d.getypos(), d.selectable()));
            }
            if (d.getStationType().equals(StationType.ENERGYSTATION)) {
                entitys.add(new EnergyStation(d.getName(), d.getxpos(), d.getypos(), d.selectable()));
            }
        }
    }
    
    private void loadAstroids(AstroidsData data) {
        for (AstroidData d : data.getAstroids()) {
            OreAstroid astroid = new OreAstroid(d.getxpos(), d.getypos(), d.getId(), d.getRotation());
            entitys.add(astroid);
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<BaseEntity> getEntitys() {
        return entitys;
    }

    public void setEntitys(ArrayList<BaseEntity> entitys) {
        this.entitys = entitys;
    }

    public ArrayList<BaseItem> getItems() {
        return items;
    }

    public void setItems(ArrayList<BaseItem> items) {
        this.items = items;
    }

    public Shape getBounds() {
        return bounds;
    }

    public void setBounds(Shape bounds) {
        this.bounds = bounds;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public ArrayList<BaseShip> getShips() {
        return ships;
    }

    public void setShips(ArrayList<BaseShip> ships) {
        this.ships = ships;
    }

    public void updateSector(GameContainer gc, int delta, int realX, int realY, Player player) {
        for (BaseShip ship : ships) {
            ship.updateShip(gc, delta);
        }
        for (BaseEntity entity : entitys) {
            entity.updateEntity(gc, delta);
        }
        for (BaseItem item : items) {
            player.getShip().pickUpItem(item);
        }
    }

    public boolean checkIfObjectIsBeingSelected(int realX, int realY) {
        boolean objectSelected = false;
        for (BaseEntity entity : entitys) {
            if (!objectSelected) {
                objectSelected = entity.selectEntitiy(realX, realY);
            }
        }

        return objectSelected;
    }

    public void addNewEntity(BaseEntity entity) {
        if (entity != null) {
            entitys.add(entity);
        }
    }

    public BaseEntity getEntity() {
        return this.entitys.get(0);
    }

    public void drawSector(GameContainer gc, Graphics g) {
        if (image != null) {
            g.drawImage(image, 0, 0);
        }
    }

    public void drawObjects(GameContainer gc, Graphics g) {
        if (!entitys.isEmpty()) {
            for (BaseEntity entity : entitys) {
                entity.drawEntity(gc, g);
            }
        }
    }

    public void drawItem(GameContainer gc, Graphics g) {
        if (!items.isEmpty()) {
            for (BaseItem item : items) {
                item.drawItem(gc, g);
            }
        }
    }

    public void drawShips(GameContainer gc, Graphics g) {
        if (!ships.isEmpty()) {
            for (BaseShip ship : ships) {
                ship.drawShip(gc, g);
            }
        }
    }
}
