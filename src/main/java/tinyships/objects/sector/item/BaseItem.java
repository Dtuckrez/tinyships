/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.sector.item;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import tinyships.global.Fonts;
import tinyships.objects.cargo.BaseCargo;

/**
 *
 * @author Dean
 */
public class BaseItem {

    protected Image image;
    protected int xPos;
    protected int yPos;

    protected BaseCargo cargo;

    protected Shape collisionZone;

    protected boolean isAlive;

    public BaseItem(String image, int xPos, int yPos, BaseCargo cargo,
            boolean isAlive) {
        try {
            this.image = new Image(image);
            this.xPos = xPos;
            this.yPos = yPos;
            this.cargo = cargo;
            this.isAlive = isAlive;
        } catch (SlickException ex) {
            Logger.getLogger(BaseItem.class.getName()).log(Level.SEVERE, null, ex);
        }

        collisionZone = new Rectangle(xPos, yPos, this.image.getWidth(), this.image.getHeight());
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public int getxPos() {
        return xPos;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public int getyPos() {
        return yPos;
    }

    public void setyPos(int yPos) {
        this.yPos = yPos;
    }

    public BaseCargo getCargo() {
        return cargo;
    }

    public void setCargo(BaseCargo cargo) {
        this.cargo = cargo;
    }

    public Shape getCollisionZone() {
        return collisionZone;
    }

    public void setCollisionZone(Shape collisionZone) {
        this.collisionZone = collisionZone;
    }

    public boolean isIsAlive() {
        return isAlive;
    }

    public void setIsAlive(boolean isAlive) {
        this.isAlive = isAlive;
    }

    public void updateItem(GameContainer gc, int delta) {
        if (isAlive) {

        }
    }

    public void destroy() {
        isAlive = false;
    }

    public void drawItem(GameContainer gc, Graphics g) {
        if (isAlive) {
            image.setCenterOfRotation(image.getWidth() / 2, image.getHeight() / 2);
            image.draw(xPos, yPos);
            g.setFont(Fonts.dialogFont);
            g.drawString(cargo.getName(), xPos, yPos - 14);
        }
    }
}
