/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.enums.gui.GameScreenContainers;
import tinyships.enums.gui.GameScreens;
import tinyships.objects.screen.container.BaseContainer;
import tinyships.ui.screens.BaseScreen;

/**
 *
 * @author Dean
 */
public class PlayerScreen extends BaseScreen {

    public PlayerScreen(GameScreens screenId, String name, Color color) {
        super(screenId, name, color);

        containers.add(new BaseContainer(GameScreenContainers.PLAYERINFO, "Info", 10, 55, 175, 250));
    }

    @Override
    public void updateScreen(boolean leftDown, GameContainer gc, int delta) {

    }

    @Override
    public void drawScreen(GameContainer gc, Graphics g) {
        super.drawScreen(gc, g);
    }
}
