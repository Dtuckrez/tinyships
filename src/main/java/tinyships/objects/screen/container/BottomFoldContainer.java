/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.container;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import tinyships.enums.gui.GameScreenContainers;
import tinyships.objects.screen.element.BaseScreenElement;

/**
 *
 * @author Dean
 */
public class BottomFoldContainer extends BaseContainer {

    boolean isShowing;

    public BottomFoldContainer(GameScreenContainers id, String name, int xPos, int yPos,
            int width, int height) {
        super(id, name, xPos, yPos, width, height);
    }

    @Override
    public void updateContainer(boolean leftDown, GameContainer gc, int delta) {
        Input input = gc.getInput();
        if (input.getMouseX() > xPos
                && input.getMouseX() < xPos + 16
                && input.getMouseY() > yPos
                && input.getMouseY() < yPos + 16
                && input.isMousePressed(0) && isShowing) {
            if (isShowing) {
                isShowing = false;
            }
        } else if (input.getMouseX() > xPos
                && input.getMouseX() < yPos + height - 16
                && input.getMouseY() > yPos
                && input.getMouseY() < yPos + height + 16
                && input.isMousePressed(0) && !isShowing) {
            isShowing = true;
        }
    }

    @Override
    public void drawContainer(GameContainer gc, Graphics g) {
        g.setColor(color);

        if (isShowing) {
            g.fillRect(xPos, yPos, width, height);
            g.drawImage(image, xPos, yPos);

            for (BaseScreenElement element : elements) {
                element.drawElement(gc, g);
            }
        }

        if (!isShowing) {
            //Using maths to save on creating the variables
            //& wasting memory.
            g.fillRect(xPos, yPos + height - 16, width, 16);
            g.drawImage(image, xPos, yPos + height - 16);
            g.setColor(Color.white);
            g.drawString(this.name, xPos + height / 2, yPos + height - 16);
        }
    }
}
