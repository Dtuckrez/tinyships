/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.container;

import java.util.ArrayList;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.enums.gui.GameScreenContainers;
import tinyships.global.Fonts;
import tinyships.objects.screen.element.BaseScreenElement;
import tinyships.objects.screen.element.ItemElement;

/**
 *
 * @author Dean
 */
public class StillContainer extends BaseContainer {

    public ArrayList<ItemElement> itemElements = new ArrayList<>();
    int lineWidth;

    public StillContainer(GameScreenContainers containerId, String name,
            int xPos, int yPos, int width, int height, int lineWidth) {
        super(containerId, name, xPos, yPos, width, height);
        this.lineWidth = lineWidth;
    }

    @Override
    public void updateContainer(boolean leftDown, GameContainer gc, int delta) {
        for (BaseScreenElement element : elements) {
            element.updateElement(leftDown, gc, delta);
        }
    }

    @Override
    public void drawContainer(GameContainer gc, Graphics g) {
        g.setColor(color);
        g.setLineWidth(lineWidth);
        g.drawRect(xPos, yPos, width, height);
        g.fillRect(xPos, yPos, width, height);
        g.setFont(Fonts.titleFont);
        g.setColor(Color.white);
        g.drawString(this.name, xPos + lineWidth + 3, yPos + lineWidth + 3);
        g.setLineWidth(1);

        for (BaseScreenElement element : elements) {
            if (!(element instanceof ItemElement)) {
                element.drawElement(gc, g);
            }
        }
    }

    public void drawItemElements(int index, GameContainer gc, Graphics g) {
        int size = 50;
        if (itemElements.size() < 50) {
            size = itemElements.size();
        }

        for (int i = 0; i < size; i++) {
            if (itemElements.get(i).itemIndex == index) {
                itemElements.get(i).drawElement(gc, g);
            }
        }
    }
}
