/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.container;

import java.util.ArrayList;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import tinyships.enums.gui.GameScreenContainers;
import tinyships.enums.gui.GameScreenElements;
import tinyships.global.Colours;
import tinyships.objects.screen.element.BaseScreenElement;

/**
 *
 * @author Dean
 */
public class BaseContainer {

    protected Image image;

    public GameScreenContainers containerId;
    public String name;

    public int xPos, yPos, height, width;

    public Color color = Colours.BOXBLUE;

    public ArrayList<BaseScreenElement> elements = new ArrayList<>();
    public ArrayList<BaseContainer> containers = new ArrayList<>();
    private boolean isHover = false;

    private boolean isHidden;

    public BaseContainer(GameScreenContainers containerId, String name, int xPos, int yPos, int width, int height) {

        this.containerId = containerId;
        this.name = name;
        this.xPos = xPos;
        this.yPos = yPos;
        this.height = height;
        this.width = width;
        isHidden = false;
    }

    public BaseContainer(GameScreenContainers containerId, String name,
            int xPos, int yPos, int width, int height, Color color) {

        this.containerId = containerId;
        this.name = name;
        this.xPos = xPos;
        this.yPos = yPos;
        this.height = height;
        this.width = width;
        this.color = color;
        isHidden = false;
    }

    public void updateContainer(boolean leftDown, GameContainer gc, int delta) {
        Input input = gc.getInput();
        isHover = input.getMouseX() + 1 > xPos && input.getMouseX() < xPos + width
                && input.getMouseY() + 1 > yPos && input.getMouseY() < yPos + height;
        if (isHover) {
//            System.out.println(this);
        }

        for (BaseScreenElement element : elements) {
            element.updateElement(leftDown, gc, delta);
        }
    }

    public boolean isHover() {
        return isHover;
    }

    public void setHidden(boolean hidden) {
        this.isHidden = hidden;
    }
    
    public boolean getHidden() {
        return isHidden;
    }
    
    public void drawContainer(GameContainer gc, Graphics g) {
        if (!isHidden) {
            g.setColor(Color.white);
            g.drawString(name, xPos, yPos);
            g.setColor(color);

            g.fillRoundRect(xPos, yPos, width, height, 8);
            for (BaseScreenElement element : elements) {
                if (element != null) {
                    element.drawElement(gc, g);
                }
            }
            for (BaseContainer container : containers) {
                if (container != null) {
                    container.drawContainer(gc, g);
                }
            }
        }
    }

    public void addElement(BaseScreenElement element) {
        elements.add(element);
    }

    public void setColor(float r, float g, float b, float a) {
        this.color = new Color(r, g, b, a);
    }

    public BaseScreenElement getElementViaId(GameScreenElements elementId) {
        for (BaseScreenElement element : elements) {
            if (element.elementId == elementId) {
                return element;
            }
        }

        return new BaseScreenElement(GameScreenElements.BASEELEMENT);
    }

    public void removeElements() {
        elements.clear();
    }

    public void removeContainer() {
        for (BaseContainer container : containers) {
            container.elements.clear();
        }

        containers.clear();
    }
}
