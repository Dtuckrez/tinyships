/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.container;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import tinyships.enums.gui.GameScreenContainers;
import tinyships.global.Colours;
import tinyships.global.Fonts;
import tinyships.objects.screen.element.BaseScreenElement;

/**
 *
 * @author Dean
 */
public class HoverHideContainer extends BaseContainer {

    boolean isShowing;

    public HoverHideContainer(GameScreenContainers containerId, String name, int xPos, int yPos, int width, int height) {
        super(containerId, name, xPos, yPos, width, height);

        color = Colours.BOXBLUE;
    }

    @Override
    public void updateContainer(boolean leftDown, GameContainer gc, int delta) {
        Input input = gc.getInput();

        if (!getHidden()) {
            if (input.getMouseX() + 1 > xPos && input.getMouseX() < xPos + width
                    && input.getMouseY() + 1 > yPos && input.getMouseY() < yPos + height) {
            } else {
                setHidden(true);
            }
        }

        for (BaseScreenElement element : elements) {
            element.updateElement(leftDown, gc, delta);
        }
    }

    @Override
    public void drawContainer(GameContainer gc, Graphics g) {
        if (!getHidden()) {
        g.setColor(color);
        g.fillRoundRect(xPos, yPos, width, height, 8);
            for (BaseScreenElement element : elements) {
                element.drawElement(gc, g);
            }
    }
    }
}
