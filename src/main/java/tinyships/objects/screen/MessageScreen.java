/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen;

import java.util.ArrayList;
import java.util.List;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.callbacks.ScrollBarCallBack;
import tinyships.enums.gui.GameScreenContainers;
import tinyships.enums.gui.GameScreenElements;
import tinyships.enums.gui.GameScreens;
import tinyships.game.message.BaseMessage;
import tinyships.global.Colours;
import tinyships.global.Fonts;
import tinyships.manager.GameManager;
import tinyships.manager.ScreenManager;
import tinyships.objects.screen.container.BaseContainer;
import tinyships.objects.screen.element.BaseScreenElement;
import tinyships.objects.screen.element.HorizontalSliderElement;
import tinyships.objects.screen.element.MultiLineTextBox;
import tinyships.objects.screen.element.SimpleTextElement;
import tinyships.objects.screen.element.SliderElement;
import tinyships.ui.screens.BaseScreen;

/**
 *
 * @author Dean
 */
public class MessageScreen extends BaseScreen implements ScrollBarCallBack{

    private boolean updateMessage;

    private final int messagePerPage = 9;
    private int currentPage = 1;
    private int lastPage;

    public MessageScreen(GameScreens screenId, String name, Color color) {
        super(screenId, name, color);
        updateMessage = true;
        containers.add(new BaseContainer(GameScreenContainers.BASECONTAINER, "", 128, 96, 768, 590));
        containers.add(new BaseContainer(GameScreenContainers.DIALOG, "", 128, 490, 768, 200));
        elements.add(new MultiLineTextBox(GameScreenElements.INFO, "", Fonts.dialogFont, 128, 490, 768, 200, Colours.WHITE, Colours.TRANSPARENT));
        elements.add(new HorizontalSliderElement(GameScreenElements.SLIDER, 896, 128, 32, 430, 1, Colours.BOXBLUE, Colours.TEXTORANGE, Colours.WHITE, Fonts.dialogFont, this));
    }

    public boolean isUpdateMessage() {
        return updateMessage;
    }

    public void setUpdateMessage(boolean updateMessage) {
        this.updateMessage = updateMessage;
    }

    @Override
    public void updateScreen(boolean leftDown, GameContainer gc, int delta) {
        lastPage = ((SliderElement) getElementById(GameScreenElements.SLIDER)).getCurrentNumber();

        int COLUMNS = 3;
        int ROWS = 0;
        
        float pageAmount = (GameManager.getPlayer().getMessages().size() / messagePerPage) + 1;
        if (updateMessage) {
            updateMessage = false;
            ((SliderElement) getElementById(GameScreenElements.SLIDER)).setMaxNumber(Math.round(pageAmount));

            int lastNumber = (currentPage) * messagePerPage;
            if (lastNumber > GameManager.getPlayer().getMessages().size()) {
                lastNumber = GameManager.getPlayer().getMessages().size();
            }
            
            int startNumber = lastNumber - 9;
            if (startNumber < 0) {
                startNumber = 0;
            }                                                                              
            
            List<BaseMessage> messages = new ArrayList<>() ;
            
            messages = GameManager.getPlayer().getMessages().subList(startNumber, lastNumber);
            for (int i = 0; i < messages.size(); i++) {
                int col = i % COLUMNS;
                int row = ROWS + i / COLUMNS;

                BaseContainer container = getContainerById(GameScreenContainers.BASECONTAINER);

                BaseContainer containerToAdd = new BaseContainer(GameScreenContainers.MESSAGE, "", 135 + (col * 235), 102 + (100 * row), 224, 64, overlayColor);
                containerToAdd.elements.add(new SimpleTextElement(GameScreenElements.BASEELEMENT, messages.get(i).getTitle(), 135 + (col * 235), 102 + (100 * row), Fonts.infomationFont));
                containerToAdd.elements.add(new SimpleTextElement(GameScreenElements.BASEELEMENT, messages.get(i).getSender(), 135 + (col * 235), 122 + (100 * row), Fonts.infomationFont));
                container.containers.add(containerToAdd);
            }
        }

        selectMessage(leftDown, gc, delta);

        for (BaseScreenElement element : elements) {
            element.updateElement(leftDown, gc, delta);
        }
        
        if (currentPage != ((HorizontalSliderElement) getElementById(GameScreenElements.SLIDER)).getCurrentNumber()) {
            currentPage = ((HorizontalSliderElement) getElementById(GameScreenElements.SLIDER)).getCurrentNumber();
            updateMessage = true;
        }

    }
    
    private void selectMessage(boolean leftDown, GameContainer gc, int delta) {
        BaseContainer container = getContainerById(GameScreenContainers.BASECONTAINER);
        for (int i = 0; i < container.containers.size(); i++) {
            container.containers.get(i).updateContainer(leftDown, gc, delta);
            if (container.containers.get(i).isHover()) {
                container.containers.get(i).color = Colours.BOXBLUE;
                if (leftDown) {
                    String content = GameManager.getPlayer().getMessages().get(currentPage * i).getContent();
                    
                    if (!GameManager.getPlayer().getMessages().get(currentPage * i).isIsRead()) {
                        GameManager.getPlayer().getMessages().get(currentPage * i).setIsRead(true);
                        if (GameManager.getPlayer().getMessages().get(currentPage * i).getMission() != null) {
                            GameManager.getPlayer().getMissions().add(GameManager.getPlayer().getMessages().get(currentPage * i).getMission());
//                            ((MissionScreen)ScreenManager.getScreenById(GameScreens.MISSIONSCREEN)).setHaveMissionsUpdated(false);
                        }
                    }
                    
                    ((MultiLineTextBox) getElementById(GameScreenElements.INFO)).updatePages(content, false);
                }
            }
        }
    }

    @Override
    public void drawScreen(GameContainer gc, Graphics g) {
        g.setColor(this.overlayColor);
        g.fillRect(0, 0, 1024, 768);
        g.setColor(Color.black);

        for (BaseContainer container : containers) {
            container.drawContainer(gc, g);
        }

        for (BaseScreenElement element : elements) {
            element.drawElement(gc, g);
        }
    }

    @Override
    public void onScroll() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
