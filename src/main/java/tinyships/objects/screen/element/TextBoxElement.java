/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.element;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.enums.gui.GameScreenElements;

/**
 *
 * @author Dean
 */
public class TextBoxElement extends BaseScreenElement {

    String name;
    String text;
    int xPos;
    int yPos;
    int width;
    int height;
    Color textColor;
    Color boxColor;
    Font font;

    private int lines;

    boolean isFull = true;

    public TextBoxElement(GameScreenElements id, String name, String text, int xPos,
            int yPos, int width, int height, Color textColor,
            Color boxColor, Font font) {
        super(id);
        this.name = name;
        this.text = text;
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = width;
        this.height = height;
        this.textColor = textColor;
        this.boxColor = boxColor;
        this.font = font;
    }

    public TextBoxElement(GameScreenElements id, String name, String text, int xPos,
            int yPos, int width, int height, Color textColor,
            Color boxColor, Font font, boolean isFull) {
        super(id);
        this.name = name;
        this.text = text;
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = width;
        this.height = height;
        this.textColor = textColor;
        this.boxColor = boxColor;
        this.font = font;
        this.isFull = isFull;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getxPos() {
        return xPos;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public int getyPos() {
        return yPos;
    }

    public void setyPos(int yPos) {
        this.yPos = yPos;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Color getTextColor() {
        return textColor;
    }

    public void setTextColor(Color textColor) {
        this.textColor = textColor;
    }

    public Color getBoxColor() {
        return boxColor;
    }

    public void setBoxColor(Color boxColor) {
        this.boxColor = boxColor;
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public void updateText(String text) {
        this.text = text;
    }

    @Override
    public void drawElement(GameContainer gc, Graphics g) {
        g.setLineWidth(2);
        g.setColor(boxColor);

        if (this.isFull) {
            g.fillRoundRect(xPos, yPos, width, height, 4);
        } else {
            g.drawRoundRect(xPos, yPos, width, height, 4);
        }

        g.setColor(textColor);
        g.setFont(font);
        g.drawString(name, xPos, yPos);
        g.drawString(text, xPos, yPos + font.getLineHeight());
    }
}
