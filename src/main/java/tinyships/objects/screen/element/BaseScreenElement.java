/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.element;

import java.util.ArrayList;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.enums.gui.GameScreenElements;
import tinyships.objects.screen.element.action.BaseElementAction;

/**
 *
 * @author Dean
 */
public class BaseScreenElement implements IBaseScreenElement {

    public ArrayList<BaseElementAction> actions = new ArrayList<>();
    public GameScreenElements elementId;
    public int tag;

    public boolean toDrawElement;

    public BaseScreenElement(GameScreenElements elementId) {
        this.elementId = elementId;
    }

    public GameScreenElements getElement() {
        return elementId;
    }

    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }
    
    @Override
    public void updateElement(boolean leftDown, GameContainer gc, int delta) {

    }

    @Override
    public void drawElement(GameContainer gc, Graphics g) {

    }
}
