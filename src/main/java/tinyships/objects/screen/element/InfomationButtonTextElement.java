/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.element;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.enums.gui.GameScreenElements;
import tinyships.global.Fonts;

/**
 *
 * @author Dean
 */
public class InfomationButtonTextElement extends ButtonImageElement {

    private String infomation;

    public InfomationButtonTextElement(GameScreenElements elementId, String buttonFile, String iconFile, String infomation) {
        super(elementId, buttonFile, iconFile, null);
        this.infomation = infomation;
    }

    public String getInfomation() {
        return this.infomation;
    }

    public void setInfomation(String infomation) {
        this.infomation = infomation;
    }

    @Override
    public void drawElement(GameContainer gc, Graphics g) {
        super.drawElement(gc, g);
        g.setColor(Color.yellow);
        g.fillRoundRect(xPos, yPos + height - (height / 4), width, height / 4, 4);
        g.fillRoundRect(xPos, yPos + height - (height / 4), width, height / 4, 4);
        g.fillRoundRect(xPos, yPos + height - (height / 4), width, height / 4, 4);
        g.setFont(Fonts.infomationFont);
        g.setColor(Color.white);
        if (infomation == null) {
            infomation = "";
        }
        g.drawString(infomation, xPos + width / 4, yPos + height - (height / 3));
    }
}
