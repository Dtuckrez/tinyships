/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.element;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import tinyships.enums.gui.GameScreenElements;

/**
 *
 * @author Dean
 */
public class ImageBoxElement extends BaseScreenElement {

    private int xPos;
    private int yPos;
    private int width;
    private int height;

    private Color color;

    private Image image;

    public ImageBoxElement(GameScreenElements elementId, int xPos, int yPos, int width, int height, Color color, String image) {
        super(elementId);
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = width;
        this.height = height;
        this.color = color;
        try {
            this.image = new Image(image);
        } catch (SlickException ex) {
            Logger.getLogger(ImageBoxElement.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int getxPos() {
        return xPos;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public int getyPos() {
        return yPos;
    }

    public void setyPos(int yPos) {
        this.yPos = yPos;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    @Override
    public void updateElement(boolean leftDown, GameContainer gc, int delta) {

    }

    @Override
    public void drawElement(GameContainer gc, Graphics g) {
        g.setColor(color);
        g.fillRoundRect(xPos, yPos, width, height, 4);
        if (image != null) {
            image.draw(xPos, yPos, width, height);
        }
    }
}
