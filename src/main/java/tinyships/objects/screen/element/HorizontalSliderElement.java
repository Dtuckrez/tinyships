/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.element;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import tinyships.callbacks.ScrollBarCallBack;
import tinyships.enums.gui.GameScreenElements;

/**
 *
 * @author Dean
 */
public class HorizontalSliderElement extends SliderElement {

    public HorizontalSliderElement(GameScreenElements elementId, int xPos, int yPos, int width, int height, int maxNumber, Color innerColour, Color outterColor, Color textColor, Font font, ScrollBarCallBack scrollBarCallBack) {
        super(elementId, xPos, yPos, width, height, maxNumber, innerColour, outterColor, textColor, font, scrollBarCallBack);
    }

    @Override
    public void updateElement(boolean leftClick, GameContainer gc, int delta) {
        Input input = gc.getInput();
        if (input.getMouseX() + 1 > xPos && input.getMouseX() < xPos + width
                && input.getMouseY() + 1 > yPos && input.getMouseY() < yPos + height
                && leftClick) {
            currentNumber = (input.getMouseY() - yPos) * maxNumber / height + 1;
            if (scrollBarCallBack != null) {
                scrollBarCallBack.onScroll();
            }
        }
    }

    public boolean hasBeenClicked(boolean leftClick, GameContainer gc, int delta) {
        Input input = gc.getInput();
        return input.getMouseX() + 1 > xPos && input.getMouseX() < xPos + width
                && input.getMouseY() + 1 > yPos && input.getMouseY() < yPos + height
                && leftClick;
    }

    @Override
    public void drawElement(GameContainer gc, Graphics g) {
        g.setColor(innerColour);
        g.fillRect(xPos - 2, yPos - 2, width + 4, height + 3);
        g.setColor(outterColor);
        if (maxNumber > 0) {
            int size = (currentNumber * height / maxNumber + 1);
            g.fillRoundRect(xPos, yPos + 2, width - 2, size + 2, 3);
        }
    }
}
