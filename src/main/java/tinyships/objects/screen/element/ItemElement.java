/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.element;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import tinyships.enums.gui.GameScreenElements;
import tinyships.enums.ship.equipment.CargoType;
import tinyships.enums.ship.equipment.WeaponType;

/**
 *
 * @author Dean
 */
public class ItemElement extends BaseScreenElement {

    public String name;
    public CargoType itemType;
    public WeaponType weaponType;

    public Image image;

    public int xPos;
    public int yPos;
    public int offset;

    public int itemIndex; // used for drawing

    public ItemElement(GameScreenElements id, String name, CargoType itemType,
            WeaponType weaponType, int xPos, int yPos, int offset,
            int itemIndex) {
        super(id);
        this.xPos = xPos;
        this.yPos = yPos;
        this.offset = offset;
        this.itemIndex = itemIndex;
    }

    public ItemElement(GameScreenElements id, String name, CargoType itemType,
            WeaponType weaponType, int xPos, int yPos, int offset,
            int itemIndex, Image image) {
        super(id);
        this.xPos = xPos;
        this.yPos = yPos;
        this.offset = offset;
        this.itemIndex = itemIndex;
        this.image = image;
    }

    @Override
    public void drawElement(GameContainer gc, Graphics g) {
        g.setColor(Color.pink);
        if (image != null) {
            g.drawImage(image, xPos + offset, yPos + offset);
        }
    }
}
