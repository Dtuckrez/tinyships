/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.element;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import tinyships.enums.gui.GameScreenElements;
import tinyships.global.Fonts;

/**
 *
 * @author Dean
 */
public class ButtonTextBoxElement extends BaseScreenElement {

    String name;
    int xPos;
    int yPos;
    int width;
    int height;

    Color textColor;
    Color boxColor;
    Color hoverColor;
    boolean isHover;

    public ButtonTextBoxElement(GameScreenElements elementId, String name, int xPos, int yPos,
            int width, int height, Color textColor, Color boxColor, Color hoverColor) {
        super(elementId);

        this.name = name;
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = width;
        this.height = height;
        this.textColor = textColor;
        this.boxColor = boxColor;
        this.hoverColor = hoverColor;
    }

    @Override
    public void updateElement(boolean leftDown, GameContainer gc, int delta) {
        Input input = gc.getInput();
        isHover = input.getMouseX() + 1 > xPos && input.getMouseX() < xPos + width
                && input.getMouseY() + 1 > yPos && input.getMouseY() < yPos + height;
    }

    public boolean isButtonPressed(GameContainer gc, int delta) {
        Input input = gc.getInput();
        return input.isMousePressed(0);
    }

    @Override
    public void drawElement(GameContainer gc, Graphics g) {

        if (!isHover) {
            g.setColor(boxColor);
        } else {
            g.setColor(hoverColor);
        }
        g.fillRect(xPos, yPos, width, height);

        g.setFont(Fonts.infomationFont);
        g.setColor(textColor);
        g.drawString(name, xPos, yPos);
    }
}
