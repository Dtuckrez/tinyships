/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.element;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import tinyships.enums.gui.GameScreenElements;
import tinyships.global.Images;
import tinyships.objects.screen.element.action.BaseElementAction;
import tinyships.callbacks.ButtonCallback;

/**
 *
 * @author Dean
 */
public class ButtonImageElement extends BaseScreenElement {

    String iconLocation;
    String buttonLocation;

    Image button;
    Image buttonHover;
    Image icon;
    Image iconHover;

    int xPos;
    int yPos;
    int width;
    int height;
    
    protected int index = 0;
    
    boolean isHover;
    boolean alwaysHover = true;

    
    protected BaseElementAction baseAction;

    ButtonCallback buttonCallback;

    public ButtonImageElement(GameScreenElements elementId, String buttonFile,
            String iconFile, BaseElementAction baseElementAction) {
        super(elementId);

        this.iconLocation = iconFile;
        this.buttonLocation = buttonFile;
        this.button = Images.getImage(buttonFile); //new Image(buttonFile);
        this.buttonHover = Images.getImage(buttonFile.replace(".png", "hover.png"));
        this.icon = Images.getImage(iconFile);
        this.iconHover = Images.getImage(iconFile.replace(".png", "hover.png"));
        this.baseAction = baseElementAction;

        this.width = button.getWidth();
        this.height = button.getHeight();
    }

    public ButtonImageElement(GameScreenElements elementId, String buttonFile,
            String iconFile, int xPos, int yPos, BaseElementAction baseElementAction) {
        super(elementId);

        try {
            this.button = new Image(buttonFile);
            this.buttonHover = new Image(buttonFile.replace(".png", "hover.png"));
            this.icon = new Image(iconFile);
            this.iconHover = new Image(iconFile.replace(".png", "hover.png"));
            this.xPos = xPos;
            this.yPos = yPos;
            this.baseAction = baseElementAction;
        } catch (SlickException ex) {
            Logger.getLogger(ButtonImageElement.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.width = button.getWidth();
        this.height = button.getHeight();
    }
    
    
    @Override
    public void updateElement(boolean leftDown, GameContainer gc, int delta) {
        Input input = gc.getInput();
        isHover = input.getMouseX() + 1 > xPos && input.getMouseX() < xPos + width
                && input.getMouseY() + 1 > yPos && input.getMouseY() < yPos + height;
        isButtonPressed(leftDown);

        if (isHover) {
            if (buttonCallback != null) {
                buttonCallback.processHover(this);
            }
        }
    }

    public boolean isButtonPressed(boolean isMousePressed) {
        if (isHover && isMousePressed) {
            if (baseAction != null) {
                baseAction.hasActionExectuted();
            }
            return true;
        }
        return false;
    }

    @Override
    public void drawElement(GameContainer gc, Graphics g) {
        if (!isHover || !alwaysHover) {
            button.draw(xPos, yPos);
            icon.draw(xPos, yPos);
        } else if (isHover || alwaysHover) {
            buttonHover.draw(xPos, yPos);
            iconHover.draw(xPos, yPos);
        }
//        icon.draw(xPos, yPos);
    }

    public void setCallback(ButtonCallback buttonCallback) {
        this.buttonCallback = buttonCallback;
    }

    public Image getButton() {
        return button;
    }

    public void setButton(Image button) {
        this.button = button;
    }

    public Image getIcon() {
        return icon;
    }

    public void setIcon(Image icon) {
        this.icon = icon;
    }

    public int getxPos() {
        return xPos;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public int getyPos() {
        return yPos;
    }

    public void setyPos(int yPos) {
        this.yPos = yPos;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isHover() {
        return isHover;
    }

    public void setIsHover(boolean isHover) {
        this.isHover = isHover;
    }

    public boolean getAlwaysHover() {
        return this.alwaysHover;
    }

    public void setAlwaysHover(boolean alwaysHover) {
        this.alwaysHover = alwaysHover;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
