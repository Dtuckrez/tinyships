/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.element;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.enums.gui.GameScreenElements;
import tinyships.global.Fonts;

/**
 *
 * @author Dean
 */
public class PopoutElement extends BaseScreenElement {

    public String name;

    public int startX, endX, xPos;
    public int startY, endY, yPos;
    public int width, height;

    boolean isActive;

    public PopoutElement(GameScreenElements id, String name, int startX, int endX,
            int startY, int endY, int width, int height) {
        super(id);
        this.name = name;
        this.startX = startX;
        this.startY = 10;
        this.xPos = startX;
        this.yPos = 10;
        this.endX = endX;
        this.endY = 10;

        this.width = width;
        this.height = height;
    }

    @Override
    public void updateElement(boolean leftDown, GameContainer gc, int delta) {
        //Move
        if (isActive) {
            if (xPos + width < endX) {
                xPos += 0.5f * delta;
            }
            if (yPos < endY) {
                xPos += 0.5f * delta;
            }
        }
        if (!isActive && xPos > endX && yPos > endY) {
            xPos += 1 * delta;
            yPos += 1 * delta;
        }
    }

    @Override
    public void drawElement(GameContainer gc, Graphics g) {
        g.fillRoundRect(xPos, yPos, width, height, 6);
        g.setColor(Color.yellow);
        g.setFont(Fonts.infomationFont);
        g.drawString(name, xPos + 6, yPos + 4);
    }

    public boolean isActive() {
        return this.isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }
}
