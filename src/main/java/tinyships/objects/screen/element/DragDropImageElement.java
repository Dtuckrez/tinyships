/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tinyships.objects.screen.element;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import tinyships.enums.gui.GameScreenElements;
import tinyships.manager.GameManager;
import tinyships.objects.screen.element.action.BaseElementAction;

/**
 *
 * @author Dean
 */
public class DragDropImageElement extends ButtonImageElement {

    private int currentX;
    private int currentY;
    private boolean isSelected;

    public DragDropImageElement(GameScreenElements elementId, String buttonFile,
            String iconFile, int xPos, int yPos, BaseElementAction baseElementAction) {
        super(elementId, buttonFile, iconFile, xPos, yPos, baseElementAction);

        currentX = xPos;
        currentY = yPos;
    }

    public DragDropImageElement(GameScreenElements elementId, String buttonFile,
            String iconFile, int xPos, int yPos, BaseElementAction baseElementAction, int index) {
        super(elementId, buttonFile, iconFile, xPos, yPos, baseElementAction);

        currentX = xPos;
        currentY = yPos;
        this.index = index;
    }
    
    public int getCurrentX() {
        return currentX;
    }

    public void setCurrentX(int currentX) {
        this.currentX = currentX;
    }

    public int getCurrentY() {
        return currentY;
    }

    public void setCurrentY(int currentY) {
        this.currentY = currentY;
    }

    public boolean isIsSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    @Override
    public void updateElement(boolean leftDown, GameContainer gc, int delta) {

        Input input = gc.getInput();
         if (input.getMouseX() + 1 > xPos && input.getMouseX() < xPos + width
                && input.getMouseY() + 1 > yPos && input.getMouseY() < yPos + height) {
             isHover = true;
             if (buttonCallback != null) {
                 buttonCallback.processHover(this);
             }
         } else {
             isHover = false;
         }
        
        if (isHover && leftDown && !GameManager.isHolding) {
            isSelected = true;
            GameManager.isHolding = true;
        }

        if (isSelected) {
            if (input.isMouseButtonDown(0)) {
                currentX = input.getMouseX() - 32;
                currentY = input.getMouseY() - 32;
            } else {
                if (buttonCallback != null) {
                    buttonCallback.processClick();
                }
                currentX = xPos;
                currentY = yPos;
                isSelected = false;
                GameManager.isHolding = false;
            }
        }
    }

    @Override
    public void drawElement(GameContainer gc, Graphics g) {
        g.setColor(Color.yellow);
        if (button != null) {
            g.drawImage(button, currentX, currentY);
        }
        if (icon != null) {
            g.drawImage(icon, currentX, currentY);
        }
    }
}
