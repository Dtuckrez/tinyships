/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.element;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import tinyships.callbacks.ScrollBarCallBack;
import tinyships.enums.gui.GameScreenElements;

/**
 *
 * @author Dean
 */
public class SliderElement extends BaseScreenElement {

    protected int xPos;
    protected int yPos;
    protected int width;
    protected int height;

    protected int maxNumber;
    protected int currentNumber;

    protected Color innerColour;
    protected Color outterColor;
    protected Color textColor;

    protected Font font;
    
    protected ScrollBarCallBack scrollBarCallBack;

    public SliderElement(GameScreenElements elementId, int xPos, int yPos, int width, int height, int maxNumber, Color innerColour,
            Color outterColor, Color textColor, Font font, ScrollBarCallBack scrollBarCallBack) {
        super(elementId);
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = width;
        this.height = height;
        this.maxNumber = maxNumber;
        this.innerColour = innerColour;
        this.outterColor = outterColor;
        this.textColor = textColor;
        this.font = font;

        currentNumber = 1;
        
        this.scrollBarCallBack = scrollBarCallBack;
    }

    public int getxPos() {
        return xPos;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public int getyPos() {
        return yPos;
    }

    public void setyPos(int yPos) {
        this.yPos = yPos;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getMaxNumber() {
        return maxNumber;
    }

    public void setMaxNumber(int maxNumber) {
        this.maxNumber = maxNumber;
    }

    public int getCurrentNumber() {
        return currentNumber;
    }

    public void setCurrentNumber(int currentNumber) {
        this.currentNumber = currentNumber;
    }

    public Color getInnerColour() {
        return innerColour;
    }

    public void setInnerColour(Color innerColour) {
        this.innerColour = innerColour;
    }

    public Color getOutterColor() {
        return outterColor;
    }

    public void setOutterColor(Color outterColor) {
        this.outterColor = outterColor;
    }

    public Color getTextColor() {
        return textColor;
    }

    public void setTextColor(Color textColor) {
        this.textColor = textColor;
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    @Override
    public void updateElement(boolean leftClick, GameContainer gc, int delta) {
        Input input = gc.getInput();
        if (input.getMouseX() + 1 > xPos && input.getMouseX() < xPos + width
                && input.getMouseY() + 1 > yPos && input.getMouseY() < yPos + height
                && leftClick) {
            currentNumber = (input.getMouseX() - xPos) * maxNumber / width + 1;
        }
    }

    @Override
    public void drawElement(GameContainer gc, Graphics g) {
        g.setColor(innerColour);
        g.fillRect(xPos - 2, yPos - 2, width + 4, height + 3);
        g.setColor(outterColor);
        if (maxNumber > 0 ) {
            int size = (currentNumber * width / maxNumber + 1);
            g.fillRoundRect(xPos, yPos + 1, size, height - 2, 3);
        }
    }
}
