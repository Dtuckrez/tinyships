/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.element;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.enums.gui.GameScreenElements;

/**
 *
 * @author Dean
 */
public class HorizontalPercentBar extends BaseScreenElement {

    public int xPos, yPos;
    public int width, height;
    public int value, maxValue;
    public Color bottom, top;

    public HorizontalPercentBar(GameScreenElements id, int xPos, int yPos, int width, int height,
            Color bottom, Color top) {
        super(id);
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = width;
        this.height = height;
        this.bottom = bottom;
        this.top = top;

        this.value = 1;
        this.maxValue = 1;
    }

    @Override
    public void updateElement(boolean leftDown, GameContainer gc, int delta) {

    }

    public void updateElement(GameContainer gc, int delta, int Value) {

    }

    @Override
    public void drawElement(GameContainer gc, Graphics g) {
        //Draw under(Empty Space
        g.setColor(bottom);
        g.fillRect(xPos - 2, yPos - 2, width + 4, height + 3);
        //Draw Health
        g.setColor(top);
        int size = (value * width / maxValue + 1);
        g.fillRoundRect(xPos, yPos + 1, size, height - 2, 3);
    }
}
