/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.element;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import tinyships.enums.gui.GameScreenElements;

/**
 *
 * @author Dean
 */
public class IconElement extends BaseScreenElement {

    private Image image;
    private int xPos;
    private int yPos;
    private int value;
    private int maxValue;

    public IconElement(GameScreenElements id, int xPos, int yPos) {
        super(id);
        this.xPos = xPos;
        this.yPos = yPos;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public int getxPos() {
        return xPos;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public int getyPos() {
        return yPos;
    }

    public void setyPos(int yPos) {
        this.yPos = yPos;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }

    @Override
    public void updateElement(boolean leftDown, GameContainer gc, int delta) {

    }

    @Override
    public void drawElement(GameContainer gc, Graphics g) {
        g.setColor(new Color(0.3f, 0.3f, 0.3f, 0.7f));
        g.setLineWidth(1);

        if (image != null) {
            image.draw(xPos + 2, yPos + 2);
        }

        if (value > 1) {
            int size = (value * 32 / maxValue + 1);
            g.fillRoundRect(xPos + 2, yPos + 2, 32, size, 4);
        }
    }
}
