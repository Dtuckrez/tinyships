/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tinyships.objects.screen.element;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import tinyships.enums.gui.GameScreenElements;
import tinyships.objects.screen.element.action.BaseElementAction;

/**
 *
 * @author Dean
 */
public class HoverImageElementButton extends ButtonImageElement{

    public HoverImageElementButton(GameScreenElements elementId, String buttonFile, String iconFile, BaseElementAction baseElementAction) {
        super(elementId, buttonFile, iconFile, baseElementAction);
    }
    
    
    public HoverImageElementButton(GameScreenElements elementId, String buttonFile,
            String iconFile, int xPos, int yPos, BaseElementAction baseElementAction) {
        super(elementId, buttonFile, iconFile, xPos, yPos, baseElementAction);
        
    }
    
    @Override
    public void updateElement(boolean leftDown, GameContainer gc, int delta) {
        Input input = gc.getInput();
        isHover = input.getMouseX() + 1 > xPos && input.getMouseX() < xPos + width
                && input.getMouseY() + 1 > yPos && input.getMouseY() < yPos + height;

        if (isHover && baseAction != null) {
            this.baseAction.hasActionExectuted();
        }

    }
    
}
