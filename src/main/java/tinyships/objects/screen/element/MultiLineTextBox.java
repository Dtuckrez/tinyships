/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.element;

import java.util.ArrayList;
import java.util.List;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import tinyships.enums.gui.GameScreenElements;
import tinyships.manager.GameManager;

/**
 *
 * @author Dean
 */
public class MultiLineTextBox extends BaseScreenElement {

    private ArrayList<Page> pages = new ArrayList<>();
    private Font font;
    private int xPos;
    private int yPos;
    private int width;
    private int height;

    private int currentPage = 0;

    private Color textColor;
    private Color boxColor;

    private boolean hasButtons;

    private ButtonImageElement acceptButton;
    private ButtonImageElement declineButton;

    public MultiLineTextBox(GameScreenElements elementId, String text, Font font, int xPos, int yPos, int width, int height,
            Color textColour, Color boxColor) {
        super(elementId);
        this.font = font;
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = width;
        this.height = height;
        this.textColor = textColour;
        this.boxColor = boxColor;
        calcualtePages(wrapLines(text));

        acceptButton = new ButtonImageElement(GameScreenElements.BASEELEMENT, "images/button/button.png", "images/button/info.png", xPos, yPos + height, null);
        declineButton = new ButtonImageElement(GameScreenElements.EXIT, "images/button/button.png", "images/button/exit.png", xPos + width - 64, yPos + height, null);
    }

    public ArrayList wrapLines(String all) {
        if (all == null) {
            all = "";
        }
        ArrayList<String> lines = new ArrayList<>();
        String[] words = all.split("\\s");
        String line = "";
        int length = 0;
        for (String w : words) {

            boolean hasLineBreak = w.contains("/n") || w.contains("/N");

            if (length + font.getWidth(w) + 1 > width - font.getWidth(w) || hasLineBreak) {
                if (font.getWidth(w) >= width || hasLineBreak) {
                    w = w.replace("/n", "");
                    w = w.replace("/N", "");
                    line += w;
                    lines.add(line);
                    line = "";
                    length = 0;
                    continue;
                }
                lines.add(line);
                line = "";
                length = 0;
            }
            if (length > 0) {
                line += " ";
                length += 1;
            }
            line += w;
            length += font.getWidth(w);
        }
        if (line.length() > 0) {
            lines.add(line);
        }
        return lines;
    }

    public void calcualtePages(ArrayList<String> lines) {
        ArrayList<String> lineToAdd = new ArrayList<>();
        int heightOfLines = 0;
        for (String line : lines) {
            if (heightOfLines + font.getHeight(line) + 1 > height) {
                if (font.getHeight(line) + heightOfLines >= height) {
                    pages.add(new Page(lineToAdd));
                    heightOfLines = 0;
                    lineToAdd.clear();
                    continue;
                }
                lineToAdd.add(line);
                heightOfLines = 0;
            }
            if (line.contains("/N")) {
                String[] split = line.split("/N");
                for (int i = 0; i < split.length; i++) {
                    lineToAdd.add(split[i]);
                    heightOfLines += font.getHeight(split[i]);
                }
            } else {
                lineToAdd.add(line);
                heightOfLines += font.getHeight(line);
            }
        }
        if (!lineToAdd.isEmpty()) {
            pages.add(new Page(lineToAdd));
        }
    }

    public void updatePages(String dialog, boolean hasButtons) {
        pages.clear();
        calcualtePages(wrapLines(dialog));
        this.hasButtons = hasButtons;
    }

    public void addNewPage(String dialog) {
        calcualtePages(wrapLines(dialog));
    }

    @Override
    public void updateElement(boolean leftDown, GameContainer gc, int delta) {
        Input input = gc.getInput();
        acceptButton.updateElement(leftDown, gc, delta);
        declineButton.updateElement(leftDown, gc, delta);

        if (((ButtonImageElement) acceptButton).isHover && leftDown) {
            GameManager.dialogManager.setShowingDialog(false);
        }
        if (((ButtonImageElement) declineButton).isHover && leftDown) {
            GameManager.dialogManager.setShowingDialog(false);
        }

        if (input.isKeyPressed(Input.KEY_SPACE)) {
            if (currentPage + 1 < pages.size()) {
                currentPage += 1;
            } else {
                if (!hasButtons) {
                    GameManager.getDialogManger().setShowingDialog(false);
                }
            }
        }
    }

    @Override
    public void drawElement(GameContainer gc, Graphics g) {
        g.setColor(boxColor);
        g.fillRoundRect(xPos, yPos, width, height, 4);
        g.setColor(textColor);
        if (currentPage < pages.size()) {
            for (int i = 0; i < pages.get(currentPage).getLines().size(); i++) {
                if (i < pages.get(currentPage).getLines().size() && pages.get(currentPage).getLines().get(i) != null && !pages.get(currentPage).getLines().get(i).isEmpty()) {
                    g.drawString(pages.get(currentPage).getLines().get(i), xPos, yPos + font.getHeight("I") * i);
                }
            }
        }
        // Using plus one as currentPage is based on a 0 based array
        if (currentPage + 1 == pages.size() && hasButtons) {
            acceptButton.drawElement(gc, g);
            declineButton.drawElement(gc, g);
        }
    }

    public ButtonImageElement getAcceptButton() {
        return acceptButton;
    }

    public void setAcceptButton(ButtonImageElement acceptButton) {
        this.acceptButton = acceptButton;
    }

    public ButtonImageElement getDeclineButton() {
        return declineButton;
    }

    public void setDeclineButton(ButtonImageElement declineButton) {
        this.declineButton = declineButton;
    }

    public boolean isHasButtons() {
        return hasButtons;
    }

    public void setHasButtons(boolean hasButtons) {
        this.hasButtons = hasButtons;
    }

    class Page {

        ArrayList<String> lines = new ArrayList<>();

        public Page(List lines) {
            this.lines.addAll(lines);
        }

        public ArrayList<String> getLines() {
            return lines;
        }

        public void setLines(ArrayList<String> lines) {
            this.lines = lines;
        }
    }
}
