/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.element.action;

import tinyships.enums.gui.GameScreens;
import tinyships.manager.ScreenManager;

/**
 *
 * @author Dean
 */
public class ChangeScreenAction extends BaseElementAction {

    private GameScreens screenId;

    public ChangeScreenAction(GameScreens screenId) {
        this.screenId = screenId;
    }

    @Override
    public boolean hasActionExectuted() {
        ScreenManager.changeScreen(screenId);
        return false;
    }
}
