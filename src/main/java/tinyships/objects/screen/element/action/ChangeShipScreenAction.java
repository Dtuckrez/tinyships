/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tinyships.objects.screen.element.action;

import tinyships.enums.gui.GameScreens;
import tinyships.manager.ShipScreenManager;

/**
 *
 * @author Dean
 */
public class ChangeShipScreenAction extends BaseElementAction {
    
    private final GameScreens screenId;

    public ChangeShipScreenAction(GameScreens screenId) {
        this.screenId = screenId;
    }

    @Override
    public boolean hasActionExectuted() {
        ShipScreenManager.changeScreen(screenId);
        return false;
    }
}
