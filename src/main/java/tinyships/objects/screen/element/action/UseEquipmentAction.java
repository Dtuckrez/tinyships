/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tinyships.objects.screen.element.action;

import tinyships.objects.ships.BaseShip;

/**
 *
 * @author Dean
 */
public class UseEquipmentAction extends BaseElementAction {
    
    BaseShip ship;
    int itemToUse;
    
    public UseEquipmentAction(BaseShip ship, int itemToUse) {
        this.ship = ship;
        this.itemToUse = itemToUse;
    }
    
    @Override
    public boolean hasActionExectuted() {
        if (ship != null) {
            if (itemToUse >= 0 && itemToUse < ship.getEquipmentBays().size()) {
                return ship.useEquipment(itemToUse);
            }
        }
        return false;
    }
    
}
