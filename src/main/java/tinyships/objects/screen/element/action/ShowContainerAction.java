/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tinyships.objects.screen.element.action;

import tinyships.enums.gui.GameScreenContainers;
import tinyships.ui.screens.BaseScreen;

/**
 *
 * @author Dean
 */
public class ShowContainerAction extends BaseElementAction {

    BaseScreen screen;
    GameScreenContainers id;
    public ShowContainerAction(BaseScreen screen, GameScreenContainers id) {
        this.screen = screen;
        this.id = id;
    }
    
    @Override
    public boolean hasActionExectuted() {
        if (screen != null && id != null) {
            screen.getContainerById(id).setHidden(false);
        }
        return false;
    }
}
