/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.element;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.enums.gui.GameScreenElements;
import tinyships.global.Colours;

/**
 *
 * @author Dean
 */
public class SimpleTextElement extends BaseScreenElement {

    protected int xPos;
    protected int yPos;
    protected String text;
    protected Font font;
    protected Color color;

    public SimpleTextElement(GameScreenElements id, String text,
            int xPos, int yPos, Font font) {
        super(id);

        this.xPos = xPos;
        this.yPos = yPos;
        this.text = text;
        this.font = font;
    }

    public SimpleTextElement(GameScreenElements id, String text,
            int xPos, int yPos, Font font, Color color) {
        super(id);

        this.xPos = xPos;
        this.yPos = yPos;
        this.text = text;
        this.font = font;
        this.color = color;
    }

    public int getxPos() {
        return xPos;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public int getyPos() {
        return yPos;
    }

    public void setyPos(int yPos) {
        this.yPos = yPos;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void updateElement(boolean leftDown, GameContainer gc, int delta) {

    }

    public void updateElement(String text, GameContainer gc, int delta) {
        this.text = text;
    }

    @Override
    public void drawElement(GameContainer gc, Graphics g) {
        g.setColor(Colours.TEXTORANGE);
        g.setFont(font);
        g.drawString(text, xPos, yPos);
    }
}
