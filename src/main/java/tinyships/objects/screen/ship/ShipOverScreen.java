/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.ship;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.enums.gui.GameScreenElements;
import tinyships.enums.gui.GameScreens;
import tinyships.ui.screens.BaseScreen;
import tinyships.objects.screen.element.BaseScreenElement;
import tinyships.objects.screen.element.ButtonImageElement;
import tinyships.objects.screen.element.action.ChangeShipScreenAction;

/**
 *
 * @author Dean
 */
public class ShipOverScreen extends BaseScreen {

    public ShipOverScreen(GameScreens screenId, String name, Color color) {
        super(screenId, name, color);
        elements.add(new ButtonImageElement(GameScreenElements.INFO, "images/button/button.png",
                "images/button/info.png", 130, 700, new ChangeShipScreenAction(GameScreens.TARGETSCEEN)));
        elements.add(new ButtonImageElement(GameScreenElements.WEAPON, "images/button/button.png",
                "images/button/info.png", 200, 700, new ChangeShipScreenAction(GameScreens.SHIPWEAPONSCREEN)));
        elements.add(new ButtonImageElement(GameScreenElements.CARGO, "images/button/button.png",
                "images/button/ore.png", 270, 700, new ChangeShipScreenAction(GameScreens.SHIPCARGOSCREEN)));
    }

    @Override
    public void updateScreen(boolean leftDown, GameContainer gc, int delta) {
        for (BaseScreenElement element : elements) {
            element.updateElement(leftDown, gc, delta);
        }
    }

    @Override
    public void drawScreen(GameContainer gc, Graphics g) {
            g.setColor(overlayColor);
            g.fillRect(0, 0, 1024, 768);
        
        for (BaseScreenElement element : elements) {
            element.drawElement(gc, g);
        }
    }
}
