/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tinyships.objects.screen.ship;

import java.util.ArrayList;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import tinyships.enums.gui.GameScreenContainers;
import tinyships.enums.gui.GameScreenElements;
import tinyships.enums.gui.GameScreens;
import tinyships.global.Colours;
import tinyships.global.Fonts;
import tinyships.manager.GameManager;
import tinyships.objects.cargo.EquipmentCargo;
import tinyships.ui.screens.BaseScreen;
import tinyships.objects.screen.container.BaseContainer;
import tinyships.objects.screen.element.BaseScreenElement;
import tinyships.objects.screen.element.ButtonImageElement;
import tinyships.objects.screen.element.DragDropImageElement;
import tinyships.objects.screen.element.HorizontalSliderElement;
import tinyships.objects.screen.element.ImageBoxElement;
import tinyships.objects.screen.element.MultiLineTextBox;
import tinyships.objects.screen.element.SimpleTextElement;
import tinyships.objects.screen.element.TextBoxElement;
import tinyships.objects.ships.PlayerShip;
import tinyships.objects.ships.cargobay.HeldCargo;
import tinyships.objects.ships.weapons.BaseWeapon;
import tinyships.callbacks.ButtonCallback;

/**
 *
 * @author Dean
 */
public class ShipWeaponScreen extends BaseScreen implements ButtonCallback {

    private boolean needsToUpdate;
    public final int ITEMSPERPAGE = 12;

    ArrayList<BaseWeapon> baseWeapons = new ArrayList<>();

    public ShipWeaponScreen(GameScreens screenId, String name, Color color) {
        super(screenId, name, color);
        BaseContainer currentWeaponsContainer = new BaseContainer(GameScreenContainers.SHIPWEAPON, "", 204, 128, 232, 295);
        containers.add(currentWeaponsContainer);

        // Ship Current Equipped Weapons
        BaseContainer container = new BaseContainer(GameScreenContainers.INFO, "", 204, 427, 232, 175);

        container.elements.add(new TextBoxElement(GameScreenElements.INFO, "Weapon: ", "Weapon Name Here", 204, 427, 232, 22, Colours.TEXTORANGE, Colours.BOXBLUE, Fonts.infomationFont));
        container.elements.add(new TextBoxElement(GameScreenElements.BASEELEMENT, "Description: ", "", 204, 467, 232, 22, Colours.TEXTORANGE, Colours.BOXBLUE, Fonts.infomationFont));
        container.elements.add(new MultiLineTextBox(GameScreenElements.DESCRIPTION, "", Fonts.infomationFont, 204, 487, 232, 115, Colours.TEXTORANGE, Colours.BOXBLUE));

        containers.add(container);

        containers.add(new BaseContainer(GameScreenContainers.STATS, "", 204, 487 + 135, 0, 0));

        container.addElement(new SimpleTextElement(GameScreenElements.BASEELEMENT, "Hull Damage:", 204, 487 + 115, Fonts.infomationFont, Colours.TEXTORANGE));
        container.addElement(new SimpleTextElement(GameScreenElements.BASEELEMENT, "Shield Damage:", 204, 487 + 135, Fonts.infomationFont, Colours.TEXTORANGE));
        container.addElement(new SimpleTextElement(GameScreenElements.BASEELEMENT, "Fire Rate:", 204, 487 + 155, Fonts.infomationFont, Colours.TEXTORANGE));
        container.addElement(new SimpleTextElement(GameScreenElements.BASEELEMENT, "Range:", 204, 487 + 175, Fonts.infomationFont, Colours.TEXTORANGE));

        // Ship equipment in cargo hold
        BaseContainer weaponsContainer = new BaseContainer(GameScreenContainers.CARGO, "", 614, 128, 264, 295, Colours.BOXBLUE);

        BaseContainer weaponsContainerInfo = new BaseContainer(GameScreenContainers.SHIPEQUIPMENT, "", 612, 427, 232, 175);

        weaponsContainerInfo.elements.add(new TextBoxElement(GameScreenElements.INFO, "Weapon: ", "Weapon Name Here", 612, 427, 232, 22, Colours.TEXTORANGE, Colours.BOXBLUE, Fonts.infomationFont));
        weaponsContainerInfo.elements.add(new TextBoxElement(GameScreenElements.BASEELEMENT, "Description: ", "", 612, 467, 232, 22, Colours.TEXTORANGE, Colours.BOXBLUE, Fonts.infomationFont));
        weaponsContainerInfo.elements.add(new MultiLineTextBox(GameScreenElements.DESCRIPTION, "", Fonts.infomationFont, 612, 487, 232, 115, Colours.TEXTORANGE, Colours.BOXBLUE));
        HorizontalSliderElement pageScroller = new HorizontalSliderElement(GameScreenElements.CURRENTNUMBER, 878, 128, 16, 295, 15, Colours.BOXBLUE, Colours.TEXTORANGE, Colours.WHITE, Fonts.dialogFont, null);
        weaponsContainerInfo.elements.add(pageScroller);

        containers.add(weaponsContainer);
        containers.add(weaponsContainerInfo);
        needsToUpdate = true;

    }

    @Override
    public void updateScreen(boolean leftDown, GameContainer gc, int delta) {
        for (BaseScreenElement element : elements) {
            element.updateElement(leftDown, gc, delta);
        }

        for (BaseContainer container : containers) {
            if (container.containerId.equals(GameScreenContainers.SHIPEQUIPMENT)) {
                HorizontalSliderElement element = (HorizontalSliderElement) container.getElementViaId(GameScreenElements.CURRENTNUMBER);
                if (element.hasBeenClicked(leftDown, gc, delta)) {
                    needsToUpdate = true;
                }
            }
            container.updateContainer(leftDown, gc, delta);
        }
//        if (needsToUpdate) {
//            BaseContainer currentWeaponsContainer = getContainerById(GameScreenContainers.SHIPWEAPON);
//
//            currentWeaponsContainer.elements.clear();
//            PlayerShip ship = GameManager.getPlayer().getShip();
//            if (ship != null && ship.getLeftWeapon()!= null) {
//                DragDropImageElement buttonImageElement = new DragDropImageElement(GameScreenElements.WEAPON,
//                        ship.getLeftWeapon().getBaseWeapon().getImage().getResourceReference(),
//                        "images/button/button.png", 288, 168, null);
//                buttonImageElement.setButton(ship.getLeftWeapon().getBaseWeapon().getImage());
//                buttonImageElement.setIcon(ship.getLeftWeapon().getBaseWeapon().getImage());
//                buttonImageElement.setCallback(this);
//                currentWeaponsContainer.elements.add(buttonImageElement);
//            }
//            needsToUpdate = false;
//            UpdateWeaponsFromCargo();
//        }
    }

    public void updateCurrentWeaponInfo() {
//        if (!GameManager.isHolding) {
//            BaseContainer container = getContainerById(GameScreenContainers.INFO);
//            BaseContainer equipmentWeaponsContainer = getContainerById(GameScreenContainers.SHIPWEAPON);
//            TextBoxElement nameBox = (TextBoxElement) container.getElementViaId(GameScreenElements.INFO);
//            MultiLineTextBox textBox = (MultiLineTextBox) container.getElementViaId(GameScreenElements.DESCRIPTION);
//
//            // Check for front weapons
//            for (int i = 0; i < equipmentWeaponsContainer.elements.size(); i++) {
//                if (equipmentWeaponsContainer.elements.get(i) instanceof ButtonImageElement) {
//                    BaseWeapon weapon = GameManager.getPlayer().getShip().getForwardWeapon().getBaseWeapon();
//                    if (((DragDropImageElement) equipmentWeaponsContainer.elements.get(i)).isHover()) {
//                        nameBox.setText(weapon.getName());
//                        textBox.updatePages(weapon.getDescription(), false);
//                        addHullDamageBar(weapon);
//                    }
//                }
//            }
//        }
    }

    public void updateCurrentCargoInfo() {
        BaseContainer weaponInfoContainer = getContainerById(GameScreenContainers.SHIPEQUIPMENT);
        TextBoxElement weaponName = null;
        MultiLineTextBox weaponDescription = null;
        if (weaponInfoContainer != null) {
            weaponName = (TextBoxElement) weaponInfoContainer.getElementViaId(GameScreenElements.INFO);
            weaponDescription = (MultiLineTextBox) weaponInfoContainer.getElementViaId(GameScreenElements.DESCRIPTION);
        }

        BaseContainer cargoWeapons = getContainerById(GameScreenContainers.CARGO);

        for (int i = 0; i < cargoWeapons.elements.size(); i++) {
            if (cargoWeapons.elements.get(i) instanceof DragDropImageElement) {
                BaseWeapon weapon = getWeaponFromCargo(((DragDropImageElement) cargoWeapons.elements.get(i)).getIndex());
                if (((DragDropImageElement) cargoWeapons.elements.get(i)).isHover()) {
                    if (weaponDescription != null && weaponName != null) {
                        weaponName.setText(weapon.getName());
                        weaponDescription.updatePages(weapon.getDescription(), false);
                    }
                }
            }
        }
    }

    private void addHullDamageBar(BaseWeapon weapon) {
        BaseContainer statsContainer = getContainerById(GameScreenContainers.STATS);
        if (statsContainer.elements.isEmpty()) {
            for (int i = 0; i < weapon.getHullDamage(); i++) {
                statsContainer.addElement(new ImageBoxElement(GameScreenElements.HULL, 210 + 110 + (i * 10), 490 + 115, 8, 14, overlayColor,
                        "images/player/blip.png"));
            }
        }
    }

    private void UpdateWeaponsFromCargo() {

        ArrayList<LookUpItem> lookUpItems = new ArrayList<>();

        BaseContainer cargoWeapons = getContainerById(GameScreenContainers.CARGO);
        cargoWeapons.elements.clear();
        GameManager.isHolding = false;

        int COLUMNS = 4;
        int ROWS = 0;

        // get all of the weapons
        for (int i = 0; i < GameManager.getPlayer().getShip().getCargo().size(); i++) {
            HeldCargo cargo = GameManager.getPlayer().getShip().getCargo().get(i);
            if (cargo.getCargo() instanceof EquipmentCargo) {
                if (((EquipmentCargo) cargo.getCargo()).getEquipment() instanceof BaseWeapon) {
                    lookUpItems.add(new LookUpItem(cargo.getCargo().getImage(), i));
                }
            }
        }

        BaseContainer infoContainer = getContainerById(GameScreenContainers.SHIPEQUIPMENT);
        HorizontalSliderElement sliderElement = (HorizontalSliderElement) infoContainer.getElementViaId(GameScreenElements.CURRENTNUMBER);
        sliderElement.setMaxNumber((int) (lookUpItems.size() > 0 ? Math.ceil((float) lookUpItems.size() / 12) : 1));
        int curPage = sliderElement.getCurrentNumber();
        int currentNumber = 0;
        if (lookUpItems.size() > 0) {
            for (int i = ((curPage - 1) * 12); i < ((curPage - 1) * 12) + 12; i++) {
                if (i < lookUpItems.size()) {
                    int col = currentNumber % COLUMNS;
                    int row = ROWS + currentNumber / COLUMNS;

                    int xPos = cargoWeapons.xPos;
                    int yPos = cargoWeapons.yPos;

                    DragDropImageElement element = new DragDropImageElement(GameScreenElements.SHIP,
                            "images/button/button.png",
                            "images/button/weapon.png", xPos + 5 + (col * 64), yPos + (100 * row), null, i);
                    element.setIndex(lookUpItems.get(i).index);
                    element.setIcon(lookUpItems.get(i).image);
                    element.setCallback(this);
                    cargoWeapons.addElement(element);

                    currentNumber++;
                }
            }
        }
    }

    @Override
    public void drawScreen(GameContainer gc, Graphics g) {
        g.setColor(this.overlayColor);
        g.fillRect(0, 0, 1024, 768);
        g.fillRoundRect(128, 96, 768, 590, 8);

        for (BaseContainer container : containers) {
            container.drawContainer(gc, g);
        }
        g.setColor(Colours.TEXTORANGE);
        for (BaseScreenElement element : elements) {
            element.drawElement(gc, g);
        }
    }

    @Override
    public void processHover(ButtonImageElement buttonImageElement) {
        updateCurrentWeaponInfo();
        updateCurrentCargoInfo();
    }

    @Override
    public void processClick() {
        // Check if cargo is being dragged 
        BaseContainer cargoContainer = getContainerById(GameScreenContainers.CARGO);

        boolean toCheck = true;
        for (BaseScreenElement cargoButton : cargoContainer.elements) {
            if (cargoButton instanceof DragDropImageElement) {
                DragDropImageElement button = (DragDropImageElement) cargoButton;
                if (button.isIsSelected()) {
                    swapCargoToWeapon(button);
                    toCheck = false;
                    button.setIsSelected(false);
                    return;
                }
            }
        }
    }

    private void swapCargoToWeapon(DragDropImageElement button) {
//        int buttonX = button.getCurrentX();
//        int buttonY = button.getCurrentY();
//
//        int size = button.getIcon().getWidth();
//
//        // Get Weapons from Equipped Weapons
//        BaseContainer container = getContainerById(GameScreenContainers.SHIPWEAPON);
//
//        if (container != null) {
//            for (BaseScreenElement weaponElement : container.elements) {
//                if (weaponElement instanceof DragDropImageElement) {
//                    DragDropImageElement element = (DragDropImageElement) weaponElement;
//                    int elementx = element.getCurrentX();
//                    int elementy = element.getCurrentY();
//                    int elementSize = element.getIcon().getWidth();
//
//                    if (buttonX + size > elementx
//                            && buttonX < elementx + elementSize
//                            && buttonY + size > elementy
//                            && buttonY < elementy + elementSize) {
//                        BaseWeapon weaponA = GameManager.getPlayer().getShip().getLeftWeapon().getBaseWeapon();
//                        BaseWeapon weaponB = getWeaponFromCargo(button.getIndex());
//
//                        GameManager.getPlayer().getShip().swapWeapons(weaponA, weaponB);
//                        needsToUpdate = true;                      
//                    }
//                }
//            }
//        }
    }

    public BaseWeapon getWeaponFromCargo(int index) {
        HeldCargo cargo = GameManager.getPlayer().getShip().getCargo().get(index);
        if (cargo.getCargo() instanceof EquipmentCargo) {
            if (((EquipmentCargo) cargo.getCargo()).getEquipment() instanceof BaseWeapon) {
                return (BaseWeapon) ((EquipmentCargo) cargo.getCargo()).getEquipment();
            }
        }
        return new BaseWeapon();
    }

    private class LookUpItem {

        Image image = null;
        int index = 0;

        public LookUpItem(Image image, int index) {
            this.image = image;
            this.index = index;
        }

    }
}
