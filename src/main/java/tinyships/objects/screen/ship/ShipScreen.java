/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.ship;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import tinyships.enums.gui.GameScreenContainers;
import tinyships.enums.gui.GameScreenElements;
import tinyships.enums.gui.GameScreens;
import tinyships.global.Fonts;
import tinyships.ui.screens.BaseScreen;
import tinyships.objects.screen.container.BaseContainer;
import tinyships.objects.screen.element.BaseScreenElement;
import tinyships.objects.screen.element.ButtonImageElement;
import tinyships.objects.screen.element.SimpleTextElement;
import tinyships.objects.ships.BaseShip;

/**
 *
 * @author Dean
 */
public class ShipScreen extends BaseScreen {

    protected BaseShip ship;
    protected boolean update = true;

    public ShipScreen(GameScreens id, String name, Color color) {
        super(id, name, color);

        elements.add(new SimpleTextElement(GameScreenElements.SHIPNAME, "Ship Name: ", 10, 55, Fonts.titleFont));
        elements.add(new SimpleTextElement(GameScreenElements.SHIPTYPE, "Type: ", 10, 80, Fonts.titleFont));
        elements.add(new SimpleTextElement(GameScreenElements.POWER, "Power: ", 10, 105, Fonts.titleFont));
        elements.add(new SimpleTextElement(GameScreenElements.HULL, "Hull: ", 10, 130, Fonts.titleFont));
        elements.add(new SimpleTextElement(GameScreenElements.SHIELD, "Shield: ", 10, 155, Fonts.titleFont));
        elements.add(new SimpleTextElement(GameScreenElements.CARGOSIZE, "Cargo: ", 10, 175, Fonts.titleFont));

        elements.add(new SimpleTextElement(GameScreenElements.DESCRIPTION, "", 10, 200, Fonts.titleFont));
        elements.add(new SimpleTextElement(GameScreenElements.LINEONE, "", 10, 225, Fonts.titleFont));
        elements.add(new SimpleTextElement(GameScreenElements.LINETWO, "", 10, 250, Fonts.titleFont));
        elements.add(new SimpleTextElement(GameScreenElements.LINETHREE, "", 10, 275, Fonts.titleFont));
        elements.add(new SimpleTextElement(GameScreenElements.LINEFOUR, "", 10, 300, Fonts.titleFont));
        elements.add(new SimpleTextElement(GameScreenElements.LINEFIVE, "", 10, 325, Fonts.titleFont));
        elements.add(new SimpleTextElement(GameScreenElements.LINESIX, "", 10, 350, Fonts.titleFont));
        elements.add(new SimpleTextElement(GameScreenElements.LINESIX, "", 10, 350, Fonts.titleFont));

        elements.add(new SimpleTextElement(GameScreenElements.CREW, "Crew:", 10, 390, Fonts.titleFont));

        elements.add(new SimpleTextElement(GameScreenElements.WEAPON, "Weapons", 612, 55, Fonts.titleFont));
        elements.add(new SimpleTextElement(GameScreenElements.EQUIPMENT, "Equipment", 612, 162, Fonts.titleFont));
        elements.add(new SimpleTextElement(GameScreenElements.CARGO, "Cargo", 612, 275, Fonts.titleFont));

        containers.add(new BaseContainer(GameScreenContainers.WEAPON, "Weapons", 612, 55, 412, 100));
        containers.add(new BaseContainer(GameScreenContainers.EQUIPMENT, "Equipment", 612, 162, 412, 100));
        containers.add(new BaseContainer(GameScreenContainers.CARGO, "Cargo", 612, 275, 412, 500));
        containers.add(new BaseContainer(GameScreenContainers.CREW, "Crew:", 10, 390, 412, 500));
    }

    public BaseShip getShip() {
        return ship;
    }

    public void setShip(BaseShip ship) {
        this.ship = ship;
    }

    @Override
    public void updateScreen(boolean leftDown, GameContainer gc, int delta) {
        hoverOverButtons(leftDown, gc, delta);

        if (ship != null) {
            ((SimpleTextElement) getElementById(GameScreenElements.SHIPNAME)).setText("Ship Name: " + ship.getName());
            ((SimpleTextElement) getElementById(GameScreenElements.SHIPTYPE)).setText("Ship Type: " + ship.getArmyType());
            ((SimpleTextElement) getElementById(GameScreenElements.HULL)).setText("Hull: " + ship.getHull() + " / " + ship.getMaxHull());
            if (ship.getShield() != null) {
                ((SimpleTextElement) getElementById(GameScreenElements.SHIELD)).setText("Shield: " + ship.getShield().getPower() + " / " + ship.getShield().getMaxPower());
            }
            ((SimpleTextElement) getElementById(GameScreenElements.POWER)).setText("Power: ");
            ((SimpleTextElement) getElementById(GameScreenElements.CARGOSIZE)).setText("Cargo: " + 1 + " / " + ship.getCargoSize());
            String split[] = ship.getDescription().split("/n");

            if (split.length >= 1) {
                ((SimpleTextElement) getElementById(GameScreenElements.LINEONE)).setText(split[0].trim());
            }
            if (split.length >= 2) {
                ((SimpleTextElement) getElementById(GameScreenElements.LINETWO)).setText(split[1].trim());
            }
            if (split.length >= 3) {
                ((SimpleTextElement) getElementById(GameScreenElements.LINETHREE)).setText(split[2].trim());
            }
            if (split.length >= 4) {
                ((SimpleTextElement) getElementById(GameScreenElements.LINEFOUR)).setText(split[3].trim());
            }
            if (split.length >= 5) {
                ((SimpleTextElement) getElementById(GameScreenElements.LINEFIVE)).setText(split[4].trim());
            }
            if (split.length >= 6) {
                ((SimpleTextElement) getElementById(GameScreenElements.LINESIX)).setText(split[5].trim());
            }

            if (ship.getIsEquipmentUpdated()) {
                processWeapons();
                processEquipment();
                processCargo();
                ship.setIsEquipmentUpdated(false);
            }
        }
    }

    private void hoverOverButtons(boolean leftDown, GameContainer gc, int delta) {
        Input input = gc.getInput();
        // Weapons
        for (BaseScreenElement baseScreenElement : getContainerById(GameScreenContainers.WEAPON).elements) {
            baseScreenElement.updateElement(leftDown, gc, delta);
            if (((ButtonImageElement) baseScreenElement).isButtonPressed(true)) {
                processCargo();
            }
        }
        // Equipment
        for (BaseScreenElement baseScreenElement : getContainerById(GameScreenContainers.EQUIPMENT).elements) {
            baseScreenElement.updateElement(leftDown, gc, delta);
            if (((ButtonImageElement) baseScreenElement).isButtonPressed(true)) {
                processCargo();
            }
        }
    }

    private void processWeapons() {
//        BaseWeaponBay weaponBay = ship.getWeaponBay();
//        for (int i = 0; i < weaponBay.getMaxWeapons(); i++) {
//            ButtonImageElement button;
//            if (i < weaponBay.getWeapons().size()) {
//                button = new ButtonImageElement(
//                        GameScreenElements.BASEELEMENT,
//                        "images/button/button.png",
//                        "images/button/weapon.png", null);
//            } else {
//                button = new ButtonImageElement(
//                        GameScreenElements.BASEELEMENT,
//                        "images/button/button.png",
//                        "images/button/trade.png", null);
//            }
//            button.setxPos(612 + (i * button.getWidth()));
//            button.setyPos(90);
//
//            getContainerById(GameScreenContainers.WEAPON).addElement(button);
//        }
    }

    private void processEquipment() {
        // Shield
        // Equipment   
        for (int i = 0; i < ship.getEquipmentBaySize(); i++) {
            ButtonImageElement button = new ButtonImageElement(
                    GameScreenElements.EQUIPMENT,
                    "images/button/button.png",
                    "images/button/equipment.png", null);
            button.setxPos(612 + (i * button.getWidth()));
            button.setyPos(197);

            getContainerById(GameScreenContainers.EQUIPMENT).addElement(button);
        }
    }

    private void processCargo() {
        getContainerById(GameScreenContainers.CARGO).elements.clear();
        for (int i = 0; i < ship.getCargo().size(); i++) {
//            InfomationButtonTextElement button = new InfomationButtonTextElement(GameScreenElements.CARGO,
//                    "images/button/button.png",
//                    "images/button/ship.png",
//                    String.valueOf(ship.getCargo().get(i).getEquipment().getName()));
//            button.setxPos(612 + (i * button.getWidth()));
//            button.setyPos(275 + 35);
//            getContainerById(GameScreenContainers.CARGO).addElement(button);
        }
    }

    private void processCrew() {

    }

    @Override
    public void drawScreen(GameContainer gc, Graphics g) {
        g.setColor(new Color(0, 0.3f, 0.7f, 0.6f));
        g.fillRect(0, 0, 1024, 768);
        for (BaseScreenElement element : elements) {
            element.drawElement(gc, g);
        }

        for (BaseContainer container : containers) {
            container.drawContainer(gc, g);
        }
    }
}
