/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tinyships.objects.screen.ship;

import java.util.List;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.callbacks.ScrollBarCallBack;
import tinyships.enums.gui.GameScreenContainers;
import tinyships.enums.gui.GameScreenElements;
import tinyships.enums.gui.GameScreens;
import tinyships.global.Colours;
import tinyships.global.Fonts;
import tinyships.manager.GameManager;
import tinyships.ui.screens.BaseScreen;
import tinyships.objects.screen.container.BaseContainer;
import tinyships.objects.screen.element.BaseScreenElement;
import tinyships.objects.screen.element.DragDropImageElement;
import tinyships.objects.screen.element.HorizontalSliderElement;
import tinyships.objects.screen.element.TextBoxElement;
import tinyships.objects.ships.cargobay.HeldCargo;
import tinyships.callbacks.ButtonCallback;
import tinyships.objects.screen.element.ButtonImageElement;

/**
 *
 * @author Dean
 */
public class ShipCargoScreen extends BaseScreen implements ButtonCallback, ScrollBarCallBack {

    HorizontalSliderElement pageSlider;
    TextBoxElement infoTextBox;
    ButtonImageElement trashButton;
    
    public ShipCargoScreen(GameScreens screenId, String name, Color color) {
        super(screenId, name, color);

        BaseContainer cargoContainer = new BaseContainer(GameScreenContainers.CARGO, "", 0, 0, 0, 0);
        containers.add(cargoContainer);
        trashButton = new DragDropImageElement(GameScreenElements.BASEELEMENT, "images/button/trash.png", "images/button/trash.png", 795, 600, null);
        elements.add(trashButton);
        infoTextBox = new TextBoxElement(GameScreenElements.INFO, "INFO", "IJDIFJDJFDIJFDIFJDIJFIDF", 
                164, 590, 600, 90, Colours.TEXTORANGE, Colours.BOXBLUE, Fonts.dialogFont);
        elements.add(infoTextBox);
        
        pageSlider = new HorizontalSliderElement(GameScreenElements.SLIDER, 860, 100, 32, 450, 1, Colours.TEXTORANGE, Colours.BOXGREEN, Colours.DARKGREY, Fonts.dialogFont, this);
        elements.add(pageSlider);

        updateCargo();

    }

    public void updateCargo() {
        // Get the ships cargo

        // Returns a class that holds elements
        BaseContainer container = getContainerById(GameScreenContainers.CARGO); 
        container.removeElements();
        // Finds A UI scroll Bar
        int cargoSize = GameManager.getPlayer().getShip().getCargo().size();
        int amount = Math.round((float)cargoSize / (float)45);
        pageSlider.setMaxNumber(amount);
        
        int COLUMNS = 9;
        int ROWS = 0;

        int curPage = pageSlider.getCurrentNumber();
        int currentNumber = 0;

        int lastNumber = 0;
        int startNumber = 0;
        
        
        
        lastNumber = (curPage) * 45;
        if (lastNumber > cargoSize) {
            lastNumber = cargoSize;
            int diff = (curPage * 45) - cargoSize;
            startNumber = ((curPage - 1) * 45);
        } else {
            startNumber = lastNumber - 45;
        }

        if (startNumber < 0) {
            startNumber = 0;
        }
        
        List<HeldCargo> cargoToShow = GameManager.getPlayer().getShip().getCargo().subList(startNumber, lastNumber);

        if (cargoToShow.size() > 0) {
            for (int i = startNumber; i < lastNumber; i++) {
//            for (int i = ((curPage - 1) * cargoToShow.size()); i < ((curPage - 1) * cargoToShow.size()) + cargoToShow.size(); i++) {
//                if (i < cargoToShow.size()) {
                    int col = currentNumber % COLUMNS;
                    int row = ROWS + currentNumber / COLUMNS;

                    // Crears a drag_drop element ui stuff.
                    DragDropImageElement element = new DragDropImageElement(GameScreenElements.SHIP,
                            "images/button/button.png",
                            "images/button/ore.png", 128 + 36 + (col * 75), 104 + (100 * row), null, i);
                    element.setCallback(this);

                    container.addElement(element);

                    currentNumber++;
//                }
            }
        }
    }

    @Override
    public void processHover(ButtonImageElement buttonImageElement) {
        HeldCargo cargo = GameManager.getPlayer().getShip().getCargo().get(buttonImageElement.getIndex());
        infoTextBox.setName(cargo.getCargo().getName());
    }

    @Override
    public void processClick() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void drawScreen(GameContainer gc, Graphics g) {
        g.setColor(this.overlayColor);
        g.fillRect(0, 0, 1024, 768);
        g.fillRoundRect(128, 96, 768, 590, 8);

        for (BaseContainer container : containers) {
            container.drawContainer(gc, g);
        }
        
        for (BaseScreenElement element : elements) {
            element.drawElement(gc, g);
        }
    }

    @Override
    public void onScroll() {
        updateCargo();
    }
}
