/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tinyships.objects.screen.ship;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.enums.gui.GameScreens;
import tinyships.ui.screens.BaseScreen;

/**
 *
 * @author Dean
 */
public class ShipEquipmentScreen extends BaseScreen {

    public ShipEquipmentScreen(GameScreens screenId, String name, Color color) {
        super(screenId, name, color);
    }
    
    @Override
    public void drawScreen(GameContainer gc, Graphics g) {
        super.drawScreen(gc, g);
    }
    
}
