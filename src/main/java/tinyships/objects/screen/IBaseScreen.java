/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

/**
 *
 * @author Dean
 */
public interface IBaseScreen {

    void updateScreen(GameContainer gc, int delta);

    void drawScreen(GameContainer gc, Graphics g);
}
