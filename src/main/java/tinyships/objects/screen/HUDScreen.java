/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import tinyships.objects.player.Player;
import tinyships.enums.gui.GameScreenContainers;
import tinyships.enums.gui.GameScreenElements;
import tinyships.enums.gui.GameScreens;
import tinyships.global.Fonts;
import tinyships.manager.GameManager;
import tinyships.objects.screen.element.BaseScreenElement;
import tinyships.objects.screen.container.BaseContainer;
import tinyships.objects.screen.container.HoverHideContainer;
import tinyships.objects.screen.element.ButtonImageElement;
import tinyships.objects.screen.element.HorizontalPercentBar;
import tinyships.objects.screen.element.HoverImageElementButton;
import tinyships.objects.screen.element.SimpleTextElement;
import tinyships.objects.screen.element.action.ShowContainerAction;
import tinyships.objects.screen.element.action.UseEquipmentAction;
import tinyships.objects.ships.BaseShip;
import tinyships.ui.screens.BaseScreen;

/**
 *
 * @author Dean
 */
public final class HUDScreen extends BaseScreen {

    public String playerTarget = "";
    private final Color lowerBar = new Color(0.3f, 0.3f, 0.3f, 0.6f);
    private Image targetArrow;
    private boolean updateEquipmentSlots;
    private boolean isLeftDown;

    public HUDScreen(GameScreens id, String name, Color color) {
        super(id, name, color);

        elements.add(new SimpleTextElement(GameScreenElements.SHIPNAME, "", 10, 615, Fonts.infomationFont));
        elements.add(new HorizontalPercentBar(GameScreenElements.HEATHBAR, 10, 645, 100, 20, lowerBar, Color.green));
        elements.add(new HorizontalPercentBar(GameScreenElements.SHIELDBAR, 10, 670, 100, 20, lowerBar, Color.blue));
        containers.add(new BaseContainer(GameScreenContainers.ENTITY, "target", 0, 0, 0, 0));

        containers.add(new HoverHideContainer(GameScreenContainers.WEAPON, "", 150, 615 - 300, 64, 375));
        containers.add(new HoverHideContainer(GameScreenContainers.EQUIPMENT, "", 225, 615 - 300, 64, 375));

//        elements.add(new HoverImageElementButton(GameScreenElements.WEAPON, "images/button/button.png", "images/button/button.png", 150, 630, new ShowContainerAction(this, GameScreenContainers.WEAPON)));
//        elements.add(new HoverImageElementButton(GameScreenElements.EQUIPMENT, "images/button/button.png", "images/button/button.png", 225, 630, new ShowContainerAction(this, GameScreenContainers.EQUIPMENT)));

        // Target Info
        BaseContainer baseContainer = getContainerById(GameScreenContainers.ENTITY);

        baseContainer.elements.add(new SimpleTextElement(GameScreenElements.TARGETNAME, "", 1020 - 280, 615, Fonts.infomationFont));
        baseContainer.elements.add(new HorizontalPercentBar(GameScreenElements.TARGETHEALTH, 1020 - 280, 645, 100, 20, lowerBar, Color.green));
        baseContainer.elements.add(new HorizontalPercentBar(GameScreenElements.TARGETSHIELD, 1020 - 280, 670, 100, 20, lowerBar, Color.blue));

        try {
            targetArrow = new Image("images/player/target.png");
        } catch (SlickException ex) {
            Logger.getLogger(HUDScreen.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void updateScreen(boolean leftDown, GameContainer gc, int delta) {
        isLeftDown = leftDown;
    }

    private void updateWeaponsContainers(Player player, GameContainer gc) {
        BaseContainer container = getContainerById(GameScreenContainers.WEAPON);
//        container.yPos = 600 - (64 * player.getShip().getWeaponBay().getMaxWeapons());
        container.height = 645 - container.yPos;
        container.elements.clear();
//        for (int i = 0; i < player.getShip().getWeaponBay().getMaxWeapons(); i++) {
//            ButtonImageElement element = new ButtonImageElement(GameScreenElements.WEAPONEQUIPPED,
//                    "images/button/button.png",
//                    "images/button/weapon.png",
//                    150, 615 - 60 - (i * 70), null);
//
//            container.addElement(element);
//        }
    }

    private void updateEquipmentContainers(Player player, GameContainer gc) {
        BaseContainer container = getContainerById(GameScreenContainers.EQUIPMENT);
        container.yPos = 600 - (64 * player.getShip().getEquipmentBays().size());
        container.height = 645 - container.yPos;
        container.elements.clear();
        for (int i = 0; i < player.getShip().getEquipmentBays().size(); i++) {
            ButtonImageElement element = new ButtonImageElement(GameScreenElements.EQUIPMENTEQUIPPED,
                    "images/button/button.png",
                    "images/button/weapon.png",
                    225, 615 - 60 - (i * 70), new UseEquipmentAction(player.getShip(), i));
            element.setTag(i);

            container.addElement(element);
        }
    }

    public void updateScreen(Player player, GameContainer gc, int delta) {
        BaseShip ship = player.getShip();
        ((SimpleTextElement) getElementById(GameScreenElements.SHIPNAME)).setText(ship.getName());
        ((HorizontalPercentBar) getElementById(GameScreenElements.HEATHBAR)).maxValue = ship.getMaxHull();
        ((HorizontalPercentBar) getElementById(GameScreenElements.HEATHBAR)).value = ship.getHull();
        if (player.getShip().getShield() != null) {
            ((HorizontalPercentBar) getElementById(GameScreenElements.SHIELDBAR)).maxValue = ship.getShield().getMaxPower();
            ((HorizontalPercentBar) getElementById(GameScreenElements.SHIELDBAR)).value = ship.getShield().getPower();
        }
        ship.setIsEquipmentUpdated(false);
        if (!ship.getIsEquipmentUpdated()) {
            updateWeaponsContainers(player, gc);
            updateEquipmentContainers(player, gc);
            ship.setIsEquipmentUpdated(true);
        }

        if (player.getShip().getTargetEntity() != null) {
            BaseContainer container = getContainerById(GameScreenContainers.ENTITY);
            ((SimpleTextElement) container.getElementViaId(GameScreenElements.TARGETNAME)).setText(player.getShip().getTargetEntity().getName());
//            ((HorizontalPercentBar) elements.get(GameScreenElements.TARGETHEALTH.getGameScreenElementValue())).maxValue = ship.getMaxHull();
//            ((HorizontalPercentBar) elements.get(GameScreenElements.TARGETHEALTH.getGameScreenElementValue())).value = ship.getHull();
        }

        for (BaseContainer baseContainer : containers) {
            if (baseContainer.containerId.equals(GameScreenContainers.EQUIPMENT)) {
                if (!baseContainer.getHidden()) {
                    baseContainer.updateContainer(isLeftDown, gc, delta);
                }
            } else if (baseContainer.containerId.equals(GameScreenContainers.WEAPON)) {
                if (!baseContainer.getHidden()) {
                    baseContainer.updateContainer(isLeftDown, gc, delta);
                }
            } else {
                baseContainer.updateContainer(isLeftDown, gc, delta);
            }
        }
        for (BaseScreenElement element : elements) {
            element.updateElement(isLeftDown, gc, delta);
        }

        if (ship.getTargetEntity() != null) {
            float xDistance = ship.getTargetEntity().getXpos() - ship.getPosition().x;
            float yDistance = ship.getTargetEntity().getYpos() - ship.getPosition().y;
            double angleToTurn = Math.toDegrees(Math.atan2(yDistance, xDistance));
            targetArrow.setRotation((float) angleToTurn);
        }
    }

    @Override
    public void drawScreen(GameContainer gc, Graphics g) {
        //Draw Controlled Ship Area        
        g.setLineWidth(5);
        g.setColor(new Color(0, 0.3f, 0.7f, 0.6f));
        g.fillRoundRect(4, gc.getHeight() - 150, 300, 145, 8);
        g.setColor(new Color(0, 0.3f, 0.7f, 0.1f));
        g.drawRoundRect(4, gc.getHeight() - 150, 300, 145, 8);

        //Draw Target Area
        g.setColor(overlayColor);
        g.drawString(playerTarget, 10, 575);

        if (isScreenActive) {
            for (BaseContainer container : containers) {
                if (container != null) {
                    if (!container.containerId.equals(GameScreenContainers.ENTITY)) {
                        container.drawContainer(gc, g);
                    } else {
                        if (GameManager.getPlayer().getShip().getTargetEntity() != null) {
                            g.setLineWidth(5);
                            g.setColor(new Color(0, 0.3f, 0.7f, 0.6f));
                            g.fillRoundRect(1024 - 290, gc.getHeight() - 150, 290, 145, 8);
                            g.setColor(new Color(0, 0.3f, 0.7f, 0.1f));
                            g.drawRoundRect(1020 - 290, gc.getHeight() - 150, 290, 145, 8);
                            container.drawContainer(gc, g);
                        }
                    }
                }
            }

            for (BaseScreenElement element : elements) {
                element.drawElement(gc, g);
            }
        }
        targetArrow.draw(564, 400);
    }
}
