/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.objects.player.Player;
import tinyships.enums.gui.GameScreenContainers;
import tinyships.enums.gui.GameScreenElements;
import tinyships.enums.gui.GameScreens;
import tinyships.game.mission.Mission;
import tinyships.global.Fonts;
import tinyships.objects.screen.container.BaseContainer;
import tinyships.objects.screen.container.StillContainer;
import tinyships.objects.screen.element.BaseScreenElement;
import tinyships.objects.screen.element.ButtonTextBoxElement;
import tinyships.objects.screen.element.SimpleTextElement;
import tinyships.ui.screens.BaseScreen;

/**
 *
 * @author Dean
 */
public class MissionScreen extends BaseScreen {

    private boolean haveMissionsUpdated;
    private Player player;

    public MissionScreen(GameScreens screenId, String name, Color color) {
        super(screenId, name, color);

        containers.add(new StillContainer(GameScreenContainers.MISSIONS, "Missions", 75, 200, 350, 250, 0));
        containers.add(new StillContainer(GameScreenContainers.MISSIONINFO, "Mission", 460, 200, 440, 400, 0));
        containers.add(new StillContainer(GameScreenContainers.OBJECTIVE, "Objectives", 75, 470, 350, 200, 0));
    }

    public boolean isHaveMissionsUpdated() {
        return haveMissionsUpdated;
    }

    public void setHaveMissionsUpdated(boolean haveMissionsUpdated) {
        this.haveMissionsUpdated = haveMissionsUpdated;
    }
    
    

    public void setPlayer(Player player) {
        this.player = player;
    }

    @Override
    public void updateScreen(boolean leftDown, GameContainer gc, int delta) {
        updateMission();

        for (BaseScreenElement element : elements) {
            element.updateElement(leftDown, gc, delta);
        }

        for (BaseContainer container : containers) {
            container.updateContainer(leftDown, gc, delta);
        }

        selectMission(gc, delta);
    }

    private void selectMission(GameContainer gc, int delta) {
        // Weapons
        for (int i = 0; i < getContainerById(GameScreenContainers.MISSIONS).elements.size(); i++) {
            BaseContainer container = getContainerById(GameScreenContainers.MISSIONS);
            if (((ButtonTextBoxElement) container.elements.get(i)).isButtonPressed(gc, delta)) {
                updateMissionBox(player.getMissions().get(i));
                updateObjectiveBox(player.getMissions().get(i));
                for (Mission mission : player.getMissions()) {
                    mission.setIsActive(false);
                }
                player.getMissions().get(i).setIsActive(true);
            }
        }
    }

    private void updateMissionBox(Mission mission) {
        if (getContainerById(GameScreenContainers.MISSIONINFO).elements.isEmpty()) {
            BaseContainer baseContainer = getContainerById(GameScreenContainers.MISSIONINFO);
            baseContainer.elements.add(new SimpleTextElement(GameScreenElements.MISSION, "", 460, 225, Fonts.infomationFont));
            baseContainer.elements.add(new SimpleTextElement(GameScreenElements.MISSION, "", 460, 245, Fonts.infomationFont));
            baseContainer.elements.add(new SimpleTextElement(GameScreenElements.MISSION, "", 460, 265, Fonts.infomationFont));
            baseContainer.elements.add(new SimpleTextElement(GameScreenElements.MISSION, "", 460, 285, Fonts.infomationFont));
            baseContainer.elements.add(new SimpleTextElement(GameScreenElements.MISSION, "", 460, 305, Fonts.infomationFont));
            baseContainer.elements.add(new SimpleTextElement(GameScreenElements.MISSION, "", 460, 325, Fonts.infomationFont));
        }

        //Remove Current Text From Mission Box Containers
        for (BaseScreenElement element : getContainerById(GameScreenContainers.MISSIONINFO).elements) {
            ((SimpleTextElement) element).setText("");
        }

        // Split Infomation on /n in description text
        String[] split = mission.getDesciption().split("/n");

        for (int i = 0; i < split.length; i++) {
            SimpleTextElement element = (SimpleTextElement) getContainerById(GameScreenContainers.MISSIONINFO).elements.get(i);
            element.setText(split[i]);
        }
    }

    private void updateObjectiveBox(Mission mission) {
        if (getContainerById(GameScreenContainers.OBJECTIVE).elements.isEmpty()) {
            BaseContainer baseContainer = getContainerById(GameScreenContainers.OBJECTIVE);
            baseContainer.elements.add(new SimpleTextElement(GameScreenElements.MISSION, "", 75, 495, Fonts.infomationFont));
            baseContainer.elements.add(new SimpleTextElement(GameScreenElements.MISSION, "", 75, 515, Fonts.infomationFont));
            baseContainer.elements.add(new SimpleTextElement(GameScreenElements.MISSION, "", 75, 535, Fonts.infomationFont));
            baseContainer.elements.add(new SimpleTextElement(GameScreenElements.MISSION, "", 75, 555, Fonts.infomationFont));
            baseContainer.elements.add(new SimpleTextElement(GameScreenElements.MISSION, "", 75, 575, Fonts.infomationFont));
            baseContainer.elements.add(new SimpleTextElement(GameScreenElements.MISSION, "", 75, 595, Fonts.infomationFont));

            //Clear current text        
            for (BaseScreenElement element : getContainerById(GameScreenContainers.OBJECTIVE).elements) {
                ((SimpleTextElement) element).setText("");
            }

            for (int i = 0; i < mission.getObjectives().size(); i++) {
                ((SimpleTextElement) baseContainer.elements.get(i)).setText(mission.getObjectives().get(i).getName());
            }
        }
    }

    private void updateMission() {
        if (!haveMissionsUpdated) {
            getContainerById(GameScreenContainers.MISSIONS).removeElements();
            getContainerById(GameScreenContainers.MISSIONINFO).removeElements();
            getContainerById(GameScreenContainers.OBJECTIVE).removeElements();
            for (int i = 0; i < this.player.getMissions().size(); i++) {
                getContainerById(GameScreenContainers.MISSIONS).addElement(
                        new ButtonTextBoxElement(GameScreenElements.MISSION,
                                player.getMissions().get(i).getName(),
                                100, 230 + (20 * i),
                                250, 18, Color.white, new Color(0, 0.3f, 0.7f, 0.6f), new Color(0, 0.3f, 0.7f, 1f)));
            }
            haveMissionsUpdated = true;
        }
    }

    @Override
    public void drawScreen(GameContainer gc, Graphics g) {
        g.setColor(overlayColor);
        g.fillRect(0, 0, 1024, 768);

        for (BaseContainer container : containers) {
            if (container != null) {
                container.drawContainer(gc, g);
            }
        }

        for (BaseScreenElement element : elements) {
            if (element != null) {
                element.drawElement(gc, g);
            }
        }
    }
}
