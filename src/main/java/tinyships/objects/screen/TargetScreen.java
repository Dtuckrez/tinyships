/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen;

import java.util.ArrayList;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import tinyships.enums.gui.GameScreens;
import tinyships.global.Fonts;
import tinyships.objects.entity.BaseEntity;
import tinyships.objects.screen.container.BaseContainer;
import tinyships.ui.screens.BaseScreen;

/**
 *
 * @author Dean
 */
public class TargetScreen extends BaseScreen {

    Image image;
    BaseEntity entity;
    ArrayList<String> description = new ArrayList<>();

    public TargetScreen(GameScreens screenId, String name, Color color) {
        super(screenId, name, color);
    }

    public void setEntity(BaseEntity baseEntity) {
        this.entity = baseEntity;
        this.image = entity.getImage();
        image.setRotation(0);
        int index = 0;
//        String toGet = entity.description;
//        if (description.isEmpty()) {
////            while (index < toGet.length()) {
////                description.add(toGet.substring(index, Math.min(index + 75, toGet.length())));
//                index += 75;
//            }
//        }
    }

    @Override
    public void updateScreen(boolean leftDown, GameContainer gc, int delta) {

    }

    @Override
    public void drawScreen(GameContainer gc, Graphics g) {
        g.setColor(this.overlayColor);
        g.fillRect(0, 0, 1024, 768);
        g.fillRoundRect(128, 96, 768, 590, 8);
        if (image != null)
            g.drawImage(image, 132, 100);
        
        g.setFont(Fonts.infomationFont);
        g.setColor(Color.black);

        for (BaseContainer container : containers) {
            container.drawContainer(gc, g);
        }

        for (int i = 0; i < description.size(); i++) {
            g.drawString(description.get(i), 275, 110 + (i * 20));
        }
    }
}
