/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import tinyships.objects.player.Player;
import tinyships.enums.gui.GameScreenContainers;
import tinyships.enums.gui.GameScreenElements;
import tinyships.enums.gui.GameScreens;
import tinyships.manager.ScreenManager;
import tinyships.objects.entity.station.BaseStation;
import tinyships.objects.screen.container.BaseContainer;
import tinyships.objects.screen.container.DropDownContainer;
import tinyships.objects.screen.element.BaseScreenElement;
import tinyships.objects.screen.element.ButtonImageElement;
import tinyships.objects.screen.element.action.ChangeScreenAction;
import tinyships.ui.screens.BaseScreen;

/**
 *
 * @author Dean
 */
public class OverScreen extends BaseScreen {

    protected Image dropImage;
    protected String message;

    protected boolean showMission;
    protected int showTime;

    private int missionCompleteYPos;
    private Player player;

    public OverScreen(GameScreens screenId, String name, Color color) {
        super(screenId, name, color);

        try {
            dropImage = new Image("images/button/dropImage.png");
            message = "";
        } catch (SlickException ex) {
            Logger.getLogger(OverScreen.class.getName()).log(Level.SEVERE, null, ex);
        }

        missionCompleteYPos = -250;

        containers.add(new DropDownContainer(GameScreenContainers.NAVIGATEMENU, "", 0, 0, 75, 6 * 65));
        containers.add(new DropDownContainer(GameScreenContainers.ENTITY, "", 1024 - 75, 0, 1024, 6 * 40));

        setupPlayerNavigationBar();
        setupEntityNavigationBar();
    }

    public boolean isShowMission() {
        return showMission;
    }

    public void setShowMission(boolean showMission) {
        this.showMission = showMission;
    }

    public void setShowMission(boolean showMission, String message) {
        this.message = message;
        this.showMission = showMission;
    }

    public int getShowTime() {
        return showTime;
    }

    public void setShowTime(int showTime) {
        this.showTime = showTime;
    }

    public void updateScreen(Player player, GameContainer gc, int delta) {
        if (this.player == null) {
            this.player = player;
        }
    }

    @Override
    public void updateScreen(boolean leftDown, GameContainer gc, int delta) {
        showMissionNotification(delta);
        if (showMission) {
            missionCompleteYPos += 1 * delta;
            if (missionCompleteYPos > 10) {
                missionCompleteYPos = 10;
            }
        }
        for (BaseScreenElement element : elements) {
            element.updateElement(leftDown, gc, delta);
        }

        for (BaseContainer container : containers) {
            if (container.containerId.equals(GameScreenContainers.ENTITY) && player != null && player.getShip().getTargetEntity() != null) {
                container.updateContainer(leftDown, gc, delta);
                if (((ButtonImageElement) (container.getElementViaId(GameScreenElements.INFO))).isButtonPressed(leftDown)) {
                    ScreenManager.setTargetScreenEntity(player.getShip().getTargetEntity());
                }
                if (((ButtonImageElement) (container.getElementViaId(GameScreenElements.STATION))).isButtonPressed(leftDown)) {
                    if (player.getShip().getTargetEntity() instanceof BaseStation) {
                        ScreenManager.setTargetScreenEntity(player.getShip().getTargetEntity());
                        ((BaseStation) player.getShip().getTargetEntity()).setIsSetForDocking(true);
                    }
                }

            } else if (container.containerId.equals(GameScreenContainers.NAVIGATEMENU)) {
                container.updateContainer(leftDown, gc, delta);
            }
        }
    }

    @Override
    public void drawScreen(GameContainer gc, Graphics g) {
        if (showMission) {
            g.setColor(Color.white);
            dropImage.draw(820, missionCompleteYPos);
            g.drawString("Mission Complete", 825, missionCompleteYPos + 20);
            g.drawString(message, 825, missionCompleteYPos + 60);
        }

        getContainerById(GameScreenContainers.NAVIGATEMENU).drawContainer(gc, g);
        getContainerById(GameScreenContainers.ENTITY).drawContainer(gc, g);

        for (BaseScreenElement element : elements) {
            if (element.elementId.equals(GameScreenElements.LOG) || element.elementId.equals(GameScreenElements.CREW)) {
                element.drawElement(gc, g);
            }
        }
    }

    public void showMissionNotification(int delta) {
        if (showMission) {
            showTime += 1 * delta;
            if (showTime >= 5000) {
                showTime = 0;
                missionCompleteYPos = -300;
                showMission = false;
            }
        }
    }

    private void setupPlayerNavigationBar() {
        getContainerById(GameScreenContainers.NAVIGATEMENU).addElement(
                new ButtonImageElement(GameScreenElements.PLAYER,
                        "images/button/button.png",
                        "images/button/info.png", null));

        getContainerById(GameScreenContainers.NAVIGATEMENU).addElement(
                new ButtonImageElement(GameScreenElements.SHIP,
                        "images/button/button.png",
                        "images/button/ship.png", null));

        getContainerById(GameScreenContainers.NAVIGATEMENU).addElement(
                new ButtonImageElement(GameScreenElements.MISSION,
                        "images/button/button.png",
                        "images/button/mission.png", null));

        getContainerById(GameScreenContainers.NAVIGATEMENU).addElement(
                new ButtonImageElement(GameScreenElements.MESSAGE,
                        "images/button/button.png",
                        "images/button/message.png", null));

        ((ButtonImageElement) getContainerById(GameScreenContainers.NAVIGATEMENU).getElementViaId(GameScreenElements.PLAYER)).setxPos(5);
        ((ButtonImageElement) getContainerById(GameScreenContainers.NAVIGATEMENU).getElementViaId(GameScreenElements.PLAYER)).setyPos(5);

        ((ButtonImageElement) getContainerById(GameScreenContainers.NAVIGATEMENU).getElementViaId(GameScreenElements.SHIP)).setxPos(5);
        ((ButtonImageElement) getContainerById(GameScreenContainers.NAVIGATEMENU).getElementViaId(GameScreenElements.SHIP)).setyPos(70);

        ((ButtonImageElement) getContainerById(GameScreenContainers.NAVIGATEMENU).getElementViaId(GameScreenElements.MISSION)).setxPos(5);
        ((ButtonImageElement) getContainerById(GameScreenContainers.NAVIGATEMENU).getElementViaId(GameScreenElements.MISSION)).setyPos(135);
        
        ((ButtonImageElement) getContainerById(GameScreenContainers.NAVIGATEMENU).getElementViaId(GameScreenElements.MESSAGE)).setxPos(5);
        ((ButtonImageElement) getContainerById(GameScreenContainers.NAVIGATEMENU).getElementViaId(GameScreenElements.MESSAGE)).setyPos(200);
    }

    private void setupEntityNavigationBar() {
        getContainerById(GameScreenContainers.ENTITY).addElement(
                new ButtonImageElement(GameScreenElements.INFO,
                        "images/button/button.png",
                        "images/button/info.png",
                        new ChangeScreenAction(GameScreens.TARGETSCEEN)));

        getContainerById(GameScreenContainers.ENTITY).addElement(
                new ButtonImageElement(GameScreenElements.STATION,
                        "images/button/button.png",
                        "images/button/station.png",
                        null));

        ((ButtonImageElement) getContainerById(GameScreenContainers.ENTITY).getElementViaId(GameScreenElements.INFO)).setxPos(1024 - 70);
        ((ButtonImageElement) getContainerById(GameScreenContainers.ENTITY).getElementViaId(GameScreenElements.INFO)).setyPos(5);

        ((ButtonImageElement) getContainerById(GameScreenContainers.ENTITY).getElementViaId(GameScreenElements.STATION)).setxPos(1024 - 70);
        ((ButtonImageElement) getContainerById(GameScreenContainers.ENTITY).getElementViaId(GameScreenElements.STATION)).setyPos(75);
    }
}
