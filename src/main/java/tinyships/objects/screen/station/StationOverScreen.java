/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.station;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.enums.gui.GameScreenElements;
import tinyships.enums.gui.GameScreens;
import tinyships.ui.screens.BaseScreen;
import tinyships.objects.screen.element.BaseScreenElement;
import tinyships.objects.screen.element.ButtonImageElement;
import tinyships.objects.screen.element.action.ChangeStationScreenAction;

/**
 *
 * @author Dean
 */
public class StationOverScreen extends BaseScreen {

    public StationOverScreen(GameScreens screenId, String name, Color color) {
        super(screenId, name, color);
        elements.add(new ButtonImageElement(GameScreenElements.INFO, "images/button/button.png",
                "images/button/info.png", 130, 700, new ChangeStationScreenAction(GameScreens.TARGETSCEEN)));
        elements.add(new ButtonImageElement(GameScreenElements.LOG, "images/button/button.png",
                "images/button/log.png", 200, 700, new ChangeStationScreenAction(GameScreens.STATIONLOGSCREEN)));
        elements.add(new ButtonImageElement(GameScreenElements.TRADE, "images/button/button.png",
                "images/button/weapon.png", 270, 700, new ChangeStationScreenAction(GameScreens.STATIONTRADESCREEN)));
        elements.add(new ButtonImageElement(GameScreenElements.CREW, "images/button/button.png",
                "images/button/crew.png", 340, 700, new ChangeStationScreenAction(GameScreens.STATIONCREWSCREEN)));
    }

    @Override
    public void updateScreen(boolean leftDown, GameContainer gc, int delta) {
        for (BaseScreenElement element : elements) {
            element.updateElement(leftDown, gc, delta);
        }
    }

    @Override
    public void drawScreen(GameContainer gc, Graphics g) {
        for (BaseScreenElement element : elements) {
            element.drawElement(gc, g);
        }
    }
}
