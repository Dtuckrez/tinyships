/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.station;

import java.util.ArrayList;
import java.util.List;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.enums.gui.GameScreenContainers;
import tinyships.enums.gui.GameScreenElements;
import tinyships.enums.gui.GameScreens;
import tinyships.global.Colours;
import tinyships.global.Fonts;
import tinyships.manager.GameManager;
import tinyships.manager.ScreenManager;
import tinyships.objects.cargo.BaseCargo;
import tinyships.objects.entity.station.cargo.StationCargo;
import tinyships.ui.screens.BaseScreen;
import tinyships.objects.screen.container.BaseContainer;
import tinyships.objects.screen.element.BaseScreenElement;
import tinyships.objects.screen.element.ButtonImageElement;
import tinyships.objects.screen.element.MultiLineTextBox;
import tinyships.objects.screen.element.SliderElement;
import tinyships.objects.screen.element.TextBoxElement;
import tinyships.objects.ships.cargobay.HeldCargo;

/**
 *
 * @author Dean
 */
public class StationTradeScreen extends BaseScreen {

    private final int ITEMS_PER_PAGE = 35;
    private boolean updateCargo = true;
    private boolean isTrading = false;

    private StationCargo cargoToTrade;

    public StationTradeScreen(GameScreens screenId, String name, Color color) {
        super(screenId, name, color);
        containers.add(new BaseContainer(GameScreenContainers.STATIONEQUIPMENT, "", 28, 96, 348, 590));
        containers.add(new BaseContainer(GameScreenContainers.SHIPEQUIPMENT, "", 648, 96, 348, 590));
        elements.add(new MultiLineTextBox(GameScreenElements.INFO, "", Fonts.dialogFont, 346, 586, 332, 100, Colours.TEXTORANGE, Colours.DARKGREY));

        // Buying/Sale Container
        containers.add(new BaseContainer(GameScreenContainers.BUYEQUIPMENT, "", 124, 768 / 2 - 100, 768, 300, Colours.DARKGREY));

        BaseContainer container = getContainerById(GameScreenContainers.BUYEQUIPMENT);
        container.addElement(new TextBoxElement(GameScreenElements.CARGO, "", "", 124, 768 / 2 - 100, 300, 35, Colours.TEXTORANGE, Colours.TEXTORANGE, Fonts.infomationFont, false));
        container.addElement(new TextBoxElement(GameScreenElements.CARGOPRICE, "", "", 430, 768 / 2 - 100, 115, 35, Colours.TEXTORANGE, Colours.TEXTORANGE, Fonts.infomationFont, false));
        container.addElement(new SliderElement(GameScreenElements.SLIDER, 220, 768 / 2, 568, 35, 15, Colours.DARKGREY, Colours.TEXTORANGE, Colours.BOXBLUE, Fonts.infomationFont, null));
        container.addElement(new TextBoxElement(GameScreenElements.STATIONCOUNT, "", "", 124, 768 / 2, 80, 35, Colours.TEXTORANGE, Colours.TEXTORANGE, Fonts.infomationFont, false));
        container.addElement(new TextBoxElement(GameScreenElements.SHIPCOUNT, "", "", 812, 768 / 2, 80, 35, Colours.TEXTORANGE, Colours.TEXTORANGE, Fonts.infomationFont, false));
        container.addElement(new TextBoxElement(GameScreenElements.TOTAL, "", "", 400, 768 / 2 + 50, 160, 35, Colours.TEXTORANGE, Colours.TEXTORANGE, Fonts.infomationFont, false));
        container.addElement(new ButtonImageElement(GameScreenElements.TRADE, "images/button/trade.png", "images/button/button.png", 300, 768 / 2 + 100, null));
        container.addElement(new ButtonImageElement(GameScreenElements.EXIT, "images/button/exit.png", "images/button/button.png", 596, 768 / 2 + 100, null));
    }

    @Override
    public void updateScreen(boolean leftDown, GameContainer gc, int delta) {
        super.updateScreen(leftDown, gc, delta);
        if (!isTrading) {
            updateStationStock(leftDown);
        } else {
            updatePurchaseScreen(leftDown, cargoToTrade);
        }
    }

    private void updateStationStock(boolean leftDown) {
        List<StationCargo> currentPageCargo = new ArrayList<>();
        currentPageCargo = ScreenManager.stationScreenManager.station.getStock().subList(0, ITEMS_PER_PAGE > ScreenManager.stationScreenManager.station.getStock().size() ? ScreenManager.stationScreenManager.station.getStock().size() : ITEMS_PER_PAGE);
        BaseContainer container = getContainerById(GameScreenContainers.STATIONEQUIPMENT);
        int COLUMNS = 5;
        int ROWS = 0;
        if (updateCargo) {
            container.elements.clear();
            for (int i = 0; i < currentPageCargo.size(); i++) {
                if (currentPageCargo.get(i) != null) {
                    int col = i % COLUMNS;
                    int row = ROWS + i / COLUMNS;

                    container.addElement(new ButtonImageElement(GameScreenElements.CARGO,
                            "images/button/button.png",
                            currentPageCargo.get(i).getCargo().getImage().getResourceReference(),
                            28 + (70 * col), 100 + (70 * row), null));
//
//                    container.addElement(new TextBoxElement(GameScreenElements.CARGOSIZE,
//                            "", String.valueOf(currentPageCargo.get(i).getAmount()),
//                            28 + (70 * col), 158 + (70 * row), 64, 24,
//                            Colours.TEXTORANGE, Colours.TRANSPARENT, Fonts.infomationFont));
                } else {
                    currentPageCargo.remove(i);
                }
            }
        }

        updateCargo = false;
        if (container.isHover()) {
            for (int i = 0; i < container.elements.size(); i++) {
                if (container.elements.get(i) instanceof ButtonImageElement) {
                    if (((ButtonImageElement) container.elements.get(i)).isHover()) {
//                        ((MultiLineTextBox) getElementById(GameScreenElements.INFO)).setName(currentPageCargo.get(i).getCargo().getName());
                        ((MultiLineTextBox) getElementById(GameScreenElements.INFO)).updatePages(currentPageCargo.get(i).getCargo().getDescription(), false);
                        if (leftDown) {
                            cargoToTrade = currentPageCargo.get(i);
                            if (cargoToTrade.getAmount() > 0) {
                                setPurchaseScreen((StationCargo) cargoToTrade);
                                isTrading = true;
                            }
                        }
                    }
                }
            }
        }
    }

    private void getShipStock() {
        // Build Grid
        List<BaseCargo> currentPageShipCargo = new ArrayList<>();
        // 0 in sublist to become pageIndex * 15
//        currentPageShipCargo = GameManager.getPlayer().getShip().getCargo().subList(0, ITEMS_PER_PAGE > GameManager.getPlayer().getShip().getCargo().size() ? GameManager.getPlayer().getShip().getCargo().size() : ITEMS_PER_PAGE);
        int COLUMNS = 5;
        int ROWS = 0;

//        elements.clear();
        for (int i = 0; i < currentPageShipCargo.size(); i++) {
            int col = i % COLUMNS;
            int row = ROWS + i / COLUMNS;

            elements.add(new ButtonImageElement(GameScreenElements.CARGO,
                    "images/button/button.png",
                    "images/button/info.png",
                    800 + (70 * col), 100 + (70 * row), null));
        }

        if (!elements.isEmpty()) {
        }
    }

    private void setPurchaseScreen(StationCargo stationCargo) {
        BaseContainer container = getContainerById(GameScreenContainers.BUYEQUIPMENT);
        ((TextBoxElement) container.getElementViaId(GameScreenElements.CARGO)).setName(stationCargo.getCargo().getName());
        ((TextBoxElement) container.getElementViaId(GameScreenElements.CARGOPRICE)).setName(String.valueOf(stationCargo.getCost()));
        ((SliderElement) container.getElementViaId(GameScreenElements.SLIDER)).setMaxNumber(stationCargo.getAmount());
        ((SliderElement) container.getElementViaId(GameScreenElements.SLIDER)).setCurrentNumber(0);
        ((TextBoxElement) container.getElementViaId(GameScreenElements.STATIONCOUNT)).setName(String.valueOf(stationCargo.getAmount()));
        ((TextBoxElement) container.getElementViaId(GameScreenElements.SHIPCOUNT)).setName(String.valueOf(GameManager.getPlayer().getShip().compareCargo(cargoToTrade.getCargo()).getAmount()));
    }

    private void updatePurchaseScreen(boolean leftDown, StationCargo stationCargo) {
        BaseContainer container = getContainerById(GameScreenContainers.BUYEQUIPMENT);
        int totalValue = stationCargo.getCost() * ((SliderElement) container.getElementViaId(GameScreenElements.SLIDER)).getCurrentNumber();
        int stationAmount = stationCargo.getAmount() - ((SliderElement) container.getElementViaId(GameScreenElements.SLIDER)).getCurrentNumber();
        int shipAmount = GameManager.getPlayer().getShip().compareCargo(stationCargo.getCargo()).getAmount() + ((SliderElement) container.getElementViaId(GameScreenElements.SLIDER)).getCurrentNumber();
        ((TextBoxElement) container.getElementViaId(GameScreenElements.TOTAL)).setName(totalValue + " / " + GameManager.getPlayer().getCredits());

        if (((ButtonImageElement) container.getElementViaId(GameScreenElements.TRADE)).isButtonPressed(leftDown)) {
            trade(cargoToTrade, ((SliderElement) container.getElementViaId(GameScreenElements.SLIDER)).getCurrentNumber(), totalValue);
        }
        ((TextBoxElement) container.getElementViaId(GameScreenElements.STATIONCOUNT)).setName(String.valueOf(stationAmount));
        ((TextBoxElement) container.getElementViaId(GameScreenElements.SHIPCOUNT)).setName(String.valueOf(shipAmount));
    }

    private void trade(HeldCargo cargoToTrade, int amountToTrade, int totalCost) {
        if (GameManager.player.getCredits() >= totalCost) {
            cargoToTrade.setAmount(cargoToTrade.getAmount() - amountToTrade);
            GameManager.getPlayer().getShip().addCargo(cargoToTrade.getCargo(), amountToTrade);
            GameManager.getPlayer().setCredits(GameManager.getPlayer().getCredits() - totalCost);
            isTrading = false;
            updateCargo = true;
        }
    }

    @Override
    public void drawScreen(GameContainer gc, Graphics g) {
        g.setColor(overlayColor);
        g.fillRect(0, 0, gc.getWidth(), gc.getHeight());

        for (BaseContainer container : containers) {
            if (container.containerId != GameScreenContainers.BUYEQUIPMENT) {
                container.drawContainer(gc, g);
            } else if (container.containerId == GameScreenContainers.BUYEQUIPMENT && isTrading) {
                container.drawContainer(gc, g);
            }
        }

        for (BaseScreenElement element : elements) {
            element.drawElement(gc, g);
        }
    }
}
