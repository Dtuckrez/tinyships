/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.station;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.enums.gui.GameScreenContainers;
import tinyships.enums.gui.GameScreenElements;
import tinyships.enums.gui.GameScreens;
import tinyships.global.Colours;
import tinyships.global.Fonts;
import tinyships.manager.ScreenManager;
import tinyships.ui.screens.BaseScreen;
import tinyships.objects.screen.container.BaseContainer;
import tinyships.objects.screen.element.BaseScreenElement;
import tinyships.objects.screen.element.SimpleTextElement;

/**
 *
 * @author Dean
 */
public class StationMessageBoardScreen extends BaseScreen {

    private boolean showing;
    private boolean updateLogs = true;

    public StationMessageBoardScreen(GameScreens id, String name, Color color) {
        super(id, name, color);

        elements.add(new SimpleTextElement(GameScreenElements.HULL, "Station Message Board", 128, 95, Fonts.titleFont, Color.white));
        containers.add(new BaseContainer(GameScreenContainers.LOG, "", 128, 96, 768, 590));

    }

    public boolean isShowing() {
        return showing;
    }

    public void setShowing(boolean showing) {
        this.showing = showing;
    }

    private void updateLogMessages() {
        if (ScreenManager.stationScreenManager.station != null && updateLogs == true) {
            for (int i = 0; i < ScreenManager.stationScreenManager.station.getLogs().size(); i++) {
                containers.add(new BaseContainer(GameScreenContainers.BASECONTAINER, "", 128, 125 + (i * 55), 768, 55 + (i * 15)));

                SimpleTextElement logTitle = new SimpleTextElement(GameScreenElements.SHIP, ScreenManager.stationScreenManager.station.getLogs().get(i).getTitle(), 128, 130 + (i * 55), Fonts.titleFont, Color.white);
                if (ScreenManager.stationScreenManager.station.getLogs().get(i).isHasQuest()) {
                    logTitle.setColor(Colours.TEXTORANGE);
                }
                elements.add(logTitle);
                elements.add(new SimpleTextElement(GameScreenElements.SHIP, ScreenManager.stationScreenManager.station.getLogs().get(i).getMessage(), 128, 155 + (i * 55), Fonts.infomationFont));
            }
            updateLogs = false;
        }
    }

    @Override
    public void updateScreen(boolean isLeft, GameContainer gc, int delta) {
        updateLogMessages();

        for (BaseScreenElement element : elements) {
            element.updateElement(isLeft, gc, delta);
        }
    }

    @Override
    public void drawScreen(GameContainer gc, Graphics g) {
        g.setColor(overlayColor);
        g.fillRect(0, 0, gc.getWidth(), gc.getHeight());

        for (BaseContainer container : containers) {
            container.drawContainer(gc, g);
        }

        for (BaseScreenElement element : elements) {
            element.drawElement(gc, g);
        }
    }
}
