/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.station;

import org.newdawn.slick.Color;
import tinyships.enums.gui.GameScreens;
import tinyships.ui.screens.BaseScreen;

/**
 *
 * @author Dean
 */
public class TradeStationScreen extends BaseScreen {

    public TradeStationScreen(GameScreens id, String name, Color color) {
        super(id, name, Color.yellow);
    }
}
