/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.screen.station;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.enums.gui.GameScreenContainers;
import tinyships.enums.gui.GameScreenElements;
import tinyships.enums.gui.GameScreens;
import tinyships.global.Colours;
import tinyships.global.Fonts;
import tinyships.manager.GameManager;
import tinyships.manager.ScreenManager;
import tinyships.objects.person.BasePerson;
import tinyships.ui.screens.BaseScreen;
import tinyships.objects.screen.container.BaseContainer;
import tinyships.objects.screen.element.BaseScreenElement;
import tinyships.objects.screen.element.ImageBoxElement;
import tinyships.objects.screen.element.TextBoxElement;

/**
 *
 * @author Dean
 */
public class StationCrewScreen extends BaseScreen {

    private boolean updateCrew = true;
    private boolean isTalking = false;

    public StationCrewScreen(GameScreens screenId, String name, Color color) {
        super(screenId, name, color);
        containers.add(new BaseContainer(GameScreenContainers.CREW, "", 128, 96, 768, 590));

    }

    @Override
    public void updateScreen(boolean leftDown, GameContainer gc, int delta) {
        super.updateScreen(leftDown, gc, delta);
        if (updateCrew) {
            updateCrew(leftDown);
            updateCrew = false;
        }
        
        BaseContainer container = getContainerById(GameScreenContainers.CREW);
        
        for (int i = 0; i < container.containers.size(); i++) {
            BasePerson person = ScreenManager.stationScreenManager.station.getPeople().get(i);
                container.containers.get(i).updateContainer(leftDown, gc, delta);
            if (container.containers.get(i).isHover()) {
                container.containers.get(i).color = Colours.BOXBLUE;
                if (leftDown && !GameManager.dialogManager.isShowingDialog() && person.isHasMission()) {
                    GameManager.dialogManager.setShowingDialog(true);
                    person.genMission();
                    if (person.getMission() != null && person.isHasMission()) {
                        GameManager.dialogManager.setMissionToAdd(person.getMission());
                        GameManager.dialogManager.updateDialogBox(person.getMission().getDesciption(), true);
                    }
                }
            }
        }
    }

    public void updateCrew(boolean leftDown) {
        BaseContainer container = getContainerById(GameScreenContainers.CREW);
        container.removeContainer();
        
        int COLUMNS = 3;
        int ROWS = 0;

        for (int i = 0; i < ScreenManager.stationScreenManager.station.getPeople().size(); i++) {
            int col = i % COLUMNS;
            int row = ROWS + i / COLUMNS;
            
            BaseContainer containerToAdd = new BaseContainer(GameScreenContainers.CREW, "", 135 + (col * 235),  102 + (100 * row), 224, 64, overlayColor);
            
            BasePerson person = ScreenManager.stationScreenManager.station.getPeople().get(i);
            containerToAdd.addElement(new TextBoxElement(GameScreenElements.INFO, person.getName(), "", 135 + (col * 235), 102 + (100 * row), 160, 35, Colours.TEXTORANGE, Colours.DARKGREY, Fonts.infomationFont, true));
            containerToAdd.addElement(new TextBoxElement(GameScreenElements.INFO, person.getRace().toString(), "", 135 + (col * 235), 144 + (100 * row), 100, 20, Colours.TEXTORANGE, Colours.DARKGREY, Fonts.infomationFont, true));
            containerToAdd.addElement(new ImageBoxElement(GameScreenElements.INFO, 130 + (col * 235 + 164), 100 + (100 * row), 64, 64, overlayColor, "images/player/face.png"));
            
            if (person.isHasMission()) {
                containerToAdd.addElement(new ImageBoxElement(GameScreenElements.INFO, 234 + (col * 232), 144 + (100 * row), 20, 20, overlayColor, "images/button/info.png"));
            }
            
            container.containers.add(containerToAdd);
        }
    }

    @Override
    public void drawScreen(GameContainer gc, Graphics g) {
        g.setColor(overlayColor);
        g.fillRect(0, 0, gc.getWidth(), gc.getHeight());

        for (BaseContainer container : containers) {
            container.drawContainer(gc, g);
        }

        for (BaseScreenElement element : elements) {
            element.drawElement(gc, g);
        }
        
        if (isTalking) {
            GameManager.dialogManager.drawDialog(gc, g);
        }
    }

    public boolean isUpdateCrew() {
        return updateCrew;
    }

    public void setUpdateCrew(boolean updateCrew) {
        this.updateCrew = updateCrew;
    }

    public boolean isIsTalking() {
        return isTalking;
    }

    public void setIsTalking(boolean isTalking) {
        this.isTalking = isTalking;
    }
    
    
}
