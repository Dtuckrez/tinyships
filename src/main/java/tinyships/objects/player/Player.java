/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.player;

import java.util.ArrayList;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.game.message.BaseMessage;
import tinyships.game.mission.BaseObjective;
import tinyships.game.mission.FlyingObjective;
import tinyships.game.mission.Mission;
import tinyships.objects.ships.PlayerShip;

/**
 *
 * @author Dean
 */
public class Player {

    private ArrayList<Mission> missions = new ArrayList<>();
    private ArrayList<BaseMessage> messages = new ArrayList<>();
    private int sectorX = 0;
    private int sectorY = 0;
    private PlayerShip ship;
    private int credits = 1000;
    private int pirateResptect = 35;

    public Player() {
        super();

//        missions.add(new Mission("Items Items Items", "We need you to pick up the items"));
//        missions.get(0).getObjectives().add(new ItemObjective("Items", "", new BaseCargo("CAKE", CargoType.TEST), 7));
//        missions.get(0).getObjectives().add(new ItemObjective("Find Ore", "Gather 200 Ore", new BaseCargo("Ore", CargoType.FOODSUPPLYS), 200));
    }

    public int getSectorX() {
        return sectorX;
    }

    public void setSectorX(int sector) {
        this.sectorX = sector;
    }

    public int getSectorY() {
        return sectorY;
    }

    public void setSectorY(int sector) {
        this.sectorY = sector;
    }

    public ArrayList<Mission> getMissions() {
        return missions;
    }

    public PlayerShip getShip() {
        return this.ship;
    }

    public void setShip(PlayerShip ship) {
        this.ship = ship;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public int getPirateResptect() {
        return pirateResptect;
    }

    public void setPirateResptect(int pirateResptect) {
        this.pirateResptect = pirateResptect;
    }

    public ArrayList<BaseMessage> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<BaseMessage> messages) {
        this.messages = messages;
    }

    public void drawShip(GameContainer gc, Graphics g) {
        if (ship.getImage() != null) {
            ship.drawShip(gc, g);
        }
    }

    public void draw(GameContainer gc, Graphics g) {
        drawMissionMarker(gc, g);
        drawShip(gc, g);
    }
    
    public void drawMissionMarker(GameContainer gc, Graphics g) {
        // Get current active mission
        for (Mission mission : missions) {
            if (mission.isIsActive()) {
                for (BaseObjective objective : mission.getObjectives()) {
                    if (!objective.getIsComplete()) {
                        if (objective instanceof FlyingObjective) {
                            if(((FlyingObjective)objective).getObjectiveArea() != null){
                               g.draw(((FlyingObjective) objective).getObjectiveArea());
                            }
                        }
                    }
                }
            }
        }
    }
}
