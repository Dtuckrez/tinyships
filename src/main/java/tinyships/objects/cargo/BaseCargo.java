/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.cargo;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import tinyships.xml.cargo.CargoData;

/**
 *
 * @author Dean
 */
public class BaseCargo {

    protected int id;
    protected String name;
    protected String description;
    protected Image image;

    public BaseCargo(String name, String description, String image) {
        try {
            this.name = name;
            this.description = description;
            if (image.equals("")) {
            this.image = new Image("images/button/" + "exit" + ".png");
            } else {
               this.image = new Image("images/button/" + image + ".png");
            }
        } catch (SlickException ex) {
            Logger.getLogger(BaseCargo.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public BaseCargo(CargoData data) {
        this.id = data.getId();
        this.name = data.getName();
        this.description = data.getDescription();
        try {
            this.image = new Image("images/button/" + "exit" + ".png");
        } catch (SlickException ex) {
            Logger.getLogger(BaseCargo.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
    
    @Override
    public BaseCargo clone() throws CloneNotSupportedException {
        return (BaseCargo) super.clone();
    }
}
