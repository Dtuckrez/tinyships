/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tinyships.objects.cargo;

import tinyships.objects.ships.equipment.BaseEquipment;

/**
 *
 * @author Dean
 */
public class EquipmentCargo extends BaseCargo {

    BaseEquipment equipment;
    
    public EquipmentCargo(BaseEquipment equipment) {
        super(equipment.getName(), equipment.getType().toString(), "");
        this.equipment = equipment;
    }

    public BaseEquipment getEquipment() {
        return equipment;
    }

    public void setEquipment(BaseEquipment equipment) {
        this.equipment = equipment;
    }
}
