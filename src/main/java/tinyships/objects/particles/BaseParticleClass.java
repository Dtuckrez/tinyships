/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.particles;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;

/**
 *
 * @author Dean
 */
public class BaseParticleClass {

    public Vector2f position;
    public Vector2f Velocity;
    public float angle;
    public float angleVolcity;
    public float size;
    public int TTL;

    public Color color;

    public BaseParticleClass(Vector2f position, Vector2f velocity, float size,
            int ttl) {
        this.position = position;
        this.Velocity = velocity;
        this.size = size;
        this.TTL = ttl;
    }

    public BaseParticleClass(Vector2f position, Vector2f velocity, float size,
            int ttl, Color color) {
        this.position = position;
        this.Velocity = velocity;
        this.size = size;
        this.TTL = ttl;

        this.color = color;
    }

    public void update() {
        TTL--;
        position.add(Velocity);
        angle += angleVolcity;
    }

    public void draw(GameContainer gc, Graphics g) {
        g.setColor(color);
        g.fillOval(position.x, position.y, size, size);
    }
}
