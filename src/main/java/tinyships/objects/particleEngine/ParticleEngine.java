/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.particleEngine;

import java.util.ArrayList;
import java.util.Random;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;
import tinyships.objects.particles.BaseParticleClass;

/**
 *
 * @author Dean
 */
public class ParticleEngine {

    public Random random;
    public Vector2f emitterLocation;
    public Color particleColours;
    public int lifeTime;
    public boolean isActive;
    public ArrayList<BaseParticleClass> particles = new ArrayList<>();
    public int totalAmount;
    public int size;

    public ParticleEngine(Color color, int lifeTime, int totalAmount, int size,
            boolean isActive) {
        this.particleColours = color;
        this.lifeTime = lifeTime;
        this.totalAmount = totalAmount;
        this.size = size;
        this.isActive = isActive;

        random = new Random();
    }

    public void resetEngine(Color color) {
        this.particleColours = color;
        lifeTime = 50;
        isActive = true;
        particles.clear();
    }

    public BaseParticleClass GenerateNewParticle() {
        if (emitterLocation != null) {
            Vector2f position = emitterLocation;
            Vector2f velocity = new Vector2f(
                    .2f * (float) ((random.nextDouble() * 2) - 1),
                    .2f * (float) ((random.nextDouble() * 2) - 1));

            float particleSize = (float) random.nextInt(size);
            int ttl = random.nextInt(lifeTime);

            return new BaseParticleClass(position, velocity, particleSize, ttl, particleColours);
        }

        return new BaseParticleClass(emitterLocation, emitterLocation, lifeTime, lifeTime);
    }

    public void update() {
        int total = totalAmount;

        for (int i = 0; i < total; i++) {
            if (isActive) {
                if (particles.size() < total) {
                    particles.add(GenerateNewParticle());
                }
            }
        }

        for (int j = 0; j < particles.size(); j++) {
            particles.get(j).update();
            if (particles.get(j).TTL <= 0) {
                particles.remove(j);
                j--;
            }
        }
    }

    public void update(boolean canCreateParicle) {
        int total = totalAmount;

        for (int i = 0; i < total; i++) {
            if (canCreateParicle) {
                if (isActive) {
                    particles.add(GenerateNewParticle());
                }
            }
        }

        for (int j = 0; j < particles.size(); j++) {
            particles.get(j).update();
            if (particles.get(j).TTL <= 0) {
                particles.remove(j);
                j--;
            }
        }
    }

    public void setEmitterLocation(Vector2f location) {
        this.emitterLocation = location;
    }

    public void draw(GameContainer gc, Graphics g) {
        for (BaseParticleClass particle : particles) {
            if (particle.TTL > 0) {
                g.setDrawMode(Graphics.MODE_ADD);
                particle.draw(gc, g);
            }
        }

        g.setDrawMode(Graphics.MODE_NORMAL);
        g.setColor(Color.white);
    }
}
