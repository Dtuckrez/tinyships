/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.entity.station;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import tinyships.objects.cargo.BaseCargo;
import tinyships.objects.entity.station.cargo.StationCargo;
import tinyships.xml.CargoLoader;

/**
 *
 * @author Dean
 */
public class EnergyStation extends BaseStation {
    protected int buildTimer = 0;
    
    protected int itemOneAmountNeeded = 2;
    protected int itemTwoAmountNeeded = 1;

    public EnergyStation(String name, int xPos, int yPos, boolean isSelectable) {
        super(name, xPos, yPos, isSelectable);
        BaseCargo cargoToAdd = CargoLoader.loadCargo("assets/cargo/cargo.xml", 0);
        stock.add(new StationCargo(cargoToAdd));
        stock.add(new StationCargo(new BaseCargo("Ore", "This mineral is used in the production process of almost all technical goods", "ore")));
        stock.add(new StationCargo(new BaseCargo("ItemB", "", "")));
        stock.add(new StationCargo(new BaseCargo("Fusion Core", "", "")));
    }

    @Override
    public void updateEntity(GameContainer gc, int delta) {
        super.updateEntity(gc, delta);

        buildTimer += 1 * delta;

        if (buildTimer >= 5000) {
            produceResource();
            buildTimer = 0;
        }
    }

    public void produceResource() {
        if (stock.get(0).getAmount() >= itemOneAmountNeeded 
                && stock.get(1).getAmount() >= itemTwoAmountNeeded) {
            
            stock.get(0).subtractAmount(itemOneAmountNeeded);
            stock.get(1).subtractAmount(itemTwoAmountNeeded);
            stock.get(2).addAmount(1);
        }
    }

    @Override
    public void drawEntity(GameContainer gc, Graphics g) {
        super.drawEntity(gc, g);
    }

}
