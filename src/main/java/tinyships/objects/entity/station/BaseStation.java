/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.entity.station;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;
import tinyships.enums.StationType;
import tinyships.global.Images;
import tinyships.objects.entity.BaseEntity;
import tinyships.objects.person.BasePerson;
import tinyships.objects.entity.station.cargo.Log;
import tinyships.objects.entity.station.cargo.StationCargo;

/**
 *
 * @author Dean
 */
public class BaseStation extends BaseEntity {
    
    protected int stationId;
    
    protected ArrayList<StationCargo> stock = new ArrayList<>();
    
    protected ArrayList<Log> logs = new ArrayList<>();
    protected ArrayList<BasePerson> people = new ArrayList<>();

    
    protected StationType type;
    protected boolean isDockable = false;
    protected boolean isSetForDocking = false;
    protected Shape dockingPoint;
    
    protected boolean updateStock;
    
    private int missionTimer = 0;
    

    public BaseStation(String name, int xPos, int yPos, boolean isSelectable) {
        super(name, xPos, yPos, isSelectable);
            this.image = Images.getImage("images/station/test_1.png");//new Image("images/station/test_1.png");
            dockingPoint = new Circle(xPos + image.getWidth() - 64, yPos + image.getHeight() + 64, 32);
        

//        stock.add(new StationCargo(new BaseCargo("Name", "Name", "Name")));

        logs.add(new Log("Test Log", "This is a log, can sometimes be clicked on", false));
        logs.add(new Log("Test Log", "This is a log, can sometimes be clicked on", true));

        
//        VIP vip = new VIP("R3X", Race.PIRATE);
//        Mission m = new Mission("Delivery Man", "R3X has asked you to move some 'Cargo' from the the Trade Station in Blue Star to the Energy Cell Production Station in Smuglers Ruin");
//        m.getObjectives().add(new DockingObjective("Docking Granted", "Dock at the Energy Cell Production Station in Smuglers Ruin", 3, 3));
//        m.getObjectives().add(new ("Docking Granted", "Dock at the Energy Cell Production Station in Smuglers Ruin", 3, 3));
//        vip.setMission(new DockingObjective(name, description, yPos, rotation));
        
//        people.add(new BasePerson("Cake", Race.PIRATE));
//        people.add(new BasePerson("More Cake", Race.PIRATE));
//        people.add(new BasePerson("A Super Cake", Race.PIRATE));
//        people.add(new BasePerson("<<<<<", Race.PIRATE));
//        people.add(new BasePerson(">>>>>", Race.PIRATE));
//        people.add(new BasePerson("_+_+_+", Race.PIRATE));
//        people.add(new BasePerson("|?|?|?|", Race.PIRATE));
    }

    public int getStationId() {
        return stationId;
    }

    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    public int getMissionTimer() {
        return missionTimer;
    }

    public void setMissionTimer(int missionTimer) {
        this.missionTimer = missionTimer;
    }
    
    public boolean isIsDockable() {
        return isDockable;
    }

    public void setIsDockable(boolean isDockable) {
        this.isDockable = isDockable;
    }

    public boolean isIsSetForDocking() {
        return isSetForDocking;
    }

    public void setIsSetForDocking(boolean isSetForDocking) {
        this.isSetForDocking = isSetForDocking;
    }

    public Shape getDockingPoint() {
        return dockingPoint;
    }

    public void setDockingPoint(Shape dockingPoint) {
        this.dockingPoint = dockingPoint;
    }

    public ArrayList<StationCargo> getStock() {
        return stock;
    }

    public void setStock(ArrayList<StationCargo> stock) {
        this.stock = stock;
    }

    public ArrayList<Log> getLogs() {
        return logs;
    }

    public void setLogs(ArrayList<Log> logs) {
        this.logs = logs;
    }

    public ArrayList<BasePerson> getPeople() {
        return people;
    }

    public void setPeople(ArrayList<BasePerson> people) {
        this.people = people;
    }

    public boolean isUpdateStock() {
        return updateStock;
    }

    public void setUpdateStock(boolean updateStock) {
        this.updateStock = updateStock;
    }

    @Override
    public void updateEntity(GameContainer gc, int delta) {
        for (BasePerson basePerson : people) {
            basePerson.updateBasePerson(delta);
        }
    }

    @Override
    public void drawEntity(GameContainer gc, Graphics g) {
        g.drawImage(image, xPos, yPos);
        if (isSetForDocking) {
            g.draw(dockingPoint);
        }
    }
}
