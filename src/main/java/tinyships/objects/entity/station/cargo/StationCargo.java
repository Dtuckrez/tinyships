/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.entity.station.cargo;

import tinyships.objects.cargo.BaseCargo;
import tinyships.objects.ships.cargobay.HeldCargo;

/**
 *
 * @author Dean
 */
public class StationCargo extends HeldCargo {
    
    private int cost;
    
    public StationCargo() {
        
    }
    
    public StationCargo(BaseCargo cargo) {
        this.cargo = cargo;
        this.setAmount(20);
        this.setCost(15);
    }

    public int getCost() {
        return cost;
    }

    public final void setCost(int cost) {
        this.cost = cost;
    }
}
