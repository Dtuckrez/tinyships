/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.entity.astroid;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import tinyships.objects.cargo.BaseCargo;
import tinyships.objects.entity.BaseEntity;
import tinyships.objects.entity.IEntity;
import tinyships.objects.entity.station.BaseStation;

/**
 *
 * @author Dean
 */
public class BaseAstroid extends BaseEntity implements IAstroid, IEntity {

    //AstroidType
    protected BaseCargo cargo;

    public BaseAstroid(String name, int xPos, int yPos, int imageNumber, boolean selectable,
            BaseCargo cargo, float rotation) {
        super(name, xPos, yPos, selectable);

        try {
            this.image = new Image("images/sector/objects/astroids/" + imageNumber + ".png");
            this.image.rotate(rotation);
            this.cargo = cargo;
        } catch (SlickException ex) {
            Logger.getLogger(BaseStation.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public int getResourceAmount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setResourceAmount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isMineable() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setIsMineable(boolean isMineable) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public BaseCargo getCargo() {
        return cargo;
    }

    @Override
    public void drawEntity(GameContainer gc, Graphics g) {
//        if (isSeleted) {
//            g.setLineWidth(3);
//            g.draw(getCollisionZone().transform(Transform.createRotateTransform(
//                    (float) Math.toRadians(rotation), 
//                    xPos + image.getWidth() / 2,
//                    yPos + image.getHeight() / 2)));
//        }
        image.draw(xPos, yPos);
    }
}
