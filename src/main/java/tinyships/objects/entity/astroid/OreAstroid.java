/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tinyships.objects.entity.astroid;

import tinyships.objects.entity.IEntity;

/**
 *
 * @author Dean
 */
public class OreAstroid extends BaseAstroid implements IEntity, IAstroid{

    public OreAstroid(int xPos, int yPos, int imageNumber, float rotation) {
        super("Ore-Astroid", xPos, yPos, imageNumber, true, null, rotation);
    }
}
