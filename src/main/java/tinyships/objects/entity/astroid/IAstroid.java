/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.entity.astroid;

import org.newdawn.slick.Image;

/**
 *
 * @author Dean
 */
public interface IAstroid {

    Image getImage();

    void setImage(Image image);

    int getResourceAmount();

    void setResourceAmount();

    boolean isMineable();

    void setIsMineable(boolean isMineable);
}
