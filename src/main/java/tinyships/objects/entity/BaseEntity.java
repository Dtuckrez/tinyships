/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.entity;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Ellipse;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Transform;
import tinyships.enums.StationType;

/**
 *
 * @author Dean
 */
public class BaseEntity implements IEntity {

    
    protected int id;
    
    protected String name;
    protected String description;
    protected Image image;
    

    protected int xPos;
    protected int yPos;
    protected int width;
    protected int height;

    protected int rotation;

    protected boolean isSeleted;
    protected boolean isSeletable;

    public BaseEntity(String name, int xPos, int yPos, boolean selectable) {
        this.name = name;
        this.xPos = xPos;
        this.yPos = yPos;
        this.isSeletable = selectable;

        this.rotation = 34;

        this.description = "This is an Entity, there are many of them in the game, /n but this one is unique as it's got it's own memeory address..."
                + "entites are used for almost all of the objects in the games sectors, /n from ships, items to be collected & astroids to be mined"
                + "this is default text as real descriptions will need to come from XML files that are read. ";
    }

    public BaseEntity(String name, int xPos, int yPos, int width, int height,
            boolean selectable) {

        this.name = name;
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = width;
        this.height = height;
        this.isSeletable = selectable;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getXpos() {
        return this.xPos;
    }

    @Override
    public void setXpos(int xPos) {
        this.xPos = xPos;
    }

    @Override
    public int getYpos() {
        return this.yPos;
    }

    @Override
    public void setYpos(int yPos) {
        this.yPos = yPos;
    }

    @Override
    public int getWidth() {
        return image.getWidth();
    }

    @Override
    public void setWidth(int width) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getHeight() {
        return image.getHeight();
    }

    @Override
    public void setHeight(int height) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Image getImage() {
        return this.image;
    }

    @Override
    public void setImage(Image image) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isSelected() {
        return this.isSeleted;
    }

    @Override
    public void setIsSelected(boolean isSelected) {
        this.isSeleted = isSelected;
    }

    @Override
    public boolean isSelectable() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setIsSelectable() {
    }

    @Override
    public void updateEntity(GameContainer gc, int delta) {
    }

    @Override
    public void drawEntity(GameContainer gc, Graphics g) {

    }

    @Override
    public boolean selectEntitiy(int realX, int realY) {
        if (realX + 1 > this.xPos
                && realX < this.xPos + image.getWidth()
                && realY + 1 > this.yPos
                && realY < this.yPos + image.getHeight()) {
            if (this.isSeletable) {
                this.isSeleted = true;
                return true;
            }
        }
        return false;
    }

    @Override
    public void destroyEntity() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Rectangle drawingRect() {
        return new Rectangle(xPos, yPos, width, height);
    }

    public Ellipse getSpeedCircle() {

        return new Ellipse(
                this.xPos + image.getWidth() / 2,
                this.yPos + image.getHeight() / 2,
                this.getImage().getWidth(),
                this.getImage().getHeight());
    }

    public Ellipse getCollisionZone() {
        Ellipse ellipse = new Ellipse(
                this.xPos + image.getWidth() / 2,
                this.yPos + image.getHeight() / 2,
                this.getImage().getWidth() / 2,
                this.getImage().getHeight() / 2);
        ellipse.transform(Transform.createRotateTransform((float) Math.toRadians(rotation)));

        return ellipse;
    }
}
