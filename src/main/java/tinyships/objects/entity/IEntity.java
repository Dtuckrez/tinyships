/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.objects.entity;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

/**
 *
 * @author Dean
 */
public interface IEntity {

    String getName();

    void setName(String name);

    int getXpos();

    void setXpos(int xPos);

    int getYpos();

    void setYpos(int yPos);

    int getWidth();

    void setWidth(int width);

    int getHeight();

    void setHeight(int height);

    Image getImage();

    void setImage(Image image);

    boolean isSelected();

    void setIsSelected(boolean isSelected);

    boolean isSelectable();

    void setIsSelectable();

    void updateEntity(GameContainer gc, int delta);

    void drawEntity(GameContainer gc, Graphics g);

    boolean selectEntitiy(int realX, int realY);

    void destroyEntity();
}
