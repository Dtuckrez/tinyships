/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tinyships.commands;

import org.newdawn.slick.geom.Vector2f;
import tinyships.manager.GameManager;
import tinyships.objects.sector.BaseSector;
import tinyships.objects.ships.BaseShip;
import tinyships.objects.ships.NPCShip;
import tinyships.objects.ships.PlayerShip;

/**
 *
 * @author Dean
 */
public class TeleShipCommand extends BaseCommand {
    public static boolean ExecuteComannd(String commandString) {

        boolean executeSuccsess = false;

        String[] split = commandString.split(" ");
        BaseShip ship = null;
        if (split.length == 4) {
            // Ignore [0] as it will be to command
            // Check every sector for ship with Id
            String id = split[1];
            if (id != null) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 2; j++) {
                        BaseSector currentSector = GameManager.sectorManager.getSectorAtPosition(i, j);
                        if (currentSector != null) {
                            for (BaseShip shipViaId : currentSector.getShips()) {
                                if (shipViaId.getId().equals(id)) {
                                    if (shipViaId instanceof NPCShip) {
                                        BaseSector sectorToMove = GameManager.sectorManager.getSectorAtPosition(Integer.parseInt(split[2]), 
                                                Integer.parseInt(split[2]));
                                        if (sectorToMove != null) {
                                            sectorToMove.getShips().add(shipViaId);
                                            currentSector.getShips().remove(shipViaId);
                                            shipViaId.setPosition(new Vector2f(-200,-100));
                                            shipViaId.getActions().clear();
                                            executeSuccsess = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return executeSuccsess;
    }
}
