/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.tools;

import java.io.File;
import java.util.ArrayList;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.gui.AbstractComponent;
import org.newdawn.slick.gui.ComponentListener;
import org.newdawn.slick.gui.TextField;
import tinyships.global.Fonts;
import tinyships.objects.entity.BaseEntity;

/**
 *
 * @author Dean
 */
public class SectorCreator extends BasicGame {

    int cameraX = 0;
    int cameraY = 0;

    int backGroundIndex;
    int forGroundIndex;

    Image image;
    ArrayList<BaseEntity> entities = new ArrayList<>();

    boolean canDraw = true;

    // GUI ELEMENTS
    TextField textField;

    public SectorCreator() {
        super("Sector Creator Tool");
    }

    @Override
    public void init(GameContainer container) throws SlickException {
        this.image = new Image("images/sector/" + 0 + ".png");

        textField = new TextField(container, Fonts.dialogFont, 10, 570, 20, 40);

        textField.addListener(new ComponentListener() {

            @Override
            public void componentActivated(AbstractComponent source) {
                backGroundIndex = Integer.parseInt(textField.getText());
                File f = new File("images/sector/" + backGroundIndex + ".png");
                if (f.exists() && !f.isDirectory()) {
                    try {
                        image = new Image("images/sector/" + backGroundIndex + ".png");
                        canDraw = true;
                    } catch (SlickException ex) {
                        canDraw = false;
                    }
                }

            }
        });
        textField.setText(String.valueOf(backGroundIndex));
    }

    @Override
    public void update(GameContainer container, int delta) throws SlickException {
    }

    @Override
    public void render(GameContainer container, Graphics g) throws SlickException {
        //Draw Background
        if (canDraw) {
            image.draw();
        }
        textField.render(container, g);
        float x = -cameraX + container.getWidth() / 2;
        float y = -cameraY + container.getHeight() / 2;
        g.pushTransform();
        g.translate(x, y);
        //Draw Objects
        g.popTransform();
    }

    public static void main(String[] args) throws SlickException {
        AppGameContainer app = new AppGameContainer(new SectorCreator());
        app.setDisplayMode(800, 600, false);
        app.setTargetFrameRate(60);
        app.start();
    }
}
