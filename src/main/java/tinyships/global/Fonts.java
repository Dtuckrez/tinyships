/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.global;

import java.awt.Font;
import org.newdawn.slick.TrueTypeFont;

/**
 *
 * @author Dean
 */
public class Fonts {

    private static final Font title = new Font("Times New Roman", Font.ITALIC, 24);
    private static final Font infomation = new Font("Monospaced", Font.PLAIN, 14);
    private static final Font dialog = new Font("Monospaced", Font.BOLD, 15);

    public static final TrueTypeFont titleFont = new TrueTypeFont(title, false);
    public static final TrueTypeFont infomationFont = new TrueTypeFont(infomation, false);
    public static final TrueTypeFont dialogFont = new TrueTypeFont(dialog, false);
}
