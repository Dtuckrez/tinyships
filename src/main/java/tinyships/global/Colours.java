/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.global;

import org.newdawn.slick.Color;

/**
 *
 * @author Dean
 */
public interface Colours {

    public static Color TRANSPARENT = new Color(0, 0, 0, 0);
    public static Color BOXBLUE = new Color(0, 0.3f, 0.7f, 0.6f);
    //#declare Orange = color red 1 green 0.5 blue 0.0
    public static Color TEXTORANGE = new Color(1, 0.5f, 0.0f, 1.0f);
    public static Color DARKGREY = new Color(0.1f, 0.1f, 0.1f, 0.8f);
    public static Color WHITE = new Color(1f, 1f, 1f, 1f);
    public static Color BOXRED = new Color(1f, 0f, 0f, 0.4f);
    public static Color BOXGREEN = new Color(0f, 0f, 1f, 0.4f);
}
