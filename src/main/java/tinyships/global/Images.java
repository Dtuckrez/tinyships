/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tinyships.global;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 *
 * @author Dean
 */
public class Images {

    // Key is the path to the images
    public static Map<String, Image> imageCollection = new HashMap<>();

    public static Image getImage(String path) {
        Image imageToGet = null;
        if (imageCollection.containsKey(path)) {
            imageToGet = imageCollection.get(path);
        } else {
            try {
                imageToGet = new Image(path);
                imageCollection.put(path, imageToGet);
            } catch (SlickException ex) {
                Logger.getLogger(Images.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return imageToGet;
    }
}   