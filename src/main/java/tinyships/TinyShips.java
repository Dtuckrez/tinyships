
/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.minlog.Log;
import java.io.IOException;
import java.util.Date;
import java.util.Scanner;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.imageout.ImageOut;
import tinyships.manager.GameManager;
import tinyships.network.listener.NetworkListener;
import tinyships.network.packet.Packet.*;
import tinyships.objects.ships.BaseShip;
import uk.org.lidalia.sysoutslf4j.context.SysOutOverSLF4J;

/**
 *
 * @author Dean
 */
public class TinyShips extends BasicGame {

    public Client client;
    public static Scanner scanner;

    public static final int SCREEN_WIDTH = 1024;
    public static final int SCREEN_HEIGHT = 768;
    public static final int FRAME_RATE = 60;

    private GameManager gameManager;
    private MenuScreen screen;
    private boolean game = false;

    public TinyShips() {
        super("TinyShips");
    }

    @Override
    public final void init(final GameContainer gc) throws SlickException {
        screen = new MenuScreen();

        scanner = new Scanner(System.in);
        client = new Client();
        regPackets();

        NetworkListener networkListener = new NetworkListener();
        networkListener.init(client);
        client.addListener(networkListener);

        client.start();
        try {
            Log.info("please enter the correct ip");
            client.connect(60000, "127.0.0.1", 9090);
        } catch (IOException ex) {
            client.close();
        }
    }
    
        private void regPackets() {
        Kryo kyro = client.getKryo();
        kyro.register(Packet0LoginRequest.class);
        kyro.register(Packet1LoginAnswer.class);
        kyro.register(Packet2Message.class);
    }

    @Override
    public final void update(final GameContainer gc, final int delta)
            throws SlickException {
        if (!game && screen.update(gc, delta)) {
            game = true;
        }
        if (game) {
            if (gameManager == null) {
                gameManager = new GameManager();
            }
            gameManager.updateGame(gc, delta);
        }
    }

    @Override
    public final void render(final GameContainer gc, final Graphics graphics)
            throws SlickException {
        Input input = gc.getInput();
        if (!game) {
            screen.draw(gc, graphics);
        }
        if (game) {
            gameManager.drawSector(gc, graphics);
            GameManager.backgroundManager.drawBackground(gc, graphics);
            BaseShip ship = GameManager.getPlayer().getShip();
            float x = -ship.getPosition().x + (float) gc.getWidth() / 2;
            float y = -ship.getPosition().y + (float) gc.getHeight() / 2;
            graphics.pushTransform();
            graphics.translate(x, y);
            gameManager.drawGame(gc, graphics);
            graphics.popTransform();

            GameManager.screenManager.drawScreens(gc, graphics);
            GameManager.dialogManager.drawDialog(gc, graphics);

            if (input.isKeyDown(Input.KEY_ENTER)) {
                Image target = new Image(gc.getWidth(), gc.getHeight());
                graphics.copyArea(target, 0, 0);
                String location = new Date().getTime() + "fasdsad.png";
                ImageOut.write(target.getFlippedCopy(false, false), location, false);
                target.destroy();
            }
        }
    }

    public static void main(final String[] args) throws SlickException {
        // Setup logging
        SysOutOverSLF4J.sendSystemOutAndErrToSLF4J();

        // Start game
        AppGameContainer app = new AppGameContainer(new TinyShips());
        app.setShowFPS(true);
        app.setVSync(true);
        app.setSmoothDeltas(true);
        app.setDisplayMode(SCREEN_WIDTH, SCREEN_HEIGHT, false);
        app.setTargetFrameRate(FRAME_RATE);
        app.start();
    }
}
