/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tinyships.xml;

import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import static tinyships.enums.gui.GameScreenElements.PLAYER;
import static tinyships.enums.ship.ShipType.NPC;
import tinyships.objects.cargo.BaseCargo;
import tinyships.objects.ships.BaseShip;
import tinyships.objects.ships.NPCShip;
import tinyships.objects.ships.PlayerShip;
import tinyships.xml.cargo.CargoData;
import tinyships.xml.cargo.CargosData;
import tinyships.xml.ships.ShipData;

/**
 *
 * @author Dean
 */
public class CargoLoader {
    
    public static BaseCargo loadCargo(String file, int cargoId) {
        try {
            JAXBContext context = JAXBContext.newInstance(CargosData.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();

            InputStream xmlFile = CargoLoader.class.getClassLoader().getResourceAsStream(file);
            CargosData cargosData = (CargosData) unmarshaller.unmarshal(xmlFile);

            CargoData data = cargosData.getCargos().get(cargoId);
            data.setId(cargoId);

//            switch (data.getType()) {
//
//                default:
//                return new BaseCargo(data);
//            }
        } catch (JAXBException ex) {
            Logger.getLogger(ShipLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new BaseCargo(new CargoData());
    }
}
