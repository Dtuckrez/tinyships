/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.xml.cargo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import tinyships.enums.ship.equipment.CargoType;

/**
 *
 * @author Mike
 */
@XmlAccessorType(XmlAccessType.NONE)

@XmlRootElement(name = "item")
public class CargoData {

    private int id;
    @XmlAttribute
    private CargoType type;
    @XmlElement
    private String name;
    @XmlElement
    private String description;
    @XmlElement
    private int price;
    @XmlElement

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CargoType getType() {
        return type;
    }

    public void setType(CargoType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }    
}
