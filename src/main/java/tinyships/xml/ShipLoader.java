/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.xml;

import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import tinyships.enums.ship.ShipType;
import tinyships.objects.ships.BaseShip;
import tinyships.objects.ships.NPCShip;
import tinyships.objects.ships.PlayerShip;
import tinyships.xml.ships.ShipData;
import tinyships.xml.ships.ShipsData;

/**
 *
 * @author Mike
 */
public class ShipLoader {

    public static BaseShip loadShip(ShipType shipType, String file, int shipId) {
        try {
            JAXBContext context = JAXBContext.newInstance(ShipsData.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();

            InputStream xmlFile = ShipLoader.class.getClassLoader().getResourceAsStream(file);
            ShipsData shipsData = (ShipsData) unmarshaller.unmarshal(xmlFile);

            ShipData data = shipsData.getShips().get(shipId);
            data.setId(shipId);

            switch (shipType) {
                case PLAYER:
                    return new PlayerShip(data);

                case NPC:
                    return new NPCShip(data);

                default:
                return new BaseShip(data);
            }
        } catch (JAXBException ex) {
            Logger.getLogger(ShipLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new BaseShip(new ShipData());
    }
}
