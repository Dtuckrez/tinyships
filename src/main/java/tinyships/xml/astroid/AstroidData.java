/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.xml.astroid;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import tinyships.enums.StationType;

/**
 *
 * @author Dean
 * @author Mike
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "astroid")
public class AstroidData {

    @XmlElement
    private int id;
    @XmlElement
    private int xpos;
    @XmlElement
    private int ypos;
    @XmlElement
    private int rotation;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getxpos() {
        return xpos;
    }

    public void setxPos(int xpos) {
        this.xpos = xpos;
    }

    public int getypos() {
        return ypos;
    }

    public void setypos(int ypos) {
        this.ypos = ypos;
    }

    public int getRotation() {
        return rotation;
    }

    public void setRotation(int rotation) {
        this.rotation = rotation;
    }

}
