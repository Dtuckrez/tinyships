/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.xml.astroid;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import tinyships.xml.station.StationData;

/**
 *
 * @author Dean
 * @author Mike
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "astroids")
public class AstroidsData {

    @XmlElement(name = "astroid")
    private List<AstroidData> astroids = new ArrayList<>();

    public List<AstroidData> getAstroids() {
        return astroids;
    }

    public void setAstroids(List<AstroidData> astroids) {
        this.astroids = astroids;
    }
}
