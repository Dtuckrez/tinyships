/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.xml.ships.shields;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import tinyships.enums.ship.equipment.EquipmentType;
import tinyships.enums.ship.equipment.ShieldSize;
import tinyships.enums.ship.equipment.ShieldType;

/**
 *
 * @author Mike
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "shield")
public class ShieldData {

    @XmlAttribute
    private ShieldType type;
    @XmlElement
    private String name;
    @XmlElement
    private String description;
    @XmlElement
    private EquipmentType equipmentType;
    @XmlElement
    private ShieldSize size;
    @XmlElement
    private int maxPower;
    @XmlElement
    private int rechargeDelay;
    @XmlElement
    private int rechargeRate;
    @XmlElement
    private int power;

    public ShieldData() {
        super();
    }

    public ShieldData(String name, String description,
            EquipmentType equipmentType, ShieldType type, ShieldSize size,
            int maxPower, int rechargeDelay, int rechargeRate) {

        this.name = name;
        this.description = description;
        this.equipmentType = equipmentType;
        this.type = type;
        this.size = size;
        this.maxPower = maxPower;
        this.rechargeDelay = rechargeDelay;
        this.rechargeRate = rechargeRate;
    }

    public ShieldType getType() {
        return type;
    }

    public void setType(ShieldType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public EquipmentType getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(EquipmentType equipmentType) {
        this.equipmentType = equipmentType;
    }

    public ShieldSize getSize() {
        return size;
    }

    public void setSize(ShieldSize size) {
        this.size = size;
    }

    public int getMaxPower() {
        return maxPower;
    }

    public void setMaxPower(int maxPower) {
        this.maxPower = maxPower;
    }

    public int getRechargeDelay() {
        return rechargeDelay;
    }

    public void setRechargeDelay(int rechargeDelay) {
        this.rechargeDelay = rechargeDelay;
    }

    public int getRechargeRate() {
        return rechargeRate;
    }

    public void setRechargeRate(int rechargeRate) {
        this.rechargeRate = rechargeRate;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }
}
