/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.xml.ships;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import tinyships.enums.ArmyType;
import tinyships.enums.ship.ShipType;
import tinyships.xml.ships.engines.EngineData;
import tinyships.xml.ships.shields.ShieldData;
import tinyships.xml.ships.weapons.WeaponBayData;
import tinyships.xml.ships.weapons.WeaponBaysData;

/**
 *
 * @author Mike
 */
@XmlAccessorType(XmlAccessType.NONE)

@XmlRootElement(name = "ship")
public class ShipData {

    private int id;
    @XmlAttribute
    private ShipType type;
    @XmlElement
    private String name;
    @XmlElement
    private String description;
    @XmlElement
    private ArmyType armyType;
    @XmlElement
    private int hull;
    @XmlElement
    private ShieldData shield;
    @XmlElement
    private int engineAmount;
    @XmlElement
    private EngineData engine;
    @XmlElement
    private WeaponBaysData weaponBays = new WeaponBaysData();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ShipType getType() {
        return type;
    }

    public void setType(ShipType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArmyType getArmyType() {
        return armyType;
    }

    public void setArmyType(ArmyType armyType) {
        this.armyType = armyType;
    }

    public int getHull() {
        return hull;
    }

    public void setHull(int hull) {
        this.hull = hull;
    }

    public ShieldData getShield() {
        return shield;
    }

    public void setShield(ShieldData shield) {
        this.shield = shield;
    }

    public EngineData getEngine() {
        return engine;
    }

    public void setEngine(EngineData engine) {
        this.engine = engine;
    }

    public WeaponBaysData getWeaponBays() {
        return weaponBays;
    }

    public void setWeaponBays(WeaponBaysData weaponBays) {
        this.weaponBays = weaponBays;
    }

    public int getEngineAmount() {
        return engineAmount;
    }

    public void setEngineAmount(int engineAmount) {
        this.engineAmount = engineAmount;
    } 
}
