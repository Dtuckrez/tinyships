/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tinyships.xml.ships.weapons;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import tinyships.objects.ships.weapons.BaseWeaponBay;
import tinyships.xml.ships.ShipData;

/**
 *
 * @author User
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "weaponBays")
public class WeaponBaysData {

    @XmlElement(name = "weaponBay")
    private BaseWeaponBay weaponBay;

    public BaseWeaponBay getWeaponBay() {
        return weaponBay;
    }

    public void setWeaponBays(BaseWeaponBay weaponBay) {
        this.weaponBay = weaponBay;
    }
    
    
}
