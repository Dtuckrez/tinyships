/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.xml.ships.weapons;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import tinyships.enums.ship.equipment.EquipmentType;
import tinyships.enums.ship.equipment.WeaponType;

/**
 *
 * @author Mike
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "weapon")
public class WeaponData {

    @XmlAttribute
    private WeaponType type;
    @XmlElement
    private String name;
    @XmlElement
    private String description;
    @XmlElement
    private EquipmentType equipmentType;
    @XmlElement
    private int hullDamage;
    @XmlElement
    private int shieldDamage;
    @XmlElement
    private int speed;
    @XmlElement
    private int lifeTime;
    @XmlElement
    private int fireTime;

    public WeaponType getType() {
        return type;
    }

    public void setType(WeaponType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public EquipmentType getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(EquipmentType equipmentType) {
        this.equipmentType = equipmentType;
    }

    public int getHullDamage() {
        return hullDamage;
    }

    public void setHullDamage(int hullDamage) {
        this.hullDamage = hullDamage;
    }

    public int getShieldDamage() {
        return shieldDamage;
    }

    public void setShieldDamage(int shieldDamage) {
        this.shieldDamage = shieldDamage;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getLifeTime() {
        return lifeTime;
    }

    public void setLifeTime(int lifeTime) {
        this.lifeTime = lifeTime;
    }

    public int getFireTime() {
        return fireTime;
    }

    public void setFireTime(int fireTime) {
        this.fireTime = fireTime;
    }
}
