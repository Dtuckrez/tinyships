/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.xml;

import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import tinyships.objects.sector.BaseSector;
import tinyships.xml.sector.SectorData;
import tinyships.xml.sector.SectorsData;

/**
 *
 * @author Dean
 * @author Mike
 */
public class SectorLoader {

    public static BaseSector loadSector(String file, int sectorId) {

        try {
            JAXBContext context = JAXBContext.newInstance(SectorsData.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();

            InputStream xmlFile = SectorLoader.class.getClassLoader().getResourceAsStream(file);
            SectorsData sectorsData = (SectorsData) unmarshaller.unmarshal(xmlFile);
            
            SectorData data = sectorsData.getSectors().get(sectorId);
            return new BaseSector(data);
        } catch (JAXBException ex) {
            Logger.getLogger(SectorLoader.class.getName()).log(Level.SEVERE, null, ex);
        }

        return new BaseSector(new SectorData());
    }
}
