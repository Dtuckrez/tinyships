/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.xml.station;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import tinyships.enums.StationType;

/**
 *
 * @author Dean
 * @author Mike
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "station")
public class StationData {

    private int id;
    @XmlElement
    private String name;
    @XmlElement
    private String description;
    @XmlElement
    private boolean selectable;
    @XmlElement
    private StationType stationType;
    @XmlElement
    private int xpos;
    @XmlElement
    private int ypos;
    @XmlElement
    private int rotation;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean selectable() {
        return selectable;
    }

    public void setselectable(boolean selectable) {
        this.selectable = selectable;
    }

    public StationType getStationType() {
        return stationType;
    }

    public void setStationType(StationType stationType) {
        this.stationType = stationType;
    }

    public int getxpos() {
        return xpos;
    }

    public void setxPos(int xpos) {
        this.xpos = xpos;
    }

    public int getypos() {
        return ypos;
    }

    public void setypos(int ypos) {
        this.ypos = ypos;
    }

    public int getRotation() {
        return rotation;
    }

    public void setRotation(int rotation) {
        this.rotation = rotation;
    }

}
