/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.ai.actions;

import org.newdawn.slick.geom.Vector2f;
import tinyships.objects.ships.BaseShip;

/**
 *
 * @author Dean
 */
public class SweepAttackAction extends BaseAction {

    private float currentRotation;
    private boolean hasMadePass;

    public boolean peformAction(BaseShip ship, BaseShip eShip, int delta) {

        ship.getEngine().accelerate();

        if (!hasMadePass) {
            if (checkRotation(ship, delta)) {
//                ship.shoot(delta);
            }
            if (ship.getSpeedCircle().intersects(eShip.getSpeedCircle())) {
                hasMadePass = true;
            }
        } else if (hasMadePass && eShip.getSpeedCircle().intersects(ship.getCollisionZone())) {
            hasMadePass = false;
        }

        double radians = Math.toRadians(ship.getRotation());
        ship.setPosition(new Vector2f(
                ship.getPosition().x += ship.getEngine().getSpeed() * Math.sin(radians),
                ship.getPosition().y -= ship.getEngine().getSpeed() * Math.cos(radians)));
        return false;
    }

    public boolean checkRotation(BaseShip ship, int delta) {
        boolean isFacing;

        Vector2f hero = new Vector2f(ship.getShipCenter().x, ship.getShipCenter().y);
        Vector2f target = new Vector2f(
                ship.getEShip().getShipCenter().x,
                ship.getEShip().getShipCenter().y);
        target.sub(hero);
        float finalRotation = (int) target.getTheta();

        if (!(currentRotation <= finalRotation + 1 && currentRotation >= finalRotation - 1)) {
            isFacing = false;
            double tempVal;
            if (currentRotation < finalRotation) {
                tempVal = finalRotation - currentRotation;

                if (tempVal > 179) {
                    currentRotation -= ship.getEngine().getTurn() * delta;
                } else {
                    currentRotation += ship.getEngine().getTurn() * delta;
                }
            } else if (currentRotation > finalRotation) {
                tempVal = currentRotation - finalRotation;
                if (tempVal < 180) {
                    currentRotation -= ship.getEngine().getTurn() * delta;
                } else {
                    currentRotation += ship.getEngine().getTurn() * delta;
                }
            }
        } else {
            isFacing = true;
        }
        if (currentRotation < 0) {
            currentRotation = 359;
        }

        if (currentRotation > 360) {
            currentRotation = 1;
        }
        ship.setRotation((int) (currentRotation + 90));
        return isFacing;
    }
}
