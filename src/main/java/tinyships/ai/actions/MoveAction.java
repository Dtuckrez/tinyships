/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.ai.actions;

import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;
import tinyships.objects.ships.BaseShip;
import tinyships.objects.ships.engines.IBaseEngine;

/**
 *
 * @author Dean
 */
public class MoveAction extends BaseAction { 

    public Vector2f target;
    public float currentRotation;

    public MoveAction(int x, int y) {
        super();

        updateTarget(x, y);
    }

    public final void updateTarget(int x, int y) {
        target = new Vector2f(x, y);
    }

    public boolean performAction(BaseShip ship, int delta) {
        Shape shape = new Circle(target.x, target.y, 8);
        shape.setCenterX(target.x);
        shape.setCenterY(target.y);

        // Slows down ship
        IBaseEngine engine = ship.getEngine();
        if (ship.getSpeedCircle().intersects(shape)) {
            
//            engine.setSpeed(engine.getSpeed() - -engine.getAcceleration() * delta);
//            if (engine.getSpeed() < 0) {
//                engine.setSpeed(0);
//            }
        }
        if (!checkRotation(ship, shape, delta)) {
            engine.accelerate();
        } else {
            engine.setSpeed(engine.getSpeed() - -engine.getAcceleration() * delta);
            if (engine.getSpeed() < 0.02f) {
                engine.setSpeed(0.02f);
            }
        }
        double radians = Math.toRadians(ship.getRotation());
        if (engine.getSpeed() > 0 && !ship.getSpeedCircle().intersects(shape)) {
            ship.setPosition(new Vector2f(
                    ship.getPosition().x += engine.getSpeed() * Math.sin(radians),
                    ship.getPosition().y -= engine.getSpeed() * Math.cos(radians)));
        } else if (ship.getSpeedCircle().intersects(shape)) {
            engine.setSpeed(0);
            return true;
        }
        return false;
    }

    public boolean checkRotation(BaseShip ship, Shape shape, int delta) {

        boolean isFacing;

        Vector2f shipos = new Vector2f(ship.getPosition().x, ship.getPosition().y);
//        target.sub(shipos);
        float finalRotation = (int) target.getTheta();

        if (!(currentRotation <= finalRotation + 3 && currentRotation >= finalRotation - 3)) {
            isFacing = false;
            double tempVal;
            if (currentRotation < finalRotation) {
                tempVal = finalRotation - currentRotation;
                if (tempVal > 180) {
                    currentRotation -=  0.01f * delta;
                } else {
                    currentRotation += 0.01f * delta;
                }
            } else if (currentRotation > finalRotation) {
                tempVal = currentRotation - finalRotation;
                if (tempVal < 179) {
                    currentRotation -= 0.01f * delta;
                } else {
                    currentRotation += 0.01f * delta;
                }
            }
        } else {
            isFacing = true;
        }
        if (currentRotation < 0) {
            currentRotation = 359;
        }

        if (currentRotation > 360) {
            currentRotation = 1;
        }
        ship.setRotation((int) (currentRotation + 90));
        
        return isFacing;
    }
}
