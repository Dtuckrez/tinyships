/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.ai.actions;

import org.newdawn.slick.geom.Vector2f;
import tinyships.objects.entity.BaseEntity;
import tinyships.objects.ships.BaseShip;
import tinyships.objects.ships.engines.IBaseEngine;

/**
 *
 * @author Dean
 */
public class MoveToEntityAction extends BaseAction {

    private float currentRotation;

    public boolean performAction(BaseShip ship, BaseEntity entity, int delta) {
        // Slows down ship
        IBaseEngine engine = ship.getEngine();
        if (ship.getSpeedCircle().intersects(entity.getSpeedCircle())) {
            engine.setSpeed(engine.getSpeed() - -(engine.getAcceleration() + 8f) * delta);
            if (engine.getSpeed() < 0) {
                engine.setSpeed(0);
            }
        }
        engine.accelerate();
        if (!checkRotation(ship, delta) && engine.getSpeed() > 0.2f) {
            engine.setSpeed(0.2f);
        }
        double radians = Math.toRadians(ship.getRotation());
        if (engine.getSpeed() > 0 && !ship.getSpeedCircle().intersects(entity.getSpeedCircle())) {
            ship.setPosition(new Vector2f(
                    ship.getPosition().x += engine.getSpeed() * Math.sin(radians),
                    ship.getPosition().y -= engine.getSpeed() * Math.cos(radians)));
        } else if (ship.getSpeedCircle().intersects(entity.getSpeedCircle())) {
            engine.setSpeed(0);
            return true;
        }
        return false;
    }

    public boolean checkRotation(BaseShip ship, int delta) {
        boolean isFacing;
        
        Vector2f target = new Vector2f(
                ship.getTargetEntity().getXpos() + ship.getTargetEntity().getImage().getWidth() / 2,
                ship.getTargetEntity().getYpos() + ship.getTargetEntity().getImage().getHeight() / 2);
        float finalRotation = (int) target.getTheta();

        if (!(currentRotation <= finalRotation + 1 && currentRotation >= finalRotation - 1)) {
            isFacing = false;
            double tempVal;
            if (currentRotation < finalRotation) {
                tempVal = finalRotation - currentRotation;

                if (tempVal > 179) {
                    currentRotation -= ship.getEngine().getTurn() * delta;
                } else {
                    currentRotation += ship.getEngine().getTurn() * delta;
                }
            } else if (currentRotation > finalRotation) {
                tempVal = currentRotation - finalRotation;
                if (tempVal < 180) {
                    currentRotation -= ship.getEngine().getTurn() * delta;
                } else {
                    currentRotation += ship.getEngine().getTurn() * delta;
                }
            }
        } else {
            isFacing = true;
        }
        if (currentRotation < 0) {
            currentRotation = 359;
        }

        if (currentRotation > 360) {
            currentRotation = 1;
        }
        ship.setRotation((int) (currentRotation + 90));
        return isFacing;
    }
}
