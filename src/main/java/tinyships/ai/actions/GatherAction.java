/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.ai.actions;

import tinyships.enums.ship.equipment.EquipmentType;
import tinyships.objects.entity.BaseEntity;
import tinyships.objects.ships.BaseShip;

/**
 *
 * @author Dean
 */
public class GatherAction extends BaseAction {

    public boolean performAction(BaseShip ship, BaseEntity entity, int delta) {
        if (ship.getSpeedCircle().intersects(entity.getSpeedCircle())
                && ship.getEngine().getSpeed() == 0 && ship.checkIfHasEquipment(EquipmentType.MINING)) {
//            ((MiningEquipment) ship.getEquipment().get(0)).setIsBeingUsed(true);
//            ((MiningEquipment) ship.getEquipment().get(0)).useEquipment(ship, entity, delta);
        } else {
//            ((MiningEquipment) ship.getEquipment().get(0)).setIsBeingUsed(true);
        }
        return false;
    }
}
