/*
 * #%L
 * This file is part of Tiny Ships.
 * %%
 * Copyright (C) 2015 Dean Tucker (dtuckrez@gmail.com)
 * %%
 * Tiny Ships is Proprietary software: Unauthorized copying and/or distribution
 * of this file, via any medium without the express permission of the owner is
 * strictly prohibited.
 * #L%
 */
package tinyships.ai.actions;

/**
 *
 * @author Dean
 */
public class LoopAction extends BaseAction {

    @Override
    public boolean performAction() {
        return true;
    }
}
