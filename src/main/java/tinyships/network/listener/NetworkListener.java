/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tinyships.network.listener;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.minlog.Log;
import tinyships.network.packet.Packet;
import tinyships.network.packet.Packet.*;

/**
 *
 * @author Dean
 */
public class NetworkListener extends Listener {

    private Client client;

    public void init(Client client) {
        this.client = client;
    }

    @Override
    public void connected(Connection cnctn) {
        Log.info("[Client] You have connected");
        client.sendTCP(new Packet.Packet0LoginRequest());
    }

    @Override
    public void disconnected(Connection cnctn) {
        Log.info("[Client] You have disconnected");
    }

    @Override
    public void received(Connection cnctn, Object o) {
        // Obeject is from client 
        if (o instanceof Packet1LoginAnswer) {
            boolean answer = ((Packet1LoginAnswer) o).accepted;
            if (answer = true) {
            } else {
                cnctn.close();
            }
        }
    }
}
